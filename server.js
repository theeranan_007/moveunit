

const fs = require('fs');
const mysql = require('promise-mysql');
const express = require('express');
const app = express();

app.set('port', (process.env.PORT || 3001));

// Express only serves static assets in production process.env.NODE_ENV =
// 'production 
process.env.NODE_ENV = 'production'
console.log("NODE_ENV: ", process.env.NODE_ENV);
if (process.env.NODE_ENV === 'production') {
  app.use(express.static('client/build'));

  // Return the main index.html, so react-router render the route in the client
  app.get('/', (req, res) => {
    res.sendFile(path.resolve('client/build', 'index.html'));
  });
}

const host = "localhost"
const user = "root"
const pswd = ""
const dbname = "navdb"

// config db ====================================
// const pool = mysql.createPool({ host: host, user: user, password: pswd, port: "3306", database: dbname });
const pool = mysql.createPool({
  host: host,
  user: user,
  password: pswd,
  database: dbname,
  connectionLimit: 10
});

app.get('/api/person', (req, res) => { // ค้นหาชื่อ นามสกุล
  const name = req.query.name;
  const sname = req.query.sname;
  const id8 = req.query.id8;

  if (!name && !sname & !id8) {
    res.json({ error: 'Missing required parameters' }); // ถ้า ส่งตัวแปรมาไม่ครบ ให้return ค่ากลับไป
    return;
  }
  let queryString = ``;
  queryString = `SELECT * from person WHERE name like '%${name}%' and  sname like '%${sname}%' and  id8 like '${id8}%'`
  pool.query(queryString, function (err, rows, fields) {

    if (err) {
      res.json({ error: err })// ส่งค่า error กลับไป
      throw err;
    }
    if (rows.length > 0) {
      res.json(rows);
    } else {
      res.json([]); // .ใช้ในกรณีที่ไม่พบข้อมูล ให้่ส่งค่าว่างกลับไป
    }
  });
});
app.get('/api/detailmodal', (req, res) => { // ดึงรายละเอียดจาก navyid
  const navyid = req.query.navyid;
  if (!navyid || navyid == 'undefined') {
    res.json({ error: 'Missing required parameters' }); // ถ้า ส่งตัวแปรมาไม่ครบ ให้return ค่ากลับไป
    return;
  }
  let queryString = ``;
  queryString = `SELECT p.NAVYID,p.NAME,p.SNAME,p.YEARIN,p.OLDYEARIN,p.ID8,p.HEIGHT,p.PERCENT,p.IS_REQUEST,
                      P.BATT,p.COMPANY,p.PLATOON,P.PSEQ,
                      statustab.title STATUS,patient.title PATIENT,religion.RELIGION,eductab.EDUCNAME,
                      skilltab.SKILL,positiontab.POSTNAME,u4.unitname UNIT4,selectexam.ITEM,
                      u1.unitname UNIT1,u2.unitname UNIT2,u3.unitname UNIT3,
                      u1.refnum valueUnit1,u2.refnum valueUnit2,u3.refnum valueUnit3,u4.refnum valueUnit4,
                      selectexam.postcode,selectexam.item,a.addname,p.addictive,swim.distance from person p
      left join statustab on statustab.statuscode = p.statuscode  
      left join statustab patient on statustab.statuscode = p.patient_status
      LEFT JOIN addictivetab a on a.addcode = p.addictive
      left join religion on religion.regcode = p.regcode
      left join eductab on eductab.ecode1 = p.educode1 and eductab.ecode2 = p.educode2
      left join skilltab on skilltab.skillcode = p.skillcode
      left join selectexam on selectexam.navyid = p.navyid
      left join unittab u1 on u1.refnum = p.unit1
      left join unittab u2 on u2.refnum = p.unit2
      left join unittab u3 on u3.refnum = p.unit3
      LEFT JOIN swim on swim.swimcode = p.swimcode
      left join positiontab on positiontab.postcode = selectexam.postcode
      left join unittab u4 on u4.refnum = selectexam.unit4
      where p.navyid='${navyid}' `
  pool.query(queryString, function (err, rows, fields) {
    if (err) {
      res.json({ error: err })// ส่งค่า error กลับไป
      throw err;
    }
    if (rows.length > 0) {
      res.json(rows[0]);
    } else {
      res.json([]); // .ใช้ในกรณีที่ไม่พบข้อมูล ให้่ส่งค่าว่างกลับไป
    }
  });
});
app.get('/api/request', (req, res) => { //คิวรี่ ร้องขอ
  const navyid = req.query.navyid;
  if (!navyid || navyid == 'undefined') {
    res.json({ error: 'Missing required parameters' }); // ถ้า ส่งตัวแปรมาไม่ครบ ให้return ค่ากลับไป
    return;
  }
  let queryString = ``;
  queryString = `SELECT (@cnt := @cnt + 1) AS rowNumber, r.unit, ur.unitname,r.selectcode,r.selectid,r.lockselectcode,substring(r.askby,1,2) as askcode,
      substring(r.askby,4) as askname,r.askby,r.remark,r.remark2,r.num
      from request r
			CROSS JOIN (SELECT @cnt := 0) as dummy
      inner join person on person.navyid = r.navyid
      left join unittab ur on ur.refnum = r.unit
      where person.navyid='${navyid}'
      order by CONVERT(substring(r.askby,1,3),INTEGER)`
  pool.query(queryString, function (err, rows, fields) {
    if (err) {
      res.json({ error: err })// ส่งค่า error กลับไป
      throw err;
    }
    if (rows.length > 0) {
      res.json(rows);
    } else {
      res.json([]); // .ใช้ในกรณีที่ไม่พบข้อมูล ให้่ส่งค่าว่างกลับไป
    }
  });
});
app.get('/api/numrequest', (req, res) => {    //คิวรี้ ลำดับใน request
  const Askcode = req.query.Askcode;
  if (!Askcode || Askcode == 'undefined') {
    res.json({ error: 'Missing required parameters' }); // ถ้า ส่งตัวแปรมาไม่ครบ ให้return ค่ากลับไป
    return;
  }
  let queryString = ``;
  queryString = `SELECT ifnull(max(num),0) Num from request r where substring(r.askby,1,2) = '${Askcode}'`
  pool.query(queryString, function (err, rows, fields) {
    if (err) {
      res.json({ error: err })// ส่งค่า error กลับไป
      throw err;
    }
    if (rows.length > 0) {
      res.json(rows[0]);
    } else {
      res.json([]); // .ใช้ในกรณีที่ไม่พบข้อมูล ให้่ส่งค่าว่างกลับไป
    }
  });
});
app.get('/api/itemselectexam', (req, res) => {    //คิวรี้ ลำดับใน request
  const unit4 = req.query.unit4;
  const postcode = req.query.postcode;
  if (!Askcode || Askcode == 'undefined') {
    res.json({ error: 'Missing required parameters' }); // ถ้า ส่งตัวแปรมาไม่ครบ ให้return ค่ากลับไป
    return;
  }
  let queryString = ``;
  queryString = `SELECT ifnull(max(num),0) Num from request r where substring(r.askby,1,2) = '${Askcode}'`
  pool.query(queryString, function (err, rows, fields) {
    if (err) {
      res.json({ error: err })// ส่งค่า error กลับไป
      throw err;
    }
    if (rows.length > 0) {
      res.json(rows[0]);
    } else {
      res.json([]); // .ใช้ในกรณีที่ไม่พบข้อมูล ให้่ส่งค่าว่างกลับไป
    }
  });
});
app.get('/api/changeunit3', (req, res) => {  // เปลี่ยนหน่วย รายบุคคล
  const navyid = req.query.navyid;
  const unit3 = req.query.unit3;
  const type = req.query.type;
  let queryString = ``;
  queryString = `select p2.moveunit_type from person p2 where p2.navyid = ${navyid}`;
  pool.query(queryString, function (err, rows, fields) {
    // delete ข้อมูล request ก่อน
    if (err) {
      res.json({ error: err })// ส่งค่า error กลับไป
      throw err;
    } else {
      if (unit3 == 0) {
        queryString = `update person p set p.unit3 = ${unit3},moveunit_type = null,moveunit_oldtype = null  where p.navyid = ${navyid}`
        pool.query(queryString, function (err, rows, fields) {
          // delete ข้อมูล request ก่อน
          if (err) {
            res.json({ error: err })// ส่งค่า error กลับไป
            throw err;
          } else {
            res.json({ success: "complete" })
          }
        })
      }
      else if (rows.length > 0) {
        if (rows[0].moveunit_type == null) {
          queryString = `update person p set p.unit3 = ${unit3},moveunit_type = '${type}',moveunit_oldtype = null  where p.navyid = ${navyid}`
        }
        else {
          queryString = `update person p set p.unit3 = ${unit3},moveunit_type = '${type}',moveunit_oldtype = '${rows[0].moveunit_type}'  where p.navyid = ${navyid}`

        }
        pool.query(queryString, function (err, rows, fields) {
          // delete ข้อมูล request ก่อน
          if (err) {
            res.json({ error: err })// ส่งค่า error กลับไป
            throw err;
          } else {
            res.json({ success: "complete" })
          }
        })
      }

      else {
        queryString = `update person p set p.unit3 = ${unit3},moveunit_type = '${type}' where p.navyid = ${navyid}`
        pool.query(queryString, function (err, rows, fields) {
          // delete ข้อมูล request ก่อน
          if (err) {
            res.json({ error: err })// ส่งค่า error กลับไป
            throw err;
          } else {
            res.json({ success: "complete" })
          }
        })
      }
    }
  })
});

app.get('/api/changeAllUnit3', (req, res) => { //เปลี่ยนหน่วย แบบหลายคน
  const navyid = req.query.navyid;
  const unit3 = req.query.Unit3;
  const type = req.query.type;
  // if (!navyid ||) {  //ดักไม่ได้ เนื่องจากทำฟังก์ชันทีหลัง และก่อนหน้ามีตัวแปรที่ไม่ได้ส่งค่ามา แต่ต้องยอมให้ คิวรี้ได้
  //   res.json({error: 'Missing required parameters'}); // ถ้า ส่งตัวแปรมาไม่ครบ ให้return ค่ากลับไป
  //   return;
  // }
  let queryString = ``;
  queryString = `select p2.moveunit_type,p2.navyid from person p2 where p2.navyid in (${navyid})`;
  pool.query(queryString, function (err, rows_old, fields) {
    if (err) {
      res.json({ error: err })// ส่งค่า error กลับไป
      throw err;
    } else {
      if (unit3 == 0) {
        queryString = `update person p set p.unit3 = ${unit3},moveunit_type = null,moveunit_oldtype = null  where p.navyid in (${navyid})`
        pool.query(queryString, function (err, rows, fields) {

          if (err) {
            res.json({ error: err })// ส่งค่า error กลับไป
            throw err;
          } else {
            res.json({ success: "complete" })
          }
        })
      }
      else if (rows_old.length > 0) {
        let j = 0;
        for (let i = 0; i < rows_old.length; i++) {
          if (rows_old[i].moveunit_type == null) {
            queryString = `update person p set p.unit3 = ${unit3},moveunit_type = '${type}',moveunit_oldtype = null  where p.navyid = ${rows_old[i].navyid}`
          }
          else {
            queryString = `update person p set p.unit3 = ${unit3},moveunit_type = '${type}',moveunit_oldtype = '${rows_old[i].moveunit_type}'  where p.navyid = ${rows_old[i].navyid}`

          }
          pool.query(queryString, function (err, rows2, fields) {

            if (err) {
              res.json({ error: err })// ส่งค่า error กลับไป
              throw err;
            }
            j++;
            if (rows_old.length == j) {
              res.json({ success: "complete" })
            }
          })
        }
      }
      else {
        queryString = `update person p set p.unit3 = ${unit3},moveunit_type = '${type}' where p.navyid in (${navyid})`
        pool.query(queryString, function (err, rows, fields) {
          // delete ข้อมูล request ก่อน
          if (err) {
            res.json({ error: err })// ส่งค่า error กลับไป
            throw err;
          } else {
            res.json({ success: "complete" })
          }
        })
      }
    }
  });
});
app.get('/api/checknumrequest', (req, res) => {  //เช็คลำดับซ้ำในร้องขอ
  const Askcode = req.query.Askcode;
  const Num = req.query.Num;
  if ((!Askcode || Askcode == 'undefined') && (!Num || Num == 'undefined')) {
    res.json({ error: 'Missing required parameters' }); // ถ้า ส่งตัวแปรมาไม่ครบ ให้return ค่ากลับไป
    return;
  }
  let queryString = ``;

  queryString = `SELECT  person.name,person.sname,ur.unitname,r.selectcode,r.selectid,substring(r.askby,1,2) as askcode,
      substring(r.askby,4) as askname,r.askby,r.remark,r.remark2,r.num
      from request r
      inner join person on person.navyid = r.navyid
      left join unittab ur on ur.refnum = r.unit
      where substring(r.askby,1,2)='${Askcode}' and r.Num='${Num}'`

  pool.query(queryString, function (err, rows, fields) {
    if (err) {
      res.json({ error: err })// ส่งค่า error กลับไป
      throw err;
    }
    if (rows.length > 0) {
      res.json(rows);
    } else {
      res.json([]); // .ใช้ในกรณีที่ไม่พบข้อมูล ให้่ส่งค่าว่างกลับไป
    }
  });
});
app.get('/api/checkitemselectexam', (req, res) => {  //เช็คลำดับซ้ำในคัดเลือกหน่วย ***ลำดับในคัดเลือกหน่วยจะใช้ฟิล์ดชื่อ item
  const unit4 = req.query.unit4;
  const postcode = req.query.postcode;
  const item = req.query.item;
  if ((!unit4 || unit4 == 'undefined') && (!postcode || postcode == 'undefined') && (!item || item == 'undefined')) {
    res.json({ error: 'Missing required parameters' }); // ถ้า ส่งตัวแปรมาไม่ครบ ให้return ค่ากลับไป
    return;
  }
  let queryString = ``;

  queryString = `SELECT person.name,person.sname,u4.unitname refnum4,positiontab.postname
      from selectexam s
      inner join person on person.navyid = s.navyid
      inner join unittab u4 on u4.refnum = s.unit4
      inner join positiontab on positiontab.postcode = s.postcode
      where s.unit4 = ${unit4} and s.postcode = ${postcode} and s.itme = ${item}`

  pool.query(queryString, function (err, rows, fields) {
    if (err) {
      res.json({ error: err })// ส่งค่า error กลับไป
      throw err;
    }
    if (rows.length > 0) {
      res.json(rows);
    } else {
      res.json([]); // .ใช้ในกรณีที่ไม่พบข้อมูล ให้่ส่งค่าว่างกลับไป
    }
  });
});
app.get('/api/request_report', (req, res) => { // คิวรี่รายงาน ร้องขอ 
  // การคิวรี่นี้ใช้ การคิวรี่ ซ้อนคิวรี่ เนื่องจาก ทหารแ 1 คน สามารถมีผู้ขอได้มากกว่า 1 ทำให้เมื่อต้องเรียงลำดับ จะต้องกรุปตามตัวทหาร หรือ แสดงคนขอคนอื่น ในฟิล์ดต่อกัน
  const sort = req.query.sort;
  const where = req.query.where;
  const have = req.query.have;
  const groupby = req.query.groupby;
  let queryString = ``;
  var data = [];
  queryString = `SELECT r.navyid,r.selectid,substring(r.askby,1,3) askcode,r.unit from request r 

  LEFT JOIN unittab as ur on ur.refnum = r.unit
  LEFT JOIN person as p on r.NAVYID = p.NAVYID
  LEFT JOIN unittab as u1 on u1.refnum = p.unit1
  LEFT JOIN unittab as u2 on u2.refnum = p.unit2
  LEFT JOIN unittab as u3 on u3.refnum = p.unit3
  LEFT JOIN statustab on p.statuscode = statustab.statuscode
  LEFT JOIN skilltab on p.skillcode = skilltab.skillcode
  LEFT JOIN eductab on p.educode1 = eductab.ecode1 and p.educode2 = eductab.ecode2
  LEFT JOIN selectexam on selectexam.navyid = p.navyid
  LEFT JOIN positiontab on positiontab.postcode = selectexam.postcode
  LEFT JOIN unittab as u4 on u4.refnum = selectexam.unit4
  LEFT JOIN addictivetab as a on a.addcode = p.addictive
  ${where != "" && where != null
      ? "WHERE " + where
      : " "}
      ${groupby != "" && groupby != null
      ? "GROUP BY " + groupby + " HAVING count(" + groupby + ")" + have
      : " "}
      ${sort != "" && sort != null
      ? "ORDER BY " + sort
      : " "}`
  pool.query(queryString, function (err, rows, fields) {
    if (err) {
      res.json({ error: err })// ส่งค่า error กลับไป
      throw err;
    }
    if (rows.length > 0) {
      var i = 0;
      var j = 0;
      id = rows;
      // 
      var askcode = 0;
      asyncForEach()
      async function asyncForEach() {
        for (let index = 0; index < id.length; index++) {
          let queryString2 = ""
          queryString2 = `SELECT p.NAVYID,ifnull(p.oldyearin,p.yearin) yearin,p.name,p.sname,concat(p.name," ",p.sname) fullname,r.askby,
      p.percent,concat(p.company,"/",p.batt) as belong,r.unit,r.num,
      r.remark,r.remark2,r.selectcode,
      eductab.educname,selectexam.unit4,p.unit3,p.unit1,p.unit2,p.unit0,selectexam.postcode,
      selectexam.item,concat(positiontab.postname," ",u4.unitname,"(",selectexam.item,")") selectexam,r.selectid,statustab.title,statustab.stitle,
      if(skilltab.skill="ไม่กำหนด","",skilltab.skill) as skill,u1.unitname as refnum1,
      u2.unitname as refnum2,u3.unitname as refnum3,ur.unitname as refnumur,
      u4.unitname as refnum4,positiontab.postname,p.id8,a.addname,p.addictive,swim.distance,p.HEIGHT,r.unit unit0 FROM request r 
    LEFT JOIN unittab as ur on ur.refnum = r.unit
    LEFT JOIN person as p on r.NAVYID = p.NAVYID
    LEFT JOIN addictivetab a on a.addcode = p.addictive
    LEFT JOIN swim on swim.swimcode = p.swimcode
    LEFT JOIN unittab as u1 on u1.refnum = p.unit1
    LEFT JOIN unittab as u2 on u2.refnum = p.unit2
    LEFT JOIN unittab as u3 on u3.refnum = p.unit3
    LEFT JOIN statustab on p.statuscode = statustab.statuscode
    LEFT JOIN skilltab on p.skillcode = skilltab.skillcode
    LEFT JOIN eductab on p.educode1 = eductab.ecode1 and p.educode2 = eductab.ecode2
    LEFT JOIN selectexam on selectexam.navyid = p.navyid
    LEFT JOIN positiontab on positiontab.postcode = selectexam.postcode
    LEFT JOIN unittab as u4 on u4.refnum = selectexam.unit4 where r.navyid = ${id[index].navyid}
    ${where != "" && where != null
              ? "and " + where
              : " "}
    ${sort != "" && sort != null
              ? "ORDER BY " + sort
              : " "}`
          await pool.query(queryString2).then(function (rows2) {
            if (rows2.length > 0) {
              var tmp = 0;
              askcode = id[index].askcode
              rows2.forEach(function (element, i) { // ปรับแต่งข้อมูลให้ได้ดตามต้องการ
                rows2[i] = { ...element, askcode: askcode, unitcode: id[index].unit }
              }, this)
              rows2.forEach(function (element) { // ปรับแต่งข้อมูลให้ได้ดตามต้องการ

                if (tmp > 0) {
                  element.id8 = ""
                  element.belong = ""
                  element.name = ""
                  element.sname = ""
                  element.fullname = ""
                }
                data[j] = {
                  ...element,
                  id: j + 1,
                  hilight: (rows2.length > 1
                    ? 1
                    : 0)
                }
                j++;
                tmp++
              }, this);
            }
            i++;
            if (id.length == i) {
              res.json(data);
            }
          });
        }
      }
    } else {
      res.json([]);
    }
  });
});
app.get('/api/tabledata', (req, res) => { // คิวรี่ ข้อมูล สามารถใช้ได้หลายรูปแบบ ยกเว้นภาษาไทย ไม่สามาถรส่งมาเป็น ก้อน where ได้ ต้องส่งแยกต่างหาก

  const sort = req.query.sort;
  const where = req.query.where;
  const have = req.query.have;
  const groupby = req.query.groupby;
  const name = req.query.name;
  const sname = req.query.sname;
  const id8 = req.query.id8;
  let queryString = ``;
  var data = [];
  queryString = `SELECT ${groupby != "" && groupby != null
    ? "count(p.NAVYID) count,"
    : ""}p.NAVYID,p.yearin,p.name,p.sname,p.batt,p.company,p.platoon,p.pseq,p.oldyearin,concat(p.name," ",p.sname) fullname,r.askby,
    p.percent,concat(p.company,"/",p.batt) as belong,r.unit,r.num,
    r.remark,r.remark2,r.selectcode,concat(r.askby," ",ur.unitname,"(",r.num,")") request,p.percent,religion.religion,p.statuscode,
    eductab.educname,selectexam.unit4,p.unit3,p.unit1,p.unit2,p.unit0,selectexam.postcode,
    selectexam.item,concat(positiontab.postname," ",u4.unitname,"(",selectexam.item,")") selectexam,r.selectid,statustab.title,statustab.stitle,
    if(skilltab.skill="ไม่กำหนด","",skilltab.skill) as skill,u1.unitname as refnum1,
    u2.unitname as refnum2,u3.unitname as refnum3,ur.unitname as refnumur,
    u4.unitname as refnum4,positiontab.postname,p.id8,a.addname,p.addictive,swim.distance,p.HEIGHT from person p 
    
    LEFT JOIN request as r on r.NAVYID = p.NAVYID and r.selectcode >= 1
    LEFT JOIN addictivetab a on a.addcode = p.addictive
    LEFT JOIN swim on swim.swimcode = p.swimcode
    LEFT JOIN unittab as ur on ur.refnum = r.unit
    LEFT JOIN unittab as u1 on u1.refnum = p.unit1
    LEFT JOIN unittab as u2 on u2.refnum = p.unit2
    LEFT JOIN unittab as u3 on u3.refnum = p.unit3
    LEFT JOIN religion on religion.regcode = p.regcode
    LEFT JOIN statustab on p.statuscode = statustab.statuscode
    LEFT JOIN skilltab on p.skillcode = skilltab.skillcode
    LEFT JOIN eductab on p.educode1 = eductab.ecode1 and p.educode2 = eductab.ecode2
    LEFT JOIN selectexam on selectexam.navyid = p.navyid
    LEFT JOIN positiontab on positiontab.postcode = selectexam.postcode
    LEFT JOIN unittab as u4 on u4.refnum = selectexam.unit4
    WHERE p.name like '%${name != 'undefined'
      ? name
      : ""}%' and p.sname like '%${sname != 'undefined'
        ? sname
        : ""}%' and p.id8 like '${id8 != 'undefined'
          ? id8
          : ""}%'
    ${where != "" && where != null
      ? " and " + where
      : " "}
      ${groupby != "" && groupby != null
      ? "GROUP BY " + groupby
      : " "}
        ${sort != "" && sort != null
      ? "ORDER BY " + sort
      : " "}`
  pool.query(queryString, function (err, rows, fields) {
    if (err) {
      console.log(err)
      throw err;
    }
    if (rows.length > 0) {
      res.json(rows);
    } else {
      res.json([]);
    }
  });

});
app.get('/api/providecount', (req, res) => { // คิวรี่ ยอดจังหวัด
  const armid = req.query.armid;
  if (!armid || armid == 'underfined') {
    res.json({ error: "missing parameter" })
    return
  }
  let queryString = ``;
  queryString = `select armtown.ARMNAME,armtown.ARMID,count(person.navyid) 'all' from person 
    left join armtown on armtown.ARMID = person.ARMID
    where person.ARMID in (${armid}) 
    GROUP BY person.ARMID
    ORDER BY person.ARMID`
  pool.query(queryString, function (err, rows, fields) {
    if (err) {
      console.log(err)
      throw err;
    }
    if (rows.length > 0) {
      res.json(rows);
    } else {
      res.json([]);
    }
  });
});
app.get('/api/religionprovincecount', (req, res) => { // คิวรี่ ศาสนาตามจังหวัด
  const armid = req.query.armid;
  if (!armid || armid == 'underfined') {
    res.json({ error: "missing parameter" })
    return
  }
  let queryString = ``;
  queryString = `select unittab.refnum,sum(if(REGCODE=1,1,0)) 'REG1',sum(if(REGCODE=2,1,0)) 'REG2',sum(if(REGCODE=3,1,0)) 'REG3',sum(if(REGCODE>=1 && REGCODE<=3,1,0)) 'REGALL' from unittab
  left join person on unittab.REFNUM = person.unit3 and armid in(${armid})
  where unittab.MOVESTAT = 1
  GROUP BY unittab.refnum
  ORDER BY unittab.refnum`
  pool.query(queryString, function (err, rows, fields) {
    if (err) {
      console.log(err)
      throw err;
    }
    if (rows.length > 0) {
      res.json(rows);
    } else {
      res.json([]);
    }
  });
});
app.get('/api/counteducation', (req, res) => { // คิวรี่ วุฒิ

  let queryString = ``;
  queryString = `select concat(eductab.ecode1,eductab.ecode2) ecode,eductab.ecode1,eductab.ecode2,eductab.educname,count(person.navyid) as counteducation
  ,sum(if(person.unit3 = 0,1,0)) counteducation2,sum(if(person.unit3 != 0,1,0)) counteducation3 from eductab
  inner join person on person.educode1 = eductab.ecode1 and person.educode2 = eductab.ecode2
  where eductab.ecode1>3
  group by eductab.educname
  order by eductab.ecode1 desc,count(person.navyid) desc,eductab.ecode2`
  pool.query(queryString, function (err, rows, fields) {
    if (err) {
      console.log(err)
      throw err;
    }
    if (rows.length > 0) {
      res.json(rows);
    } else {
      res.json([]);
    }
  });
});
app.get('/api/countskill', (req, res) => { // คิวรี่ อัตรา กพ.ทร

  let queryString = ``;
  queryString = `select skilltab.skillcode,skilltab.skill,count(person.navyid) as countskill,
  sum(if(person.unit3 = 0,1,0)) countskill2,sum(if(person.unit3 != 0,1,0)) countskill3 from skilltab
              inner join person on person.skillcode = skilltab.skillcode
              where skilltab.skillcode = "D" or skilltab.skillcode = "E" or skilltab.skillcode = "F" or skilltab.skillcode = "G" or skilltab.skillcode = "2H" or skilltab.skillcode = "L"
              group by skilltab.skillcode
              order by skilltab.skillcode`
  pool.query(queryString, function (err, rows, fields) {
    if (err) {
      console.log(err)
      throw err;
    }
    if (rows.length > 0) {
      res.json(rows);
    } else {
      res.json([]);
    }
  });
});
app.get('/api/amount', (req, res) => { // คิวรี่ อัตรา กพ.ทร
  const armid = req.query.armid;
  if (!armid) {
    res.json({ error: "missing armid" })
    return
  }
  let queryString = ``;
  queryString = `select unittab.refnum,unittab.unitname,count,amount.percent,amount.manual_percent,sum(if(REGCODE!=2,1,0)) 'REG1',sum(if(REGCODE=2,1,0)) 'REG2',sum(if(REGCODE=3,1,0)) 'REG3',sum(if(REGCODE!=2 || REGCODE=2,1,0)) 'REGALL' from amount 
  inner join unittab on unittab.refnum = amount.unit
  left join person on unittab.REFNUM = person.unit3 and armid in(${armid})
  GROUP BY unittab.refnum
  ORDER BY unittab.refnum`
  pool.query(queryString, function (err, rows, fields) {
    if (err) {
      console.log(err)
      throw err;
    }
    if (rows.length > 0) {
      res.json(rows);
    } else {
      res.json([]);
    }
  });
});
app.get('/api/updateamount', (req, res) => { // อัพเดทยอดตาม อัตรา กพ.ทร
  const unit = req.query.unit;
  const count = req.query.count;
  const percent = req.query.percent;
  const manual_percent = req.query.manual_percent;
  if (!unit || unit == 'undefined') {
    res.json({ error: 'Missing required parameters' }); // ถ้า ส่งตัวแปรมาไม่ครบ ให้return ค่ากลับไป
    return;
  }
  let queryString = ``;
  wait();
  async function wait() {
    queryString = `update amount set ${count == "-1" ? `percent = ${percent}` : `${count == "-2" ? `manual_percent = ${manual_percent}` : `count = ${count}`}`} where unit = ${unit}`
    log("amount", queryString)
    await pool.query(queryString, function (err, rows, fields) {
      if (err) {
        console.log(err)
        throw err;
      } else {
        res.json({ success: "complete" });
      }
    });
  }
});
app.get('/api/insertrequest', (req, res) => { //เพิ่มข้อมูลเข้าตารางร้องขอและปรับผู้ขออัติโนมัติ
  const navyid = req.query.navyid;
  const Unit = req.query.Unit;
  const Askcode = req.query.Askcode;
  const Askname = req.query.Askname;
  const Askby = Askcode + " " + Askname;
  const Num = req.query.Num;
  const Remark = req.query.Remark;
  const Remark2 = req.query.Remark2;
  let queryString = ``;

  queryString = `insert into request (yearin,navyid,name,sname,unit,askby,num,remark,remark2,selectid)
                  values((select person.yearin from person where person.navyid = ${navyid}),${navyid},(select person.name from person where person.navyid = ${navyid}),(select person.sname from person where person.navyid = ${navyid}),${Unit},'${Askby}',${Num},'${Remark}','${Remark2}',(select ifnull(max(r2.selectid),0)+1 from request r2))`
  log('Insertrequest', queryString)
  pool.query(queryString, function (err, rows, fields) {
    // insert ข้อมูลลง request ก่อน
    if (err) {
      res.json({ status: 500, error: err, response: queryString })
    } else {
      process_request(req, res, "insert");
    }
  });

});
app.get('/api/lockrequest', (req, res) => { //เพิ่มข้อมูลเข้าตารางร้องขอและปรับผู้ขออัติโนมัติ
  const navyid = req.query.navyid;
  const lockselectcode = req.query.lockselectcode;
  const Selectid = req.query.Selectid;
  if (navyid == "" || navyid == undefined || navyid == "undefined" ||
    lockselectcode == "" || lockselectcode == undefined || lockselectcode == "undefined" ||
    Selectid == "" || Selectid == undefined || Selectid == "undefined") {
    res.json({ status: 500, error: "missing parameter", response: null })
  }
  let queryString = ``;

  queryString = `update request set lockselectcode = ${lockselectcode} where navyid = ${navyid} and selectid = ${Selectid} and selectcode = 1`
  log('Lockrequest', queryString)
  pool.query(queryString, function (err, rows, fields) {
    // insert ข้อมูลลง request ก่อน
    if (err) {
      res.json({ status: 500, error: err, response: queryString })
    } else {
      //process_request(req, res, "insert");
      res.json({ status: 400, error: null, response: "success" })
    }
  });

});
app.get('/api/editrequest', (req, res) => { //แก้ไขผู้ขอ
  const navyid = req.query.navyid;
  const Unit = req.query.Unit;
  const Askcode = req.query.Askcode;
  const Askname = req.query.Askname;
  const Askby = Askcode + " " + Askname;
  const Num = req.query.Num;
  const Remark = req.query.Remark;
  const Remark2 = req.query.Remark2;
  const Selectid = req.query.Selectid;
  let queryString = ``;
  var today = new Date();
  var dd = today.getDate();
  var mm = today.getMonth() + 1; //January is 0!
  var yyyy = today.getFullYear();

  if (dd < 10) {
    dd = '0' + dd
  }

  if (mm < 10) {
    mm = '0' + mm
  }
  today = yyyy + '-' + mm + '-' + dd;

  var totime = new Date();
  var hh = totime.getHours();
  var mm = totime.getMinutes();
  var ss = totime.getSeconds();
  if (hh < 10) {
    hh = '0' + hh
  }
  if (mm < 10) {
    mm = '0' + mm
  }
  if (ss < 10) {
    ss = '0' + ss
  }
  totime = hh + ':' + mm + ':' + ss;
  queryString = `update request set unit=${Unit},askby='${Askby}',num=${Num},remark='${Remark}',remark2='${Remark2}',upddate = '${today}',updtime = '${totime}',updatecount = (select max(updatecount)+1) where navyid = ${navyid} and selectid = ${Selectid}`
  log('Editrequest', queryString)
  pool.query(queryString, function (err, rows, fields) {
    // insert ข้อมูลลง request ก่อน
    if (err) {
      res.json({ status: 500, error: err, response: queryString });
    } else {
      queryString = `update person set person.unit3 = (select unit from request where navyid = ${navyid} and selectcode >= 1),person.unit0 = (select unit from request where navyid = ${navyid} and selectcode >= 1),person.ask = '(select askby from request where navyid = ${navyid} and selectcode >= 1)',person.number = (select num from request where navyid = ${navyid} and selectcode >= 1),person.rem = (select remark from request where navyid = ${navyid} and selectcode >= 1),person.rem2 = (select remark2 from request where navyid = ${navyid} and selectcode >= 1),moveunit_type="Request",moveunit_oldtype=(select moveunit_type) where person.navyid=${navyid}`
      log('Editrequest', queryString)
      pool.query(queryString, function (err, rows, fields) {
        // insert ข้อมูลลง request ก่อน
        if (err) {
          res.json({ status: 500, error: err, response: queryString })
        } else {
          res.json({ status: 200, error: null, response: rows })
        }
      });
    }
  });
});
app.get('/api/check_last_item', (req, res) => {
  const unit4 = req.query.unit4;
  const postcode = req.query.postcode;
  queryString = `select ifnull(max(s2.item),0)+1 as item  from selectexam s2 where s2.unit4 = ${unit4} and s2.postcode = '${postcode}'`;
  pool.query(queryString, function (err, rows, fields) {
    if (err) {
      res.json({ error: err })// ส่งค่า error กลับไป
      throw err;
    } else {
      res.json({ rows });
    }
  })
});
app.get('/api/insertselectexam', (req, res) => { //เพิ่มข้อมูลตาราง คัดเลือกหน่วย
  const navyid = req.query.navyid;
  const unit4 = req.query.unit4;
  const postcode = req.query.postcode;
  const item = req.query.item;
  if (!navyid || !postcode || !unit4) {
    res.json({ error: "Missing parameter" })
  }
  let queryString2 = ``;
  let queryString = ``;
  queryString2 = `select navyid,unit4,postcode,item from selectexam where navyid=${navyid} limit 1;`
  pool.query(queryString2, function (err, rows_2, fields) {
    if (err) {
      res.json({ error: err })// ส่งค่า error กลับไป
      throw err;
    }
    else {
      queryString2 = `select navyid,unit4,postcode,item from selectexam where (unit4=${unit4} and postcode = '${postcode}' and item=${item})or(navyid=${navyid}) limit 1;`
      pool.query(queryString2, function (err, rows_, fields) {
        if (err) {
          res.json({ error: err })// ส่งค่า error กลับไป
          throw err;
        }
        if (rows_.length > 0 || rows_2.length > 0) {
          console.log(rows_ == rows_2)
          if (rows_2.length > 0) {
            rows_ = rows_2;
          }
          let queryString = ``;
          queryString2 = `delete from selectexam where navyid=${navyid} and unit4 = ${rows_[0].unit4} and postcode = '${rows_[0].postcode}' and item = ${rows_[0].item};`
          pool.query(queryString2, function (err, rows2, fields) {
            if (err) {
              res.json({ error: err })// ส่งค่า error กลับไป
              throw err;
            } else {
              let queryString3 = ``;
              // if (rows_[0].navyid == navyid) {
              queryString3 = `update selectexam set item = (select item-1) where unit4 = ${rows_[0].unit4} and postcode = '${rows_[0].postcode}' and item > ${rows_[0].item};`
              // }
              // queryString3 = `update selectexam set item = (select item) where unit4 = ${unit4} and postcode = '${rows_[0].postcode}' and item > ${rows_[0].item};`
              pool.query(queryString3, function (err, rows3, fields) {
                if (err) {
                  res.json({ error: err })// ส่งค่า error กลับไป
                  throw err;
                } else {
                  queryString3 = `update person set unit4 = null,postcode = null,item = null where navyid=${navyid};`
                  pool.query(queryString3, function (err, rows3, fields) {
                    if (err) {
                      res.json({ error: err })// ส่งค่า error กลับไป
                      throw err;
                    } else {
                      queryString2 = `update selectexam set item = (select item+1) where unit4 = ${unit4} and postcode = '${postcode}' and item >= ${item};` // ถ้าซ้ำลบชื่อ ออกก่อน
                      pool.query(queryString2, function (err, rows2, fields) {
                        if (err) {
                          res.json({ error: err })// ส่งค่า error กลับไป
                          throw err;
                        } else {

                          queryString = `insert into selectexam (yearin,navyid,Unit4,Postcode,item)
                              values((select person.yearin from person where person.navyid = ${navyid} LIMIT 1),${navyid},${unit4},'${postcode}','${item}'); `

                          pool.query(queryString, function (err, rows, fields) {
                            if (err) {
                              res.json({ error: err })// ส่งค่า error กลับไป
                              throw err;
                            } else {
                              queryString = `update person set unit4 = ${unit4},postcode = '${postcode}',item = (select s2.item from selectexam s2 where s2.navyid = ${navyid} limit 1) where navyid = ${navyid}; `
                              pool.query(queryString, function (err, rows, fields) {
                                if (err) {
                                  console.log(queryString)
                                  res.json({ error: err })// ส่งค่า error กลับไป
                                  throw err;
                                } else {
                                  res.json({ success: "complete" })
                                }
                              });
                            }
                          });
                        }
                      });
                    }
                  });
                }
              });
            }
          });
        }
        else {

          queryString = `insert into selectexam (yearin,navyid,Unit4,Postcode,item)
                                  values((select person.yearin from person where person.navyid = ${navyid} LIMIT 1),${navyid},${unit4},'${postcode}','${item}'); `

          pool.query(queryString, function (err, rows, fields) {
            if (err) {
              res.json({ error: err })// ส่งค่า error กลับไป
              throw err;
            } else {
              queryString = `update person set unit4 = ${unit4},postcode = '${postcode}',item = (select s2.item from selectexam s2 where s2.navyid = ${navyid} limit 1) where navyid = ${navyid}; `
              pool.query(queryString, function (err, rows, fields) {
                if (err) {
                  console.log(queryString)
                  res.json({ error: err })// ส่งค่า error กลับไป
                  throw err;
                } else {
                  res.json({ success: "complete" })
                }
              });
            }
          });
        }
      });
    }
  });
});
app.get('/api/deleteselectexam', (req, res) => {
  const navyid = req.query.navyid;
  const unit4 = req.query.unit4;
  const postcode = req.query.postcode;
  const item = req.query.item;
  let queryString2 = ``;
  queryString2 = `delete from selectexam where navyid=${navyid} and unit4 = ${unit4} and postcode = '${postcode}' and item = ${item};`
  pool.query(queryString2, function (err, rows2, fields) {
    if (err) {
      res.json({ error: err })// ส่งค่า error กลับไป
      throw err;
    } else {
      let queryString3 = ``;
      queryString3 = `update selectexam set item = (select item-1) where unit4 = ${unit4} and postcode = '${postcode}' and item > ${item};`
      pool.query(queryString3, function (err, rows3, fields) {
        if (err) {
          res.json({ error: err })// ส่งค่า error กลับไป
          throw err;
        } else {
          queryString3 = `update person set unit4 = null,postcode = null,item = null where navyid=${navyid};`
          pool.query(queryString3, function (err, rows3, fields) {
            if (err) {
              res.json({ error: err })// ส่งค่า error กลับไป
              throw err;
            } else {
              res.json({ complete: "success" })
            }
          });
        }
      });
    }
  });
});

// app.get('/api/updateUnit3', (req, res) => {
//   const navyid = req.query.navyid;
//   const unit3 = req.query.unit3;
//   let queryString = ``;

//   queryString = `update person p set p.unit3 = ${unit3}`
//   pool.query(queryString, function (err, rows, fields) {
//     // delete ข้อมูล request ก่อน
//     if (err) {
//       res.json({error:err})// ส่งค่า error กลับไป
//       throw err;
//     } else {
//       res.json({success:"complete"})
//     }
//   })
// })
app.get('/api/deleterequest', (req, res) => {
  const navyid = req.query.navyid;
  const Selectid = req.query.Selectid;
  let queryString = ``;

  queryString = `delete from request where navyid=${navyid} and selectid=${Selectid}`
  log("Deleterequest", queryString);
  pool.query(queryString, function (err, rows, fields) {
    // delete ข้อมูล request ก่อน
    if (err) {
      res.json({ status: 500, error: err, response: queryString })
    } else {
      process_request(req, res, "delete");
    }
  });
});
app.get('/api/changerequest', (req, res) => { // เปลี่ยนผู้ขอ
  const navyid = req.query.navyid;
  const Selectid = req.query.Selectid;
  let queryString = ``;

  queryString = `update request set selectcode = 0 where request.navyid=${navyid} `
  pool.query(queryString, function (err, rows, fields) {
    // set request.selectcode = 0 ทั้งหมด
    if (err) {
      res.json({ error: err })// ส่งค่า error กลับไป
      throw err;
    } else {
      queryString = `update request set selectcode = 0 where request.navyid=${navyid} `
      pool.query(queryString, function (err, rows, fields) {
        // set request.selectcode = 0 ทั้งหมด
        if (err) {
          res.json({ error: err })// ส่งค่า error กลับไป
          throw err;
        } else {
          queryString = `
          update person set person.unit3 = 0,person.unit0 = "",person.ask = "",person.number = "",person.rem = "",person.rem2 = "" ,person.moveunit_type = null , person.moveunit_oldtype = null where person.navyid=${navyid}`
          pool.query(queryString, function (err, rows, fields) {
            // set request.selectcode = 0 ทั้งหมด
            if (err) {
              res.json({ error: err })// ส่งค่า error กลับไป
              throw err;
            } else {
              queryString = `select request.navyid,request.unit,request.num,request.askby,request.remark,request.remark2,request.selectid from request where request.navyid=${navyid} and request.selectid=${Selectid}`
              pool.query(queryString, function (err, rows, fields) {
                // เลือก ผู้ขอทั้งหมด จาก ทหาร 1 คน
                if (err) {
                  res.json({ error: err })// ส่งค่า error กลับไป
                  throw err;
                }
                if (rows.length > 0) {
                  console.log(rows)
                  var unit0 = rows[0].unit
                  var ask = rows[0].askby
                  var number = rows[0].num
                  var rem = rows[0].remark
                  var rem2 = rows[0].remark2
                  var selectid = rows[0].selectid

                  queryString = `update request set request.selectcode = 1 where request.selectid = ${selectid} and request.navyid = ${navyid}`
                  pool.query(queryString, function (err, rows, fields) {
                    // เลือกคนขอที่ selectid
                    if (err) {
                      res.json({ error: err })// ส่งค่า error กลับไป
                      throw err;
                    }
                  });
                  queryString = `update person set person.unit3 = ${parseInt(unit0)},person.unit0 = ${parseInt(unit0)},person.ask = '${ask}',person.number = ${parseInt(number)},person.rem = '${rem}',person.rem2 = '${rem2}',moveunit_type = "Request",moveunit_oldtype = (select moveunit_type)  where person.navyid=${parseInt(navyid)}`
                  console.log(queryString)
                  pool.query(queryString, function (err, rows, fields) {
                    // เลือกคนขอที่ selectid
                    if (err) {
                      res.json({ error: err })// ส่งค่า error กลับไป
                      throw err;
                    } else {
                      res.json({ success: "complete" })
                    }
                  });
                } else { }
              });
            }
          });
        }
      });
    }
  });
});



///////////////////////////////////////// tab tools extend person ////////////////////////////////
app.get('/api/incident', (req, res) => { // คิวรี่ เหตุ
  let queryString = ``;

  queryString = `select s.title,s.statuscode,count(p.navyid) count from statustab s 
  inner join person p on p.statuscode = s.statuscode
  where s.statuscode != 'AA'
  group by s.statuscode
  order by s.statuscode`

  pool.query(queryString, function (err, rows, fields) {
    if (err) {
      res.json({ error: err })// ส่งค่า error กลับไป
      throw err;
    }
    if (rows.length > 0) {
      res.json(rows);
    } else {
      res.json([]); // .ใช้ในกรณีที่ไม่พบข้อมูล ให้่ส่งค่าว่างกลับไป
    }
  });
});
app.get('/api/unittab', (req, res) => { // คิวรี่ หน่วย
  let queryString = ``;

  queryString = `select refnum as value,unitname as label,concat(refnum,unitname) as 'values' from unittab where movestat=1`

  pool.query(queryString, function (err, rows, fields) {
    if (err) {
      res.json({ error: err })// ส่งค่า error กลับไป
      throw err;
    }
    if (rows.length > 0) {
      res.json(rows);
    } else {
      res.json([]); // .ใช้ในกรณีที่ไม่พบข้อมูล ให้่ส่งค่าว่างกลับไป
    }
  });
});
app.get('/api/positiontab', (req, res) => { //ตำแหน่งคัดเลือก
  let queryString = ``;

  queryString = `select postcode as value,postname as label from positiontab`

  pool.query(queryString, function (err, rows, fields) {
    if (err) {
      res.json({ error: err })// ส่งค่า error กลับไป
      throw err;
    }
    if (rows.length > 0) {
      res.json(rows);
    } else {
      res.json([]); // .ใช้ในกรณีที่ไม่พบข้อมูล ให้่ส่งค่าว่างกลับไป
    }
  });
});
app.get('/api/statustab', (req, res) => {
  let queryString = ``;
  queryString = `select statuscode as value,title as label from statustab`

  pool.query(queryString, function (err, rows, fields) {
    if (err) {
      res.json({ error: err })// ส่งค่า error กลับไป
      throw err;
    }
    if (rows.length > 0) {
      res.json(rows);
    } else {
      res.json([]); // .ใช้ในกรณีที่ไม่พบข้อมูล ให้่ส่งค่าว่างกลับไป
    }
  });
});
app.get('/api/armtown', (req, res) => { //จังหวัด
  let queryString = ``;
  queryString = `select armtown.armid as value,armtown.armname as label from armtown inner join person on person.armid = armtown.armid group by armtown.armid order by armtown.legion desc,armtown.armid`

  pool.query(queryString, function (err, rows, fields) {
    if (err) {
      res.json({ error: err })// ส่งค่า error กลับไป
      throw err;
    }
    if (rows.length > 0) {
      res.json(rows);
    } else {
      res.json([]); // .ใช้ในกรณีที่ไม่พบข้อมูล ให้่ส่งค่าว่างกลับไป
    }
  });
});
app.get('/api/religion', (req, res) => { //จังหวัด
  let queryString = ``;
  queryString = `select regcode as value,religion as label from religion`

  pool.query(queryString, function (err, rows, fields) {
    if (err) {
      res.json({ error: err })// ส่งค่า error กลับไป
      throw err;
    }
    if (rows.length > 0) {
      res.json(rows);
    } else {
      res.json([]); // .ใช้ในกรณีที่ไม่พบข้อมูล ให้่ส่งค่าว่างกลับไป
    }
  });
});


////////////////////////////////////////////////// ยอดตามตารางต่างๆ ///////////////////////////////////////////////////////
app.get('/api/hisrequest', (req, res) => {
  let queryString = ``;

  queryString = `select unittab.refnum,count(request.unit) as c,amount.count,
      (CONVERT(amount.count,INTEGER) - CONVERT(count(request.unit),INTEGER)) as total,"request" as groupCount from unittab
      left join person on person.unit3 = unittab.refnum 
      left JOIN request on person.NAVYID = request.NAVYID and request.selectcode >= 1 and person.unit3 = request.unit
      left join amount on amount.unit = unittab.refnum
      where movestat=1
      group by unittab.refnum
      Union all
      select unittab.refnum,count(person.navyid) as c,amount.count,
      (CONVERT(amount.count,INTEGER) - CONVERT(count(person.navyid),INTEGER)) as total,"incident" from unittab
      left JOIN person on person.unit3 = unittab.refnum and person.STATUSCODE != 'AA'
      left join amount on amount.unit = unittab.refnum
      where movestat=1
      group by unittab.refnum
      Union all
      select unittab.refnum,count(person.navyid) as c,amount.count,
      (CONVERT(amount.count,INTEGER) - CONVERT(count(person.navyid),INTEGER)) as total,"selectexam" from unittab
      left join selectexam on selectexam.unit4 = unittab.refnum 
			LEFT JOIN person on selectexam.navyid = person.navyid and person.unit3=0
      left join amount on amount.unit = unittab.refnum
      where movestat=1
      group by unittab.refnum
      Union all
      select unittab.refnum,count(person.navyid) as c,amount.count,
      (CONVERT(amount.count,INTEGER) - CONVERT(count(person.navyid),INTEGER)) as total,"religion" from unittab
      LEFT JOIN person on person.unit3 = unittab.refnum and person.regcode = 2
      left join amount on amount.unit = unittab.refnum
      where movestat=1
      group by unittab.refnum
      Union all
      select unittab.refnum,count(person.navyid) as c,amount.count,
      (CONVERT(amount.count,INTEGER) - CONVERT(count(person.navyid),INTEGER)) as total,"unit1" from unittab
      LEFT JOIN person on person.unit1 = unittab.refnum and person.unit3 = 0
      left join amount on amount.unit = unittab.refnum
      where movestat=1
      group by unittab.refnum
      UNION ALL
      select unittab.refnum,count(person.navyid) as c,amount.count,
      (CONVERT(amount.count,INTEGER) - CONVERT(count(person.navyid),INTEGER)) as total,'unit2'  from unittab
      LEFT JOIN person on person.unit2 = unittab.refnum and person.unit3 = 0
      left join amount on amount.unit = unittab.refnum
      where movestat=1
      group by unittab.refnum
      UNION ALL
      select unittab.refnum,count(person.navyid) as c,amount.count,
      (CONVERT(amount.count,INTEGER) - CONVERT(count(person.navyid),INTEGER)) as total,'unit3'  from unittab
      LEFT JOIN person on person.unit3 = unittab.refnum
      left join amount on amount.unit = unittab.refnum
      where movestat=1
      group by unittab.refnum
      UNION ALL
      select "amount",sum(count) as c,"","",'totalamount'  from amount
      UNION ALL
      select "request",count(person.navyid) as c,"","",'totalrequest'  from person
      INNER JOIN request on person.navyid = request.navyid and request.unit = person.unit3 where request.selectcode >= 1
      UNION ALL
      select "incident",count(person.navyid) as c,"","",'totalincident'  from person where person.STATUSCODE!="AA"
      UNION ALL
      select "selectexam",count(person.navyid) as c,"","",'totalselectexam'  from person 
      INNER JOIN selectexam on person.navyid = selectexam.navyid where person.unit3 = 0
      UNION ALL
      select "religion",count(person.navyid) as c,"","",'totalreligion'  from person where person.REGCODE = 2
      UNION ALL
      select "unit1",count(person.unit1) as c,"","",'totalunit1'  from person where person.unit3 = 0
      UNION ALL
      select "unit2",count(person.unit2) as c,"","",'totalunit2'  from person where person.unit3 = 0 
      UNION ALL
      select "unit3",count(person.unit3) as c,"","",'totalunit3'  from person
      `

  pool.query(queryString, function (err, rows, fields) {

    if (err) {
      console.log(err)

      throw err;
    }
    if (rows.length > 0) {
      res.json(rows);
    } else {
      res.json([]);
    }
  });
});
app.get('/api/hisrequest_request', (req, res) => {  // ยอด ร้องขอ
  let queryString = ``;
  queryString = `select unittab.refnum,unittab.unitname,count(person.navyid) as c,amount.count,
      (CONVERT(amount.count,INTEGER) - CONVERT(sum(if(person.unit3 = request.unit,1,0)),INTEGER)) as total,sum(if(person.unit3 = request.unit,1,0)) count2,sum(if(person.unit3 != request.unit,1,0)) count3,"request" as groupCount from unittab
      left join request on request.unit = unittab.refnum and request.selectcode >= 1
      left JOIN person on person.NAVYID = request.NAVYID 
      left join amount on amount.unit = unittab.refnum
      where movestat=1
      group by unittab.refnum
      `
  pool.query(queryString, function (err, rows, fields) {
    if (err) {
      res.json({ error: err })
      throw err;
    }
    if (rows.length > 0) {
      res.json(rows);
    } else {
      res.json([]);
    }
  });
});
app.get('/api/hisrequest_incident', (req, res) => {  // ยอด มีเหตุ
  let queryString = ``;
  queryString = `select unittab.refnum,count(person.navyid) as c,amount.count,
  (CONVERT(amount.count,INTEGER) - CONVERT(count(person.navyid),INTEGER)) as total,"incident" as groupCount from unittab
  left JOIN person on person.unit3 = unittab.refnum and person.STATUSCODE != 'AA'
  left join amount on amount.unit = unittab.refnum
  where movestat=1
  group by unittab.refnum
      `
  pool.query(queryString, function (err, rows, fields) {
    if (err) {
      res.json({ error: err })
      throw err;
    }
    if (rows.length > 0) {
      res.json(rows);
    } else {
      res.json([]);
    }
  });
});
app.get('/api/hisrequest_selectexam', (req, res) => {  // ยอด คัดเลือกหน่วย
  let queryString = ``;
  queryString = `select unittab.refnum,count(person.navyid) as c,amount.count,
  (CONVERT(amount.count,INTEGER) - CONVERT(count(person.navyid),INTEGER)) as total,"selectexam" as groupCount from unittab
  left join selectexam on selectexam.unit4 = unittab.refnum 
  LEFT JOIN person on selectexam.navyid = person.navyid
  left join amount on amount.unit = unittab.refnum
  where movestat=1
  group by unittab.refnum`
  pool.query(queryString, function (err, rows, fields) {
    if (err) {
      res.json({ error: err })
      throw err;
    }
    if (rows.length > 0) {
      res.json(rows);
    } else {
      res.json([]);
    }
  });
});
app.get('/api/hisrequest_province', (req, res) => {  // ยอด จังหวัด
  var armid = req.query.armid
  if (!armid) {
    res.json({ error: "Missing parameter" })
  }
  let queryString = ``;
  queryString = `select unittab.refnum,count(person.navyid) as c,CONVERT(amount.percent,INTEGER) as count,
  (CONVERT(amount.percent,INTEGER) - count(person.navyid)) as total,'province' as groupCount  from unittab
  LEFT JOIN person on person.unit3 = unittab.refnum and person.armid in(${armid})
  left join amount on amount.unit = unittab.refnum
  where movestat=1
  group by unittab.refnum
  order by unittab.refnum
      `
  pool.query(queryString, function (err, rows, fields) {
    if (err) {
      res.json({ error: err })
      throw err;
    }
    if (rows.length > 0) {
      res.json(rows);
    } else {
      res.json([]);
    }
  });
});
app.get('/api/hisrequest_all', (req, res) => {  // ยอด all
  let queryString = ``;
  queryString = `select unittab.refnum,count(person.navyid) as c,amount.count,
  (CONVERT(amount.count,INTEGER) - CONVERT(count(person.navyid),INTEGER)) as total,'unit3' as groupCount  from unittab
  LEFT JOIN person on person.unit3 = unittab.refnum
  left join amount on amount.unit = unittab.refnum
  where movestat=1
  group by unittab.refnum`
  pool.query(queryString, function (err, rows, fields) {
    if (err) {
      res.json({ error: err })
      throw err;
    }
    if (rows.length > 0) {
      res.json(rows);
    } else {
      res.json([]);
    }
  });
});
app.get('/api/hisrequest_selectexam2', (req, res) => {  // ยอด ร้องขอ
  let queryString = ``;
  queryString = `select positiontab.postname,u.unitname,positiontab.Postcode,u.refnum,count(p.navyid) count1,sum(if(p.unit3 = selectexam.unit4,1,0)) count2,sum(if(p.unit3 != selectexam.unit4,1,0)) count3 from selectexam 
  left join person p on p.navyid = selectexam.navyid
  left join unittab u on u.refnum = selectexam.unit4
  left join positiontab on positiontab.POSTCODE = selectexam.Postcode
  GROUP BY selectexam.Postcode,selectexam.unit4
  ORDER BY selectexam.Postcode,selectexam.unit4`
  pool.query(queryString, function (err, rows, fields) {
    if (err) {
      res.json({ status: 500, error: error, response: null })
    }
    else {
      res.json({ status: 200, error: null, response: rows })
    }
  });
});
app.get('/api/hisrequest_belong', (req, res) => {  // ยอด ร้องขอ
  let queryString = ``;
  queryString = `select u.unitname,u.refnum,a.count,count(p.navyid) count1,(a.count-count(p.navyid)) total from unittab u
  left join person p on p.unit3 = u.refnum and p.batt = 5 and p.oldyearin is null
  left join amount a on a.unit = u.refnum
                  where u.movestat = 1
                  GROUP BY u.refnum
                  ORDER BY u.refnum`
  pool.query(queryString, function (err, rows, fields) {
    if (err) {
      res.json({ status: 500, error: err, response: null })
    }
    else {
      res.json({ status: 200, error: null, response: rows })
    }
  });
});
app.get('/api/groupcount', (req, res) => {
  let queryString = `select u.refnum,u.unitname,
  sum(if(p.statuscode!="AA",1,0)) as incident,
  
  sum(if(s.unit4 = p.unit3 and p.STATUSCODE = "AA",1,0)) selectexam, /*คัดเลือกไม่ซ้ำ*/
  sum(if(s.unit4 = p.unit3 and p.STATUSCODE!= "AA",1,0)) selectexam_incident, /*คัดเลือกซ้ำมีเหตุ*/
  
  sum(if(armid in (25,32,45) and p.STATUSCODE= "AA" and s.unit4 != p.unit3,1,0)) province, /*5จชตไม่ซ้ำ*/
  sum(if(armid in (25,32,45) and p.STATUSCODE!= "AA" and s.unit4 != p.unit3,1,0)) province_incident,  /*5จชตซ้ำมีเหตุ*/
  sum(if(armid in (25,32,45) and p.STATUSCODE= "AA" and s.unit4 = p.unit3,1,0)) province_selectexam, /*5จชตซ้ำคัดเลือก*/
  
  sum(if(r.navyid is not null and p.STATUSCODE = "AA" and s.unit4 != p.unit3,1,0) and armid not in (25,32,45)) as request, /*ร้องขอไม่ซ้ำ*/
  sum(if(r.navyid is not null and p.STATUSCODE!= "AA" and s.unit4 != p.unit3,1,0) and armid not in (25,32,45)) as request_incident,	/*ร้องขอซ้ำมีเหตุ*/
  sum(if(r.navyid is not null and p.STATUSCODE= "AA" and s.unit4 = p.unit3,1,0) and armid not in (25,32,45)) as request_selectexam, /*ร้องขอซ้ำซ้ำคัดเลือก*/
  sum(if(r.navyid is not null and p.STATUSCODE= "AA" and s.unit4 != p.unit3,1,0) and armid  in (25,32,45)) as request_province, /*ร้องขอซ้ำ5 จชต*/
  
  sum(if(p.statuscode="AA" and s.unit4 != p.unit3 and armid not in (25,32,45) and r.navyid is null,1,0)) as "change",
  
  count(p.navyid) as "total"
  
  from person p
  left join unittab u on u.refnum = p.unit3
  left join request r on r.navyid = p.navyid and r.selectcode >= 1
  left join selectexam s on s.navyid = p.navyid
  GROUP BY u.REFNUM
  ORDER BY u.REFNUM`;
  log("Group Count", queryString);
  pool.query(queryString, function (err, rows, fields) {
    if (err) {
      res.json({ status: 500, error: err, response: queryString })// ส่งค่า error กลับไป
    }
    else if (rows.length > 0) {
      res.json({ status: 200, error: null, response: rows })// ส่งค่า error กลับไป
    }
    else {
      res.json({ status: 300, error: null, response: queryString })// ส่งค่า error กลับไป
    }
  })
});
app.get('/api/test', (req, res) => {
  test(req, res);
})
app.listen(app.get('port'), () => {

  console.log(`Find the server at: http://localhost:${app.get('port')}/`); // eslint-disable-line no-console
});




///////////////////////////////////////// function helper /////////////////////////////////
function test(req, res) {
  res.json({ status: 200, response: "ทดสอบ" });
}
function process_request(req, res, type) {
  const navyid = req.query.navyid;
  const Unit = req.query.Unit;
  const Askcode = req.query.Askcode;
  const Askname = req.query.Askname;
  const Askby = Askcode + " " + Askname;
  const Num = req.query.Num;
  const Remark = req.query.Remark;
  const Remark2 = req.query.Remark2;
  const Selectid = req.query.Selectid;
  let queryString = ``;
  log("Request", type);
  //
  queryString = `select * from request where request.navyid=${navyid} and lockselectcode = 1`;
  log("Request", queryString);
  pool.query(queryString, function (err, rowsdddddd, fields) {
    if (err) {
      res.json({ status: 500, error: err, response: queryString })// ส่งค่า error กลับไป
    } else if (rowsdddddd.length > 0) {
      queryString = `update request set selectcode = 0 where request.navyid=${navyid} and lockselectcode != 1`
      log("Request", queryString);
      pool.query(queryString, function (err, rows, fields) {
        if (err) {
          res.json({ status: 500, error: err, response: queryString })// ส่งค่า error กลับไป
        } else {
          res.json({ status: 200, error: null, response: queryString })
          return
        }
      })
    }
    else {
      queryString = `update person set person.unit3 = 0,person.unit0 = "",person.ask = "",person.number = "",person.rem = "",person.rem2 = "",person.moveunit_type = "" where person.navyid=${navyid}`
      log("Request", queryString);
      pool.query(queryString, function (err, rows, fields) {
        if (err) {
          res.json({ status: 500, error: err, response: queryString })// ส่งค่า error กลับไป
        } else {

          queryString = `update request set selectcode = 0 where request.navyid=${navyid} `
          log("Request", queryString);
          pool.query(queryString, function (err, rows, fields) {
            if (err) {
              res.json({ status: 500, error: err, response: queryString })// ส่งค่า error กลับไป
            } else {

              if (type == "change") {
                queryString = `select request.navyid,request.unit,request.num,request.askby,request.remark,request.remark2,request.selectid from request where request.navyid=${navyid} and request.selectid=${Selectid}`
              }
              else if (type == "delete") {
                log("Request", JSON.stringify(rows));
                if (rows.affectedRows == 0) {
                  res.json({ status: 200, error: null, response: queryString })
                  return
                }
                else {
                  queryString = `select request.navyid,request.unit,request.num,request.askby,request.remark,request.remark2,request.selectid from request where request.navyid=${navyid}  order by substring(request.askby,1,2) asc,request.num asc `
                }
              }
              else {
                queryString = `select request.navyid,request.unit,request.num,request.askby,request.remark,request.remark2,request.selectid from request where request.navyid=${navyid}  order by substring(request.askby,1,2) asc,request.num asc `
              }
              log("Request", queryString);
              pool.query(queryString, function (err, rows, fields) {
                if (err) {
                  res.json({ status: 500, error: err, response: queryString })
                }
                else if (rows.length > 0) {
                  var unit0 = rows[0].unit
                  var ask = rows[0].askby
                  var number = rows[0].num
                  var rem = rows[0].remark
                  var rem2 = rows[0].remark2
                  var selectid = rows[0].selectid

                  queryString = `update request set request.selectcode = 1 where request.selectid = ${selectid} and request.navyid = ${navyid}`
                  log("Request", queryString);
                  pool.query(queryString, function (err, rows, fields) {
                    if (err) {
                      res.json({ status: 500, error: err, response: queryString })
                    }
                    else {
                      queryString = `update person set person.unit3 = ${parseInt(unit0)},person.unit0 = ${parseInt(unit0)},person.ask = '${ask}',person.number = ${parseInt(number)},person.rem = '${rem}',person.rem2 = '${rem2}',moveunit_type = "Request",moveunit_oldtype = (select moveunit_type)  where person.navyid=${parseInt(navyid)}`
                      log("Request", queryString);
                      pool.query(queryString, function (err, rows, fields) {
                        if (err) {
                          res.json({ status: 500, error: err, response: queryString })
                        } else {
                          res.json({ status: 200, error: null, response: rows })
                          return
                        }
                      });
                    }
                  });
                }
                else {
                  res.json({ status: 300, error: null, response: queryString })
                  return
                }
              });
            }
          });
        }
      });
    }
  });
}
function log(head, message) {
  var totime = new Date();
  var hh = totime.getHours();
  var mm = totime.getMinutes();
  var ss = totime.getSeconds();
  if (hh < 10) {
    hh = '0' + hh
  }

  if (mm < 10) {
    mm = '0' + mm
  }
  if (ss < 10) {
    ss = '0' + ss
  }
  totime = hh + ':' + mm + ':' + ss;
  console.log("[" + totime + "]: " + head + " => " + message);
}

