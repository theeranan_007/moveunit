import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

import {Router, Route} from 'react-router'
import createHistory from 'history/createBrowserHistory'
import {createStore, applyMiddleware} from 'redux'
import {Provider} from 'react-redux'
import createSagaMiddleware from 'redux-saga'
import mainReducer from './Reducers'
import watchFetchSearchData from './fetch/Person.js'
import watchFetchCheckItemSelectexamData from './fetch/CheckItemSelectexam.js'
import watchFetchModalData from './fetch/DetailModal.js'
import watchFetchAmountData from './fetch/Amount.js'
import watchFetchUpdateAmountData from './fetch/UpdateAmount.js'
import watchFetchTableData from './fetch/TableData.js'
import watchFetchCounteducationData from './fetch/Counteducation.js'
import watchFetchCountskillData from './fetch/Countskill.js'
import watchFetchRequestData from './fetch/Request.js'
import watchFetchInsertRequestData from './fetch/InsertRequest.js'
import watchFetchInsertSelectexamData from './fetch/InsertSelectexam.js'
import watchFetchEditRequestData from './fetch/EditRequest.js'
import watchFetchDeleteRequestData from './fetch/DeleteRequest.js'
import watchFetchDeleteSelectexamData from './fetch/DeleteSelectexam.js'
import watchFetchProvidecountData from './fetch/Providecount.js'
import watchFetchReligionProvidecountData from './fetch/ReligionProvidecount.js'
import watchFetchChangeRequestData from './fetch/ChangeRequest.js'
import watchFetchUnittabData from './fetch/Unittab.js'
import watchFetchArmTownData from './fetch/Armtown.js'
import watchFetchIncidentData from './fetch/Incident.js'
import watchFetchPositiontabData from './fetch/Positiontab.js'
import watchFetchStatustabData from './fetch/Statustab.js'
import watchFetchChangeUnit3 from './fetch/ChangeUnit3.js'
import watchFetchChangeAllUnit3 from './fetch/ChangeAllUnit3.js'
import watchFetchHisrequestData from './fetch/Hisrequest.js'
import watchFetchHisrequestRequestData from './fetch/Hisrequest_request.js'
import watchFetchHisrequestIncidentData from './fetch/Hisrequest_incident.js'
import watchFetchHisrequestbelongData from './fetch/Hisrequest_belong.js'
import watchFetchHisrequestSelectexamData from './fetch/Hisrequest_selectexam.js'
import watchFetchHisrequestSelectexam2Data from './fetch/Hisrequest_selectexam2.js'
import watchFetchHisrequestProvinceData from './fetch/Hisrequest_province.js'
import watchFetchHisrequestAllData from './fetch/Hisrequest_all.js'
import watchFetchNumrequesttData from './fetch/NumRequest.js'
import watchFetchCheckNumrequesttData from './fetch/CheckNumRequest.js'
import watchFetchRequestReportData from './fetch/RequestReport.js'
import watchFetchGroupCountData from './fetch/GroupCount.js'
import watchFetchLockRequest from './fetch/LockRequest.js'

import Request from './view/Request.js';
import Incident from './view/Incident.js';
import Selectexam from './view/Selectexam.js';
import Education from './view/Education.js';
import Province from './view/Province.js';
import Height from './view/Height.js';
import Belong from './view/Belong.js';
import Change from './view/Change.js';
import Group from './view/Group.js';

import './index.css';
import 'bootstrap/dist/css/bootstrap.css';
import styles from './example.scss';
import "react-filter-box/lib/react-filter-box.css";
import 'react-select/dist/react-select.css';
import 'font-awesome/css/font-awesome.min.css';
import 'bootstrap/dist/css/bootstrap-theme.css';
import 'react-bootstrap-table/dist/react-bootstrap-table-all.min.css'
import 'react-bootstrap-toggle/dist/bootstrap2-toggle.css';
import "react-toggle-switch/dist/css/switch.min.css"
// import 'react-table-filter/lib/styles.css';
// import 'react-table-filter/src/tableFilter.scss'
import 'antd/dist/antd.css';
//history
const history = createHistory()

//saga middleware
const sagaMiddleware = createSagaMiddleware()

//redux store with saga middleware
const store = createStore(mainReducer, applyMiddleware(sagaMiddleware))
// activate the saga(s)
watchFetchProvidecountData
sagaMiddleware.run(watchFetchSearchData)
sagaMiddleware.run(watchFetchLockRequest)
sagaMiddleware.run(watchFetchCheckItemSelectexamData)
sagaMiddleware.run(watchFetchProvidecountData)
sagaMiddleware.run(watchFetchReligionProvidecountData)
sagaMiddleware.run(watchFetchModalData)
sagaMiddleware.run(watchFetchArmTownData)
sagaMiddleware.run(watchFetchAmountData)
sagaMiddleware.run(watchFetchUpdateAmountData)
sagaMiddleware.run(watchFetchRequestData)
sagaMiddleware.run(watchFetchInsertRequestData)
sagaMiddleware.run(watchFetchInsertSelectexamData)
sagaMiddleware.run(watchFetchEditRequestData)
sagaMiddleware.run(watchFetchDeleteRequestData)
sagaMiddleware.run(watchFetchDeleteSelectexamData)
sagaMiddleware.run(watchFetchTableData)
sagaMiddleware.run(watchFetchCountskillData)
sagaMiddleware.run(watchFetchCounteducationData)
sagaMiddleware.run(watchFetchChangeRequestData)
sagaMiddleware.run(watchFetchUnittabData)
sagaMiddleware.run(watchFetchIncidentData)
sagaMiddleware.run(watchFetchPositiontabData)
sagaMiddleware.run(watchFetchStatustabData)
sagaMiddleware.run(watchFetchChangeUnit3)
sagaMiddleware.run(watchFetchChangeAllUnit3)
sagaMiddleware.run(watchFetchHisrequestData)
sagaMiddleware.run(watchFetchHisrequestRequestData)
sagaMiddleware.run(watchFetchHisrequestIncidentData)
sagaMiddleware.run(watchFetchHisrequestSelectexamData)
sagaMiddleware.run(watchFetchHisrequestSelectexam2Data)
sagaMiddleware.run(watchFetchHisrequestbelongData)
sagaMiddleware.run(watchFetchHisrequestProvinceData)
sagaMiddleware.run(watchFetchHisrequestAllData)
sagaMiddleware.run(watchFetchNumrequesttData)
sagaMiddleware.run(watchFetchCheckNumrequesttData)
sagaMiddleware.run(watchFetchRequestReportData)
sagaMiddleware.run(watchFetchGroupCountData)




//fetch initial data

store.subscribe(()=>{
  // console.log('store: ' + JSON.stringify(store.getState()))
})
// store.dispatch({type: "FETCH_UNITTAB_DATA"})
// store.dispatch({type: "FETCH_HISREQUEST_DATA"})

ReactDOM.render(
  <Provider store={store}>
  <Router history={history}>
    <div>
      <Route path='/' component={App}></Route>
      
      <Route path="/request" component={Request}/>
      <Route path="/incident" component={Incident}/>
      <Route path="/selectexam" component={Selectexam}/>
      <Route path="/education" component={Education}/>
      <Route path="/provide" component={Province}/>
      <Route path="/height" component={Height}/>
      <Route path="/change" component={Change}/>
      <Route path="/belong" component={Belong}/>
      <Route path="/group_report" component={Group}/>
    </div>
  </Router>
</Provider>, document.getElementById('root'));
