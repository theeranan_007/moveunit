import { takeEvery } from 'redux-saga'
import { call, put } from 'redux-saga/effects'
import queryUnittab from '../queryclient/queryUnittab';


function * fetchSearchData(action) {

const unittabData = yield call(queryUnittab.search);
const result = yield put({type: "CHANGE_UNITTAB_DATA", data: unittabData});
  console.log("getUnittabData");

  // if it is from a redux-action, we get an object with error set not a thrown error
  if (result !== undefined) {
    const { error } = result;
    if (error) {
      throw result;
    }
  }
  return result;
}

function * watchFetchUnittabData() {
yield * takeEvery("FETCH_UNITTAB_DATA", fetchSearchData);
}
export default watchFetchUnittabData;
