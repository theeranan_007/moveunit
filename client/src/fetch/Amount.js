import { takeEvery } from 'redux-saga'
import { call, put } from 'redux-saga/effects'
import queryAmount from '../queryclient/queryAmount';


function * fetchSearchData(action) {

const Data = yield call(queryAmount.search,action.payload.armid);
const result = yield put({type: "CHANGE_AMOUNT_DATA", data: Data});
  console.log("getUnittabData");

  // if it is from a redux-action, we get an object with error set not a thrown error
  if (result !== undefined) {
    const { error } = result;
    if (error) {
      throw result;
    }
  }
  return result;
}

function * watchFetchAmountData() {
yield * takeEvery("FETCH_AMOUNT_DATA", fetchSearchData);
}
export default watchFetchAmountData;
