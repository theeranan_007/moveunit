import { takeEvery } from 'redux-saga'
import { call, put } from 'redux-saga/effects'
import queryCountskill from '../queryclient/queryCountskill';


function * fetchSearchData(action) {

const Data = yield call(queryCountskill.search);
const result = yield put({type: "CHANGE_COUNTSKILL_DATA", data: Data});
  console.log("getUnittabData");

  // if it is from a redux-action, we get an object with error set not a thrown error
  if (result !== undefined) {
    const { error } = result;
    if (error) {
      throw result;
    }
  }
  return result;
}

function * watchFetchCountskillData() {
yield * takeEvery("FETCH_COUNTSKILL_DATA", fetchSearchData);
}
export default watchFetchCountskillData;
