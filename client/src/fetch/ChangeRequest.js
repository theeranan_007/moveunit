import { takeEvery } from 'redux-saga'
import { call, put } from 'redux-saga/effects'
import queryChangeRequest from '../queryclient/queryChangeRequest';


function * fetchSearchData(action) {

const requestData = yield call(queryChangeRequest.search, action.payload.navyid,action.payload.selectid);
const result = yield put({type: "CHANGE_CHANGEREQUEST_DATA", data: requestData});
  console.log("getChangeRequest");
  // if it is from a redux-action, we get an object with error set not a thrown error
  if (result !== undefined) {
    const { error } = result;
    if (error) {
      throw result;
    }
  }
  return result;
}

function * watchFetchChangeRequest() {
yield * takeEvery("FETCH_CHANGEREQUEST_DATA", fetchSearchData);
}
export default watchFetchChangeRequest;
