import { takeEvery } from 'redux-saga'
import { call, put } from 'redux-saga/effects'
import queryReligionProvidecount from '../queryclient/queryReligionProvidecount';


function* fetchSearchData(action) {

  const searchData = yield call(queryReligionProvidecount.search, action.payload.armid);
const result = yield put({type: "CHANGE_RELIGIONPROVIDECOUNT_DATA", data:searchData});
  console.log("getSearchData");

  // if it is from a redux-action, we get an object with error set not a thrown error
  if (result !== undefined) {
    const { error } = result;
    if (error) {
      throw result;
    }
  }
  return result;
}

function* watchFetchReligionProvidecountData(){
  yield* takeEvery("FETCH_RELIGIONPROVIDECOUNT_DATA", fetchSearchData);
}
export default watchFetchReligionProvidecountData;
