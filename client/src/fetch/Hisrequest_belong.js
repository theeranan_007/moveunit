import { takeEvery } from 'redux-saga'
import { call, put } from 'redux-saga/effects'
import queryHisrequest_belong from '../queryclient/queryHisrequest_belong';


function * fetchSearchData(action) {

const Data = yield call(queryHisrequest_belong.search);
const result = yield put({type: "CHANGE_HISREQUESTBELONG_DATA", data: Data});
  console.log("getHisrequestData");

  // if it is from a redux-action, we get an object with error set not a thrown error
  if (result !== undefined) {
    const { error } = result;
    if (error) {
      throw result;
    }
  }
  return result;
}

function * watchFetchHisrequestbelongData() {
yield * takeEvery("FETCH_HISREQUESTBELONG_DATA", fetchSearchData);
}
export default watchFetchHisrequestbelongData;
