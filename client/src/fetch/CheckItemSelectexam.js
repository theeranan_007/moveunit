import { takeEvery } from 'redux-saga'
import { call, put } from 'redux-saga/effects'
import queryCheckItemSelectexam from '../queryclient/queryCheckItemSelectexam';


function * fetchSearchData(action) {
console.log(action)
const getCheckItemSelectexamData = yield call(queryCheckItemSelectexam.search, action.payload.Unit4, action.payload.Postcode);
const result = yield put({type: "CHANGE_CHECKITEMSELECTEXAM_DATA", data: getCheckItemSelectexamData});
  console.log("getCheckItemSelectexamData");

  // if it is from a redux-action, we get an object with error set not a thrown error
  if (result !== undefined) {
    const { error } = result;
    if (error) {
      throw result;
    }
  }
  return result;
}

function * watchFetchCheckItemSelectexamData() {
yield * takeEvery("FETCH_CHECKITEMSELECTEXAM_DATA", fetchSearchData);
}
export default watchFetchCheckItemSelectexamData;
