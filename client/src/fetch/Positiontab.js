import { takeEvery } from 'redux-saga'
import { call, put } from 'redux-saga/effects'
import queryPositiontab from '../queryclient/queryPositiontab';


function * fetchSearchData(action) {

const positiontabData = yield call(queryPositiontab.search);
const result = yield put({type: "CHANGE_POSITIONTAB_DATA", data: positiontabData});
  console.log("getPositiontabData");

  // if it is from a redux-action, we get an object with error set not a thrown error
  if (result !== undefined) {
    const { error } = result;
    if (error) {
      throw result;
    }
  }
  return result;
}

function * watchFetchPositiontabData() {
yield * takeEvery("FETCH_POSITIONTAB_DATA", fetchSearchData);
}
export default watchFetchPositiontabData;
