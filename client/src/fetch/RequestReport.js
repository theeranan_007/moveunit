import { takeEvery } from 'redux-saga'
import { call, put } from 'redux-saga/effects'
import Search_person from '../queryclient/queryRequestReport';


function* fetchSearchData(action) {

  const searchData = yield call(Search_person.search, action.payload.where, action.payload.groupby, action.payload.have, action.payload.sort);
  const result = yield put({type: "CHANGE_REQUESTREPORT_DATA", data:searchData});
  console.log("getSearchData");

  // if it is from a redux-action, we get an object with error set not a thrown error
  if (result !== undefined) {
    const { error } = result;
    if (error) {
      throw result;
    }
  }
  return result;
}

function* watchFetchRequestReportData(){
  yield* takeEvery("FETCH_REQUESTREPORT_DATA", fetchSearchData);
}
export default watchFetchRequestReportData;
