import { takeEvery } from 'redux-saga'
import { call, put } from 'redux-saga/effects'
import queryTableData from '../queryclient/queryTableData';


function* fetchSearchData(action) {
  // yield put({ type: "CHANGE_TABLE_DATA", data: [] });
  const searchData = yield call(queryTableData.search, action.payload.where, action.payload.sort, action.payload.groupby, action.payload.name, action.payload.sname, action.payload.id8);
  const result = yield put({ type: "CHANGE_TABLE_DATA", data: searchData });
  console.log("getTableData");

  // if it is from a redux-action, we get an object with error set not a thrown error
  if (result !== undefined) {
    const { error } = result;
    if (error) {
      throw result;
    }
  }
  return result;
}

function* watchFetchTableData() {
  yield* takeEvery("FETCH_TABLE_DATA", fetchSearchData);
}
export default watchFetchTableData;
