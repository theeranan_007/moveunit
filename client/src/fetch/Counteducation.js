import { takeEvery } from 'redux-saga'
import { call, put } from 'redux-saga/effects'
import queryCounteducation from '../queryclient/queryCounteducation';


function * fetchSearchData(action) {

const Data = yield call(queryCounteducation.search);
const result = yield put({type: "CHANGE_COUNTEDUCATION_DATA", data: Data});
  console.log("getUnittabData");

  // if it is from a redux-action, we get an object with error set not a thrown error
  if (result !== undefined) {
    const { error } = result;
    if (error) {
      throw result;
    }
  }
  return result;
}

function * watchFetchCounteducationData() {
yield * takeEvery("FETCH_COUNTEDUCATION_DATA", fetchSearchData);
}
export default watchFetchCounteducationData;
