import { takeEvery } from 'redux-saga'
import { call, put } from 'redux-saga/effects'
import queryDeleteSelectexam from '../queryclient/queryDeleteSelectexam';


function * fetchSearchData(action) {

const selectexamData = yield call(queryDeleteSelectexam.search, action.payload.navyid,action.payload.unit4,action.payload.postcode,action.payload.item);
const result = yield put({type: "CHANGE_DELETESELECTEXAM_DATA", data: selectexamData});
  console.log("getDeleteSelectexam");
  // if it is from a redux-action, we get an object with error set not a thrown error
  if (result !== undefined) {
    const { error } = result;
    if (error) {
      throw result;
    }
  }
  return result;
}

function * watchFetchDeleteSelectexam() {
yield * takeEvery("FETCH_DELETESELECTEXAM_DATA", fetchSearchData);
}
export default watchFetchDeleteSelectexam;
