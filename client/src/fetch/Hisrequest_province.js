import { takeEvery } from 'redux-saga'
import { call, put } from 'redux-saga/effects'
import queryHisrequest_province from '../queryclient/queryHisrequest_province';


function * fetchSearchData(action) {

const Data = yield call(queryHisrequest_province.search,action.payload.armid);
const result = yield put({type: "CHANGE_HISREQUESTPROVINCE_DATA", data: Data});
  console.log("getHisrequestData");

  // if it is from a redux-action, we get an object with error set not a thrown error
  if (result !== undefined) {
    const { error } = result;
    if (error) {
      throw result;
    }
  }
  return result;
}

function * watchFetchHisrequestProvinceData() {
yield * takeEvery("FETCH_HISREQUESTPROVINCE_DATA", fetchSearchData);
}
export default watchFetchHisrequestProvinceData;
