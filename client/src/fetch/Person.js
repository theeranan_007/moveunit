import { takeEvery } from 'redux-saga'
import { call, put } from 'redux-saga/effects'
import Search_person from '../queryclient/Search_person';


function* fetchSearchData(action) {

  const searchData = yield call(Search_person.search, action.payload.name, action.payload.sname, action.payload.id8);
const result = yield put({type: "CHANGE_SEARCH_DATA", data:searchData});
  console.log("getSearchData");

  // if it is from a redux-action, we get an object with error set not a thrown error
  if (result !== undefined) {
    const { error } = result;
    if (error) {
      throw result;
    }
  }
  return result;
}

function* watchFetchSearchData(){
  yield* takeEvery("FETCH_SEARCH_DATA", fetchSearchData);
}
export default watchFetchSearchData;
