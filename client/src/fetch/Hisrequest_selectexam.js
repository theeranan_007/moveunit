import { takeEvery } from 'redux-saga'
import { call, put } from 'redux-saga/effects'
import queryHisrequest_selectexam from '../queryclient/queryHisrequest_selectexam';


function * fetchSearchData(action) {

const Data = yield call(queryHisrequest_selectexam.search);
const result = yield put({type: "CHANGE_HISREQUESTSELECTEXAM_DATA", data: Data});
  console.log("getHisrequestData");

  // if it is from a redux-action, we get an object with error set not a thrown error
  if (result !== undefined) {
    const { error } = result;
    if (error) {
      throw result;
    }
  }
  return result;
}

function * watchFetchHisrequestselectexamData() {
yield * takeEvery("FETCH_HISREQUESTSELECTEXAM_DATA", fetchSearchData);
}
export default watchFetchHisrequestselectexamData;
