import { takeEvery } from 'redux-saga'
import { call, put } from 'redux-saga/effects'
import queryChangeUnit3 from '../queryclient/queryChangeUnit3';


function * fetchSearchData(action) {

const unittabData = yield call(queryChangeUnit3.search,action.payload.navyid,action.payload.unit3,action.payload.type);
const result = yield put({type: "CHANGE_CHANGEUNIT3_DATA", data: unittabData});
  console.log("getChangeUnit3");

  // if it is from a redux-action, we get an object with error set not a thrown error
  if (result !== undefined) {
    const { error } = result;
    if (error) {
      throw result;
    }
  }
  return result;
}

function * watchFetchChangeUnit3() {
yield * takeEvery("FETCH_CHANGEUNIT3_DATA", fetchSearchData);
}
export default watchFetchChangeUnit3;
