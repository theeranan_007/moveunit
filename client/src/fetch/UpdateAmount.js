import { takeEvery } from 'redux-saga'
import { call, put } from 'redux-saga/effects'
import queryUpdateAmount from '../queryclient/queryUpdateAmount';


function* fetchSearchData(action) {

  const searchData = yield call(queryUpdateAmount.search, action.payload.unit, action.payload.count, action.payload.percent, action.payload.manual_percent);
  const result = yield put({ type: "CHANGE_UPDATEAMOUNT_DATA", data: searchData });
  console.log("getSearchData");

  // if it is from a redux-action, we get an object with error set not a thrown error
  if (result !== undefined) {
    const { error } = result;
    if (error) {
      throw result;
    }
  }
  return result;
}

function* watchFetchUpdateAmountData() {
  yield* takeEvery("FETCH_UPDATEAMOUNT_DATA", fetchSearchData);
}
export default watchFetchUpdateAmountData;
