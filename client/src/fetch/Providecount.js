import { takeEvery } from 'redux-saga'
import { call, put } from 'redux-saga/effects'
import queryProvidecount from '../queryclient/queryProvidecount';


function* fetchSearchData(action) {

  const searchData = yield call(queryProvidecount.search, action.payload.armid);
const result = yield put({type: "CHANGE_PROVIDECOUNT_DATA", data:searchData});
  console.log("getSearchData");

  // if it is from a redux-action, we get an object with error set not a thrown error
  if (result !== undefined) {
    const { error } = result;
    if (error) {
      throw result;
    }
  }
  return result;
}

function* watchFetchProvidecountData(){
  yield* takeEvery("FETCH_PROVIDECOUNT_DATA", fetchSearchData);
}
export default watchFetchProvidecountData;
