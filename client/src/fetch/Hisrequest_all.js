import { takeEvery } from 'redux-saga'
import { call, put } from 'redux-saga/effects'
import queryHisrequest_all from '../queryclient/queryHisrequest_all';


function * fetchSearchData(action) {

const Data = yield call(queryHisrequest_all.search);
const result = yield put({type: "CHANGE_HISREQUESTALL_DATA", data: Data});
  console.log("getHisrequestData");

  // if it is from a redux-action, we get an object with error set not a thrown error
  if (result !== undefined) {
    const { error } = result;
    if (error) {
      throw result;
    }
  }
  return result;
}

function * watchFetchHisrequestAllData() {
yield * takeEvery("FETCH_HISREQUESTALL_DATA", fetchSearchData);
}
export default watchFetchHisrequestAllData;
