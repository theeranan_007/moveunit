import { takeEvery } from 'redux-saga'
import { call, put } from 'redux-saga/effects'
import queryNumrequest from '../queryclient/queryNumrequest';


function * fetchSearchData(action) {
console.log(action)
const numrequestData = yield call(queryNumrequest.search,action.payload.Unit,action.payload.Askcode);
const result = yield put({type: "CHANGE_NUMREQUEST_DATA", data: numrequestData});
  console.log("getNumrequestData");

  // if it is from a redux-action, we get an object with error set not a thrown error
  if (result !== undefined) {
    const { error } = result;
    if (error) {
      throw result;
    }
  }
  return result;
}

function * watchFetchNumRequestData() {
yield * takeEvery("FETCH_NUMREQUEST_DATA", fetchSearchData);
}
export default watchFetchNumRequestData;
