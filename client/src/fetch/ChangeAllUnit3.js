import { takeEvery } from 'redux-saga'
import { call, put } from 'redux-saga/effects'
import queryChangeAllUnit3 from '../queryclient/queryChangeAllUnit3';


function * fetchSearchData(action) {
const changeAllData = yield call(queryChangeAllUnit3.search,action.payload.navyid,action.payload.unit3,action.payload.type);
const result = yield put({type: "CHANGE_CHANGEALLUNIT3_DATA", data: changeAllData});
  console.log("getChangeAllUnit3");

  // if it is from a redux-action, we get an object with error set not a thrown error
  if (result !== undefined) {
    const { error } = result;
    if (error) {
      throw result;
    }
  }
  return result;
}

function * watchFetchChangeAllUnit3() {
yield * takeEvery("FETCH_CHANGEALLUNIT3_DATA", fetchSearchData);
}
export default watchFetchChangeAllUnit3;
