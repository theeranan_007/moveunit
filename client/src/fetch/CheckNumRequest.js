import { takeEvery } from 'redux-saga'
import { call, put } from 'redux-saga/effects'
import queryCheckNumrequest from '../queryclient/queryCheckNumrequest';


function * fetchSearchData(action) {
console.log(action)
const checknumrequestData = yield call(queryCheckNumrequest.search, action.payload.Unit, action.payload.Askcode, action.payload.Num);
const result = yield put({type: "CHANGE_CHECKNUMREQUEST_DATA", data: checknumrequestData});
  console.log("getCheckNumrequestData");

  // if it is from a redux-action, we get an object with error set not a thrown error
  if (result !== undefined) {
    const { error } = result;
    if (error) {
      throw result;
    }
  }
  return result;
}

function * watchFetchCheckNumRequestData() {
yield * takeEvery("FETCH_CHECKNUMREQUEST_DATA", fetchSearchData);
}
export default watchFetchCheckNumRequestData;
