import { takeEvery } from 'redux-saga'
import { call, put } from 'redux-saga/effects'
import queryDeleteRequest from '../queryclient/queryDeleteRequest';


function * fetchSearchData(action) {

const requestData = yield call(queryDeleteRequest.search, action.payload.navyid,action.payload.selectid);
const result = yield put({type: "CHANGE_DELETEREQUEST_DATA", data: requestData});
  console.log("getDeleteRequest");
  // if it is from a redux-action, we get an object with error set not a thrown error
  if (result !== undefined) {
    const { error } = result;
    if (error) {
      throw result;
    }
  }
  return result;
}

function * watchFetchDeleteRequest() {
yield * takeEvery("FETCH_DELETEREQUEST_DATA", fetchSearchData);
}
export default watchFetchDeleteRequest;
