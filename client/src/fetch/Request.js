import { takeEvery } from 'redux-saga'
import { call, put } from 'redux-saga/effects'
import Search_Request from '../queryclient/Search_Request';


function * fetchSearchData(action) {

const requestData = yield call(Search_Request.search, action.payload.navyid);
const result = yield put({type: "CHANGE_REQUEST_DATA", data: requestData});
  console.log("getRequestData");
  // if it is from a redux-action, we get an object with error set not a thrown error
  if (result !== undefined) {
    const { error } = result;
    if (error) {
      throw result;
    }
  }
  return result;
}

function * watchFetchRequestData() {
yield * takeEvery("FETCH_REQUEST_DATA", fetchSearchData);
}
export default watchFetchRequestData;
