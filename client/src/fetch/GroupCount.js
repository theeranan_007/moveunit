import { takeEvery } from 'redux-saga'
import { call, put } from 'redux-saga/effects'
import queryGroupCount from '../queryclient/queryGroupCount';


function * fetchSearchData(action) {

const Data = yield call(queryGroupCount.search);
const result = yield put({type: "CHANGE_GROUPCOUNT_DATA", data: Data});
  console.log("getGROUPCOUNTData");

  // if it is from a redux-action, we get an object with error set not a thrown error
  if (result !== undefined) {
    const { error } = result;
    if (error) {
      throw result;
    }
  }
  return result;
}

function * watchFetchGroupCountData() {
yield * takeEvery("FETCH_GROUPCOUNT_DATA", fetchSearchData);
}
export default watchFetchGroupCountData;
