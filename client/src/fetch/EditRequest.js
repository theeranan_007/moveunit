import { takeEvery } from 'redux-saga'
import { call, put } from 'redux-saga/effects'
import queryEditRequest from '../queryclient/queryEditRequest';


function * fetchSearchData(action) {

const requestData = yield call(queryEditRequest.search, action.payload.navyid, action.payload.unit, action.payload.askcode, action.payload.askname, action.payload.num, action.payload.remark, action.payload.remark2, action.payload.selectid);
const result = yield put({type: "CHANGE_EDITREQUEST_DATA", data: requestData});
  console.log("getEditRequest");
  // if it is from a redux-action, we get an object with error set not a thrown error
  if (result !== undefined) {
    const { error } = result;
    if (error) {
      throw result;
    }
  }
  return result;
}

function * watchFetchEditRequest() {
yield * takeEvery("FETCH_EDITREQUEST_DATA", fetchSearchData);
}
export default watchFetchEditRequest;
