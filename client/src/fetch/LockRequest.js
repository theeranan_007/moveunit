import { takeEvery } from 'redux-saga'
import { call, put } from 'redux-saga/effects'
import queryLockRequest from '../queryclient/queryLockRequest';


function * fetchSearchData(action) {

const requestData = yield call(queryLockRequest.search, action.payload.navyid,action.payload.selectid,action.payload.lockselectcode);
const result = yield put({type: "CHANGE_LOCKREQUEST_DATA", data: requestData});
  console.log("getDeleteRequest");
  // if it is from a redux-action, we get an object with error set not a thrown error
  if (result !== undefined) {
    const { error } = result;
    if (error) {
      throw result;
    }
  }
  return result;
}

function * watchFetchLockRequest() {
yield * takeEvery("FETCH_LOCKREQUEST_DATA", fetchSearchData);
}
export default watchFetchLockRequest;
