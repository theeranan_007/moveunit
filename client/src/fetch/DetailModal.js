import { takeEvery } from 'redux-saga'
import { call, put } from 'redux-saga/effects'
import Search_DetailModal from '../queryclient/Search_DetailModal';


function * fetchSearchData(action) {

  const modalData = yield call(Search_DetailModal.search, action.payload.navyid);
  const result = yield put({type: "CHANGE_MODAL_DATA", data: modalData});
  console.log("getDetailModalData");

  // if it is from a redux-action, we get an object with error set not a thrown error
  if (result !== undefined) {
    const { error } = result;
    if (error) {
      throw result;
    }
  }
  return result;
}

function * watchFetchModalData() {
yield * takeEvery("FETCH_MODAL_DATA", fetchSearchData);
}
export default watchFetchModalData;
