import { takeEvery } from 'redux-saga'
import { call, put } from 'redux-saga/effects'
import queryHisrequest_request from '../queryclient/queryHisrequest_request';


function * fetchSearchData(action) {

const Data = yield call(queryHisrequest_request.search);
const result = yield put({type: "CHANGE_HISREQUESTREQUEST_DATA", data: Data});
  console.log("getHisrequestData");

  // if it is from a redux-action, we get an object with error set not a thrown error
  if (result !== undefined) {
    const { error } = result;
    if (error) {
      throw result;
    }
  }
  return result;
}

function * watchFetchHisrequestRequestData() {
yield * takeEvery("FETCH_HISREQUESTREQUEST_DATA", fetchSearchData);
}
export default watchFetchHisrequestRequestData;
