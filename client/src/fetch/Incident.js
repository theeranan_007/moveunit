import { takeEvery } from 'redux-saga'
import { call, put } from 'redux-saga/effects'
import queryIncident from '../queryclient/queryIncident';


function * fetchSearchData(action) {

const incidentData = yield call(queryIncident.search);
const result = yield put({type: "CHANGE_INCIDENT_DATA", data: incidentData});
  console.log("getUnittabData");

  // if it is from a redux-action, we get an object with error set not a thrown error
  if (result !== undefined) {
    const { error } = result;
    if (error) {
      throw result;
    }
  }
  return result;
}

function * watchFetchIncidentData() {
yield * takeEvery("FETCH_INCIDENT_DATA", fetchSearchData);
}
export default watchFetchIncidentData;
