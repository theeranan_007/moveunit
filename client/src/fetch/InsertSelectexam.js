import { takeEvery } from 'redux-saga'
import { call, put } from 'redux-saga/effects'
import queryInsertSelectexam from '../queryclient/queryInsertSelectexam';


function * fetchSearchData(action) {

const selectexamData = yield call(queryInsertSelectexam.search, action.payload.navyid,action.payload.unit4,action.payload.postcode,action.payload.item);
const result = yield put({type: "CHANGE_INSERTSELECTEXAM_DATA", data: selectexamData});
  console.log("getInsertSelectexam");
  // if it is from a redux-action, we get an object with error set not a thrown error
  if (result !== undefined) {
    const { error } = result;
    if (error) {
      throw result;
    }
  }
  return result;
}

function * watchFetchInsertSelectexam() {
yield * takeEvery("FETCH_INSERTSELECTEXAM_DATA", fetchSearchData);
}
export default watchFetchInsertSelectexam;
