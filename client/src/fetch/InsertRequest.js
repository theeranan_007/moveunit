import { takeEvery } from 'redux-saga'
import { call, put } from 'redux-saga/effects'
import queryInsertRequest from '../queryclient/queryInsertRequest';


function * fetchSearchData(action) {

const requestData = yield call(queryInsertRequest.search, action.payload.navyid, action.payload.unit, action.payload.askcode, action.payload.askname, action.payload.num, action.payload.remark, action.payload.remark2);
const result = yield put({type: "CHANGE_INSERTREQUEST_DATA", data: requestData});
  console.log("getInsertRequest");
  // if it is from a redux-action, we get an object with error set not a thrown error
  if (result !== undefined) {
    const { error } = result;
    if (error) {
      throw result;
    }
  }
  return result;
}

function * watchFetchInsertRequest() {
yield * takeEvery("FETCH_INSERTREQUEST_DATA", fetchSearchData);
}
export default watchFetchInsertRequest;
