import { takeEvery } from 'redux-saga'
import { call, put } from 'redux-saga/effects'
import queryStatustab from '../queryclient/queryStatustab';


function * fetchSearchData(action) {

const statustabData = yield call(queryStatustab.search);
const result = yield put({type: "CHANGE_STATUSTAB_DATA", data: statustabData});
  console.log("getStatustabData");

  // if it is from a redux-action, we get an object with error set not a thrown error
  if (result !== undefined) {
    const { error } = result;
    if (error) {
      throw result;
    }
  }
  return result;
}

function * watchFetchStatustabData() {
yield * takeEvery("FETCH_STATUSTAB_DATA", fetchSearchData);
}
export default watchFetchStatustabData;
