import { takeEvery } from 'redux-saga'
import { call, put } from 'redux-saga/effects'
import queryHisrequest_incident from '../queryclient/queryHisrequest_incident';


function * fetchSearchData(action) {

const Data = yield call(queryHisrequest_incident.search);
const result = yield put({type: "CHANGE_HISREQUESTINCIDENT_DATA", data: Data});
  console.log("getHisrequestData");

  // if it is from a redux-action, we get an object with error set not a thrown error
  if (result !== undefined) {
    const { error } = result;
    if (error) {
      throw result;
    }
  }
  return result;
}

function * watchFetchHisrequestIncidentData() {
yield * takeEvery("FETCH_HISREQUESTINCIDENT_DATA", fetchSearchData);
}
export default watchFetchHisrequestIncidentData;
