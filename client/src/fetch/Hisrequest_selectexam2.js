import { takeEvery } from 'redux-saga'
import { call, put } from 'redux-saga/effects'
import queryHisrequest_selectexam2 from '../queryclient/queryHisrequest_selectexam2';


function * fetchSearchData(action) {

const Data = yield call(queryHisrequest_selectexam2.search);
const result = yield put({type: "CHANGE_HISREQUESTSELECTEXAM2_DATA", data: Data});
  console.log("getHisrequestData");

  // if it is from a redux-action, we get an object with error set not a thrown error
  if (result !== undefined) {
    const { error } = result;
    if (error) {
      throw result;
    }
  }
  return result;
}

function * watchFetchHisrequestselectexam2Data() {
yield * takeEvery("FETCH_HISREQUESTSELECTEXAM2_DATA", fetchSearchData);
}
export default watchFetchHisrequestselectexam2Data;
