import { takeEvery } from 'redux-saga'
import { call, put } from 'redux-saga/effects'
import Search_Selectexam from '../queryclient/Search_Selectexam';


function * fetchSearchData(action) {

const selectexamData = yield call(Search_Selectexam.search, action.payload.position,action.payload.unit4);
const result = yield put({type: "CHANGE_SELECTEXAM_DATA", data: selectexamData});
  console.log("getRequestData");
  // if it is from a redux-action, we get an object with error set not a thrown error
  if (result !== undefined) {
    const { error } = result;
    if (error) {
      throw result;
    }
  }
  return result;
}

function * watchFetchSelectexamData() {
yield * takeEvery("FETCH_SELECTEXAM_DATA", fetchSearchData);
}
export default watchFetchSelectexamData;
