import { takeEvery } from 'redux-saga'
import { call, put } from 'redux-saga/effects'
import queryHisrequest from '../queryclient/queryHisrequest';


function * fetchSearchData(action) {

const hisrequestData = yield call(queryHisrequest.search);
const result = yield put({type: "CHANGE_HISREQUEST_DATA", data: hisrequestData});
  console.log("getHisrequestData");

  // if it is from a redux-action, we get an object with error set not a thrown error
  if (result !== undefined) {
    const { error } = result;
    if (error) {
      throw result;
    }
  }
  return result;
}

function * watchFetchHisrequestData() {
yield * takeEvery("FETCH_HISREQUEST_DATA", fetchSearchData);
}
export default watchFetchHisrequestData;
