import { takeEvery } from 'redux-saga'
import { call, put } from 'redux-saga/effects'
import queryArmTown from '../queryclient/queryArmTown';


function * fetchSearchData(action) {

const armtownData = yield call(queryArmTown.search);
const result = yield put({type: "CHANGE_ARMTOWN_DATA", data: armtownData});
  console.log("getArmtownData");

  // if it is from a redux-action, we get an object with error set not a thrown error
  if (result !== undefined) {
    const { error } = result;
    if (error) {
      throw result;
    }
  }
  return result;
}

function * watchFetchArmTownData() {
yield * takeEvery("FETCH_ARMTOWN_DATA", fetchSearchData);
}
export default watchFetchArmTownData;
