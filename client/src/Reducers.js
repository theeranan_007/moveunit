var initialState = {
  searchData: [],
  tableData: [],
  modalData: [],
  view: [],
  requestData: [],
  insertrequestData: [],
  insertselectexamData: [],
  editrequestData: [],
  deleterequestData: [],
  changerequestData: [],
  unittabData: [],
  incidentData: [],
  statustabData: [],
  positiontabData: [],
  hisrequestData: [],
  hisrequestrequestData: [],
  hisrequestincidentData: [],
  hisrequestselectexamData: [],
  hisrequestselectexam2Data: [],
  hisrequestbelongData: [],
  hisrequestprovinceData: [],
  hisrequestallData: [],
  numrequestData: [],
  checknumrequestData: [],
  changeUnit3: [],
  changeAllUnit3: [],
  requestreportData: [],
  deleteselectexamData: [],
  armtown: [],
  providecount: [],
  updateamountData: [],
  amountData: [],
  religionprovincecountData: [],
  counteducationData: [],
  countskillData: [],
  builderData: [],
  Alerts: [],
  typeOpenModal: [],
  checkItemSelectexamData: [],
  groupcountData: [],
  lockRequestData:[],
};

function mainReducer(state = initialState, action) {
  var alert = { type: "", headline: "", message: "", timeout: '0', position: 'top-right' }
  var view = { showModalBuilder: false, showModalSelectexam: false, showModal: false, requesttab: 1, belongtab: 1, selectexamtab: 1, grouptab: 1, providetab: 1, changetab: 1, incidenttab: 1, educationtab: 1, unit: 0, state: false, where: "", sort: "", orderby: "", have: "", name: "", sname: "", id8: "" }
  switch (action.type) {
    case 'ALERTS':
      if (action.payload.hasOwnProperty('type')) {
        alert.type = action.payload.type
      }
      if (action.payload.hasOwnProperty('headline')) {
        alert.headline = action.payload.headline
      }
      if (action.payload.hasOwnProperty('message')) {
        alert.message = action.payload.message
      }
      if (action.payload.hasOwnProperty('timeout')) {
        alert.timeout = action.payload.timeout
      }
      if (action.payload.hasOwnProperty('position')) {
        alert.position = action.payload.position
      }
      return {
        ...state,
        Alerts: alert
      }
    case 'CHANGE_SEARCH_DATA':
      return {
        ...state,
        searchData: action.data
      }
    case 'CHANGE_CHECKITEMSELECTEXAM_DATA':
      return {
        ...state,
        checkItemSelectexamData: action.data
      }
    case 'CHANGE_TYPEMODAL_DATA':
      return {
        ...state,
        typeOpenModal: action.payload
      }
    case 'CHANGE_MODAL_DATA':
      return {
        ...state,
        modalData: action.data
      }
    case 'CHANGE_TABLE_DATA':
      return {
        ...state,
        tableData: action.data
      }
    case 'CHANGE_COUNTEDUCATION_DATA':
      return {
        ...state,
        counteducationData: action.data
      }
    case 'CHANGE_COUNTSKILL_DATA':
      return {
        ...state,
        countskillData: action.data
      }
    case 'CHANGE_VIEW_DATA':

      if (action.payload.hasOwnProperty('showModal')) {
        view.showModal = action.payload.showModal
      }
      if (action.payload.hasOwnProperty('unit')) {
        view.unit = action.payload.unit
      }
      if (action.payload.hasOwnProperty('providetab')) {
        view.providetab = action.payload.providetab
      }
      if (action.payload.hasOwnProperty('incidenttab')) {
        view.incidenttab = action.payload.incidenttab
      }
      if (action.payload.hasOwnProperty('changetab')) {
        view.changetab = action.payload.changetab
      }
      if (action.payload.hasOwnProperty('selectexamtab')) {
        view.selectexamtab = action.payload.selectexamtab
      }
      if (action.payload.hasOwnProperty('educationtab')) {
        view.educationtab = action.payload.educationtab
      }
      if (action.payload.hasOwnProperty('requesttab')) {
        view.requesttab = action.payload.requesttab
      }
      if (action.payload.hasOwnProperty('belongtab')) {
        view.belongtab = action.payload.belongtab
      }
      if (action.payload.hasOwnProperty('grouptab')) {
        view.grouptab = action.payload.grouptab
      }
      if (action.payload.hasOwnProperty('state')) {
        view.state = action.payload.state
      }
      if (action.payload.hasOwnProperty('showModalSelectexam')) {
        view.showModalSelectexam = action.payload.showModalSelectexam
      }
      if (action.payload.hasOwnProperty('showModalBuilder')) {
        view.showModalBuilder = action.payload.showModalBuilder
      }
      if (action.payload.hasOwnProperty('where') || action.payload.hasOwnProperty('sort') || action.payload.hasOwnProperty('groupby')) {
        view.where = action.payload.where
        view.sort = action.payload.sort
        view.groupby = action.payload.groupby
        view.have = action.payload.have
        view.name = action.payload.name
        view.sname = action.payload.sname
        view.id8 = action.payload.id8
      }
      return {
        ...state,
        view: view
      }
    case 'CHANGE_REQUEST_DATA':
      return {
        ...state,
        requestData: action.data
      }
    case 'CHANGE_INSERTREQUEST_DATA':
      return {
        ...state,
        insertrequestData: action.data
      }
    case 'CHANGE_INSERTSELECTEXAM_DATA':
      return {
        ...state,
        insertselectexamData: action.data
      }
    case 'CHANGE_EDITREQUEST_DATA':
      return {
        ...state,
        editrequestData: action.data
      }
    case 'CHANGE_DELETEREQUEST_DATA':
      return {
        ...state,
        deleterequestData: action.data
      }
    case 'CHANGE_DELETESELECTEXAM_DATA':
      return {
        ...state,
        deleteselectexamData: action.data
      }
    case 'CHANGE_CHANGEREQUEST_DATA':
      return {
        ...state,
        changerequestData: action.data
      }
    case 'CHANGE_UNITTAB_DATA':
      return {
        ...state,
        unittabData: action.data
      }
    case 'CHANGE_INCIDENT_DATA':
      return {
        ...state,
        incidentData: action.data
      }
    case 'CHANGE_POSITIONTAB_DATA':
      return {
        ...state,
        positiontabData: action.data
      }
    case 'CHANGE_STATUSTAB_DATA':
      return {
        ...state,
        statustabData: action.data
      }
    case 'CHANGE_HISREQUEST_DATA':
      return {
        ...state,
        hisrequestData: action.data
      }
    case 'CHANGE_HISREQUESTREQUEST_DATA':
      return {
        ...state,
        hisrequestrequestData: action.data
      }
    case 'CHANGE_HISREQUESTINCIDENT_DATA':
      return {
        ...state,
        hisrequestincidentData: action.data
      }
    case 'CHANGE_HISREQUESTSELECTEXAM_DATA':
      return {
        ...state,
        hisrequestselectexamData: action.data
      }
    case 'CHANGE_HISREQUESTSELECTEXAM2_DATA':
      return {
        ...state,
        hisrequestselectexam2Data: action.data
      }
    case 'CHANGE_HISREQUESTBELONG_DATA':
      return {
        ...state,
        hisrequestbelongData: action.data
      }
    case 'CHANGE_HISREQUESTPROVINCE_DATA':
      return {
        ...state,
        hisrequestprovinceData: action.data
      }
    case 'CHANGE_HISREQUESTALL_DATA':
      return {
        ...state,
        hisrequestallData: action.data
      }
    case 'CHANGE_NUMREQUEST_DATA':
      return {
        ...state,
        numrequestData: action.data
      }
    case 'CHANGE_CHECKNUMREQUEST_DATA':
      return {
        ...state,
        checknumrequestData: action.data
      }
    case 'CHANGE_REQUESTREPORT_DATA':
      return {
        ...state,
        requestreportData: action.data
      }
    case 'CHANGE_CHANGEUNIT3_DATA':
      return {
        ...state,
        changeUnit3: action.data
      }
    case 'CHANGE_CHANGEALLUNIT3_DATA':
      return {
        ...state,
        changeAllUnit3: action.data
      }
    case 'CHANGE_ARMTOWN_DATA':
      return {
        ...state,
        armtown: action.data
      }
    case 'CHANGE_PROVIDECOUNT_DATA':
      return {
        ...state,
        providecount: action.data
      }
    case 'CHANGE_RELIGIONPROVIDECOUNT_DATA':
      return {
        ...state,
        religionprovidecountData: action.data
      }
    case 'CHANGE_AMOUNT_DATA':
      return {
        ...state,
        amountData: action.data
      }
    case 'CHANGE_UPDATEAMOUNT_DATA':
      return {
        ...state,
        updateamountData: action.data
      }
    case 'CHANGE_BUILDER_DATA':
      return {
        ...state,
        builderData: action.payload
      }
    case 'CHANGE_GROUPCOUNT_DATA':
      return {
        ...state,
        groupcountData: action.data
      }
      case 'CHANGE_LOCKREQUEST_DATA':
      return {
        ...state,
        lockRequestData: action.data
      }
    default:
      return state
  }
}

export default mainReducer
