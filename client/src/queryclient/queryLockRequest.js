
function search(navyid,selectid,lockselectcode) {
    return fetch(`/api/lockrequest?navyid=${navyid}&Selectid=${selectid}&lockselectcode=${lockselectcode}`, {
        accept: 'application/json',
    })
        .then(checkStatus)
        .then(parseJSON);
}
function checkStatus(response) {
    if (response.status >= 200 && response.status < 300) {
        return response;
    } else {
        const error = new Error(`HTTP Error ${response.statusText}`);
        error.status = response.statusText;
        error.response = response;
        console.log(error); // eslint-disable-line no-console
        throw error;
    }
}
function parseJSON(response) {
    return response.json();
}

const queryLockRequest = { search };
export default queryLockRequest;
