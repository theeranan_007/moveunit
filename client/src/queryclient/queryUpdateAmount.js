
function search(unit, count, percent, manual_percent) {
    return fetch(`/api/updateamount?unit=${unit}&count=${count}&percent=${percent}&manual_percent=${manual_percent}`, {
        accept: 'application/json',
    })
        .then(checkStatus)
        .then(parseJSON);
}

function checkStatus(response) {
    if (response.status >= 200 && response.status < 300) {
        return response;
    } else {
        const error = new Error(`HTTP Error ${response.statusText}`);
        error.status = response.statusText;
        error.response = response;
        console.log(error); // eslint-disable-line no-console
        throw error;
    }
}

function parseJSON(response) {
    return response.json();
}

const queryUpdateAmount = { search };
export default queryUpdateAmount;
