
function search(navyid,selectid) {
    return fetch(`/api/deleterequest?navyid=${navyid}&Selectid=${selectid}`, {
        accept: 'application/json',
    })
        .then(checkStatus)
        .then(parseJSON);
}
function checkStatus(response) {
    if (response.status >= 200 && response.status < 300) {
        return response;
    } else {
        const error = new Error(`HTTP Error ${response.statusText}`);
        error.status = response.statusText;
        error.response = response;
        console.log(error); // eslint-disable-line no-console
        throw error;
    }
}
function parseJSON(response) {
    return response.json();
}

const queryDeleteRequest = { search };
export default queryDeleteRequest;
