
function search(navyid,unit3,type) {
    return fetch(`/api/changeAllUnit3?navyid=${navyid}&Unit3=${unit3}&type=${type}`, {
        accept: 'application/json',
    })
        .then(checkStatus)
        .then(parseJSON);
}

function checkStatus(response) {
    if (response.status >= 200 && response.status < 300) {
        return response;
    } else {
        const error = new Error(`HTTP Error ${response.statusText}`);
        error.status = response.statusText;
        error.response = response;
        console.log(error); // eslint-disable-line no-console
        throw error;
    }
}

function parseJSON(response) {
    return response.json();
}

const queryChangeAllUnit3 = { search };
export default queryChangeAllUnit3;
