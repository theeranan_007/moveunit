
function search(navyid,unit,askcode,askname,num,remark,remark2,selectid) {
    return fetch(`/api/editrequest?navyid=${navyid}&Unit=${unit}&Askcode=${askcode}&Askname=${askname}&Num=${num}&Remark=${remark}&Remark2=${remark2}&Selectid=${selectid}`, {
        accept: 'application/json',
    })
        .then(checkStatus)
        .then(parseJSON);
}
function checkStatus(response) {
    if (response.status >= 200 && response.status < 300) {
        return response;
    } else {
        const error = new Error(`HTTP Error ${response.statusText}`);
        error.status = response.statusText;
        error.response = response;
        console.log(error); // eslint-disable-line no-console
        throw error;
    }
}
function parseJSON(response) {
    return response.json();
}

const queryEditRequest = { search };
export default queryEditRequest;
