
function search(where, sort,groupby,name,sname,id8) {
    return fetch(`/api/tabledata?where=${where}&sort=${sort}&groupby=${groupby}&name=${name}&sname=${sname}&id8=${id8}`, {
        accept: 'application/json',
    })
        .then(checkStatus)
        .then(parseJSON);
}

function checkStatus(response) {
    if (response.status >= 200 && response.status < 300) {
        return response;
    } else {
        const error = new Error(`HTTP Error ${response.statusText}`);
        error.status = response.statusText;
        error.response = response;
        console.log(error); // eslint-disable-line no-console
        throw error;
    }
}

function parseJSON(response) {
    return response.json();
}

const queryTableData = { search };
export default queryTableData;
