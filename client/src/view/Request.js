import React, { Component } from 'react';
import Search from '../component/Search'
import Hisrequest from '../component/Hisrequest'
import { Col, Panel, Tab, Tabs, Row,Button } from 'react-bootstrap'
import FontAwesome from 'react-fontawesome'
import Requestfrom from '../component/Requestfrom'
import Request_report from '../component/Request_report'
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import { request } from 'http';
import { connect } from 'react-redux'
class Request extends Component {
  constructor(Props) {
    super(Props)
    this.handleSelect = this.handleSelect.bind(this)
    this.CountFormat1 = this.CountFormat1.bind(this)
    this.CountFormat2 = this.CountFormat2.bind(this)
    this.CountFormat3 = this.CountFormat3.bind(this)
    this.querytab2 = this.querytab2.bind(this)
    this.refreshrequest = this.refreshrequest.bind(this)
  }
  refreshrequest(){
    this.props.fetchHisrequest_request({})
  }
  querytab2(refnum, sw) {
    this.props.requesttab({ requesttab: 2 ,state:sw,unit:refnum})
  }
  CountFormat1(cell, row) {
    //this.props.requesttab({ requesttab: 2 ,state:1})
    return <a onClick={() => this.querytab2(cell, 1)}>{row['c']}</a>
  }
  CountFormat2(cell, row) {
    //this.props.requesttab({ requesttab: 2 ,state:2})
    return <a onClick={() => this.querytab2(cell, 2)}>{row['count2']}</a>
  }
  CountFormat3(cell, row) {
    //this.props.requesttab({ requesttab: 2 ,state:3})
    return <a onClick={() => this.querytab2(cell, 3)}>{row['count3']}</a>
  }
  handleSelect(key) {
    this.props.requesttab({ requesttab: key ,state:0,unit:31})
  }
  render() {
    return (
      <div>
        <Col md={9}>
          <Panel header="ร้องขอ" bsStyle="primary">
            <Tabs activeKey={this.props.view.requesttab} onSelect={this.handleSelect} id="uncontrolled-tab-example">
              <Tab eventKey={1} title="ค้นหา">
                <Search type="request" content={< Requestfrom />} />
              </Tab>
              <Tab eventKey={2} title="จัดแบ่ง">
                <Request_report />
              </Tab>
              <Tab eventKey={3} title="ยอดร้องขอ">
              <Row>
                  <Col sm={12}>
                    <Button onClick={this.refreshrequest} className="pull-right" bsStyle="default"><FontAwesome name="refresh" />&nbsp;รีเฟรช</Button>
                  </Col>
                </Row>
                <Row>
                  <Col sm={12}>
                    <BootstrapTable
                      className="table-responsive"
                      data={this.props.hisrequestrequestData}
                      search={false}>
                      <TableHeaderColumn dataField='unitname' >หน่วย</TableHeaderColumn>
                      <TableHeaderColumn dataField='refnum' dataFormat={this.CountFormat1}>จำนวนทั้งสิ้น</TableHeaderColumn>
                      <TableHeaderColumn dataField='refnum' dataFormat={this.CountFormat2}>ได้ตามร้องขอ</TableHeaderColumn>
                      <TableHeaderColumn dataField='refnum' dataFormat={this.CountFormat3}>ไม่ได้ตามร้องขอ</TableHeaderColumn>
                      <TableHeaderColumn isKey={true} hidden={true} dataField='refnum'>unit</TableHeaderColumn>
                    </BootstrapTable>
                  </Col>
                </Row>
              </Tab>
            </Tabs>
          </Panel>
        </Col>
        <Col md={3}>
          <Hisrequest tabactive={1} />
        </Col>
      </div>
    );
  }
}
function mapStatetoProps(state) {
  return {
    view: state.view,
    hisrequestrequestData: state.hisrequestrequestData,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    requesttab: requesttab => dispatch({ type: "CHANGE_VIEW_DATA", payload: requesttab }),
    requesttab: state => dispatch({ type: "CHANGE_VIEW_DATA", payload: state }),
    requesttab: unit => dispatch({ type: "CHANGE_VIEW_DATA", payload: unit }),
    fetchRequestReport: where => dispatch({ type: "FETCH_REQUESTREPORT_DATA", payload: where }),
    fetchRequestReport: groupby => dispatch({ type: "FETCH_REQUESTREPORT_DATA", payload: groupby }),
    fetchRequestReport: have => dispatch({ type: "FETCH_REQUESTREPORT_DATA", payload: have }),
    fetchRequestReport: sort => dispatch({ type: "FETCH_REQUESTREPORT_DATA", payload: sort }),
    fetchHisrequest_request: navyid => dispatch({ type: "FETCH_HISREQUESTREQUEST_DATA" }),
        
  }
}

const Connectedrequest_Request = connect(mapStatetoProps, mapDispatchToProps)(Request)

export default Connectedrequest_Request

