import React, { Component } from 'react';
import Select from 'react-select';
import {
  Col,
  Row,
  Tab,
  Tabs,
  Panel,
  ControlLabel,
  Button
} from 'react-bootstrap';
import Hisrequest from '../component/Hisrequest'
import Table_changeUnit3 from '../component/Table_changeUnit3';
import Search from '../component/Search'
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import FontAwesome from 'react-fontawesome';
var query = 0,tableData=[]
class Change extends Component {
  constructor(props) {
    super(props)
    this.state = {
      valuePopsition: "",
      valueUnit4: "",
      data: [],
      optionsstatus: [],
      type: "top-left",
      timeout: 0,
      message:"",
      headline:""
    }
    this.logchangePosition = this.logchangePosition.bind(this)
    this.logchangeUnit4 = this.logchangeUnit4.bind(this)
  }
  logchangeUnit4(val) {
    this.setState({ valueUnit4: val.value })
  }
  logchangePosition(val) {
    this.setState({ valuePopsition: val.value })

  }
  
  componentDidMount() {
    if (this.props.positiontabData == "" || this.props.positiontabData == null) {
      this.props.fetchPositiontab();
    }
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.view.state == 1) {
      
      if (nextProps.view.unit == 31) {
        this.props.fetchData({ where: "", sort: "p.percent desc", groupby: "", have: "", name: "", sname: "", id8: "" })
        
      } else {
        this.props.fetchData({ where: "p.unit3=" + nextProps.view.unit, sort: "p.percent desc", groupby: "", have: "", name: "", sname: "", id8: "" })
      }
      this.props.changetab({ changetab: 2, unit: nextProps.view.unit, state: 0 })
      
    }
    if (nextProps.changeAllUnit3.hasOwnProperty('success') && query == 1) {
      query = 0
    }
  }
  render() {
    return (
      <div>
        <Col md={9}>
          <Panel header="สับเปลี่ยน" bsStyle="primary">
            <Table_changeUnit3 data={this.props.tableData} type="Change"/>
          </Panel>
        </Col>
        <Col md={3}>
          <Hisrequest tabactive={5} />
        </Col>
      </div>
    );
  }
}
function mapStatetoProps(state) {
  // console.log("เช็ค mapstatetoprops" + JSON.stringify(requestData))
  return {
    positiontabData: state.positiontabData,
    unittabData: state.unittabData,
    tableData: state.tableData,
    view: state.view,
    changeAllUnit3: state.changeAllUnit3,

  }
}

function mapDispatchToProps(dispatch) {
  return {
    changetab: changetab => dispatch({ type: "CHANGE_VIEW_DATA", payload: changetab }),
    changetab: unit => dispatch({ type: "CHANGE_VIEW_DATA", payload: unit }),
    changetab: state => dispatch({ type: "CHANGE_VIEW_DATA", payload: state }),
    fetchPositiontab: navyid => dispatch({ type: "FETCH_POSITIONTAB_DATA", payload: navyid }),
    fetchUnittab: navyid => dispatch({ type: "FETCH_UNITTAB_DATA", payload: navyid }),
    fetchData: where => dispatch({ type: "FETCH_TABLE_DATA", payload: where }),
    fetchData: sort => dispatch({ type: "FETCH_TABLE_DATA", payload: sort }),
    fetchData: groupby => dispatch({ type: "FETCH_TABLE_DATA", payload: groupby }),
    fetchData: have => dispatch({ type: "FETCH_TABLE_DATA", payload: have }),
    fetchData: name => dispatch({ type: "FETCH_TABLE_DATA", payload: name }),
    fetchData: sname => dispatch({ type: "FETCH_TABLE_DATA", payload: sname }),
    fetchData: id8 => dispatch({ type: "FETCH_TABLE_DATA", payload: id8 }),
    alert: type => dispatch({type:"ALERTS" , payload:type}),
    alert: headline => dispatch({type:"ALERTS" , payload:headline}),
    alert: message => dispatch({type:"ALERTS" , payload:message}),
    alert: timeout => dispatch({type:"ALERTS" , payload:timeout}),
    alert: position => dispatch({type:"ALERTS" , payload:position}),
  }
}

const ConnectedChange = connect(mapStatetoProps, mapDispatchToProps)(Change)
export default ConnectedChange
