import React, { Component } from 'react';
import Search from '../component/Search'
import Hisrequest from '../component/Hisrequest'
import { Col, Panel, Tab, Tabs, Row, ControlLabel, Button, Table, FormControl } from 'react-bootstrap'
import { connect } from 'react-redux'
import FontAwesome from 'react-fontawesome';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import Select from 'react-select';
import Table_changeUnit3 from '../component/Table_changeUnit3';
let groupcountData = [];
let groupcountData_all = [];
class Group extends Component {
    constructor(props) {
        super(props)
        this.refresh = this.refresh.bind(this)
        this.querytab2 = this.querytab2.bind(this)
        this.cellformat_incident = this.cellformat_incident.bind(this)
        this.cellformat_selectexam = this.cellformat_selectexam.bind(this)
        this.cellformat_selectexam_incident = this.cellformat_selectexam_incident.bind(this)
        this.cellformat_province = this.cellformat_province.bind(this)
        this.cellformat_province_incident = this.cellformat_province_incident.bind(this)
        this.cellformat_province_selectexam = this.cellformat_province_selectexam.bind(this)
        this.cellformat_request = this.cellformat_request.bind(this)
        this.cellformat_request_incident = this.cellformat_request_incident.bind(this)
        this.cellformat_request_selectexam = this.cellformat_request_selectexam.bind(this)
        this.cellformat_request_province = this.cellformat_request_province.bind(this)
        this.cellformat_change = this.cellformat_change.bind(this)
        this.cellformat_total = this.cellformat_total.bind(this)
        this.handleSelect = this.handleSelect.bind(this)
    }
    refresh() {
        this.props.fetchGroup({});
        this.props.alert({ type: "success", headline: "กำลังโหลด...", message: < div className="loader" > </div>, timeout: 0, position: "top-right" })
    }
    componentDidMount() {
        this.refresh();
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.groupcountData.status == 200) {
            this.props.alert({ type: "", timeout: 1 })
            if (groupcountData != nextProps.groupcountData.response) {
                groupcountData = nextProps.groupcountData.response
                let incident = 0;
                let selectexam = 0;
                let selectexam_incident = 0;
                let province = 0;
                let province_incident = 0;
                let province_selectexam = 0;
                let request = 0;
                let request_incident = 0;
                let request_selectexam = 0;
                let request_province = 0;
                let change = 0;
                let total = 0;
                groupcountData.forEach(element => {
                    incident += element.incident;
                    selectexam += element.selectexam;
                    selectexam_incident += element.selectexam_incident;
                    province += element.province;
                    province_incident += element.province_incident;
                    province_selectexam += element.province_selectexam;
                    request += element.request;
                    request_incident += element.request_incident;
                    request_selectexam += element.request_selectexam;
                    request_province += element.request_province;
                    change += element.change;
                    total += element.total;
                });
                groupcountData_all = groupcountData;
                groupcountData_all[31] = {
                    refnum: 31,
                    unitname: "รวม",
                    incident: incident,
                    selectexam: selectexam,
                    selectexam_incident: selectexam_incident,
                    province: province,
                    province_incident: province_incident,
                    province_selectexam: province_selectexam,
                    request: request,
                    request_incident: request_incident,
                    request_selectexam: request_selectexam,
                    request_province: request_province,
                    change: change,
                    total: total
                }
            }
        }
        else {
            groupcountData = [];
        }
    }
    cellformat_incident(cell, row) {
        return <a onClick={() => this.querytab2(row['refnum'], 1)}>{cell}</a>
    }
    cellformat_selectexam(cell, row) {
        return <a onClick={() => this.querytab2(row['refnum'], 2)}>{cell}</a>
    }
    cellformat_selectexam_incident(cell, row) {
        return <a onClick={() => this.querytab2(row['refnum'], 3)}>{cell}</a>
    }
    cellformat_province(cell, row) {
        return <a onClick={() => this.querytab2(row['refnum'], 4)}>{cell}</a>
    }
    cellformat_province_incident(cell, row) {
        return <a onClick={() => this.querytab2(row['refnum'], 5)}>{cell}</a>
    }
    cellformat_province_selectexam(cell, row) {
        return <a onClick={() => this.querytab2(row['refnum'], 6)}>{cell}</a>
    }
    cellformat_request(cell, row) {
        return <a onClick={() => this.querytab2(row['refnum'], 7)}>{cell}</a>
    }
    cellformat_request_incident(cell, row) {
        return <a onClick={() => this.querytab2(row['refnum'], 8)}>{cell}</a>
    }
    cellformat_request_selectexam(cell, row) {
        return <a onClick={() => this.querytab2(row['refnum'], 9)}>{cell}</a>
    }
    cellformat_request_province(cell, row) {
        return <a onClick={() => this.querytab2(row['refnum'], 10)}>{cell}</a>
    }
    cellformat_change(cell, row) {
        return <a onClick={() => this.querytab2(row['refnum'], 11)}>{cell}</a>
    }
    cellformat_total(cell, row) {
        return <a onClick={() => this.querytab2(row['refnum'], 12)}>{cell}</a>
    }
    querytab2(refnum, sw) {
        this.handleSelect(2)
        switch (sw) {
            case 1:
                if (refnum == 31) {
                    this.props.fetchData({ where: "p.statuscode != 'AA'", sort: "p.unit3,p.statuscode,p.unit1,p.unit2", groupby: "", have: "" })
                }
                else {
                    this.props.fetchData({ where: "p.statuscode != 'AA' and p.unit3 = " + refnum, sort: "p.unit3,p.statuscode,p.unit1,p.unit2", groupby: "", have: "" })
                }
                break;
            case 2:
                if (refnum == 31) {
                    this.props.fetchData({ where: "selectexam.unit4 = p.unit3 and p.STATUSCODE = 'AA'", sort: "p.unit3,p.statuscode,p.unit1,p.unit2", groupby: "", have: "" })
                }
                else {
                    this.props.fetchData({ where: "selectexam.unit4 = p.unit3 and p.STATUSCODE = 'AA' and p.unit3 = " + refnum, sort: "p.unit3,p.statuscode,p.unit1,p.unit2", groupby: "", have: "" })
                }
                break;
            case 3:
                if (refnum == 31) {
                    this.props.fetchData({ where: "selectexam.unit4 = p.unit3 and p.STATUSCODE != 'AA'", sort: "p.unit3,p.statuscode,p.unit1,p.unit2", groupby: "", have: "" })
                }
                else {
                    this.props.fetchData({ where: "selectexam.unit4 = p.unit3 and p.STATUSCODE != 'AA' and p.unit3 = " + refnum, sort: "p.unit3,p.statuscode,p.unit1,p.unit2", groupby: "", have: "" })
                }
                break;
            case 4:
                if (refnum == 31) {
                    this.props.fetchData({ where: "armid in (25,32,45) and p.STATUSCODE= 'AA' and selectexam.unit4 != p.unit3", sort: "p.unit3,p.statuscode,p.unit1,p.unit2", groupby: "", have: "" })
                }
                else {
                    this.props.fetchData({ where: "armid in (25,32,45) and p.STATUSCODE= 'AA' and selectexam.unit4 != p.unit3 and p.unit3 = " + refnum, sort: "p.unit3,p.statuscode,p.unit1,p.unit2", groupby: "", have: "" })
                }
                break;
            case 5:
                if (refnum == 31) {
                    this.props.fetchData({ where: "armid in (25,32,45) and p.STATUSCODE!= 'AA' and selectexam.unit4 != p.unit3", sort: "p.unit3,p.statuscode,p.unit1,p.unit2", groupby: "", have: "" })
                }
                else {
                    this.props.fetchData({ where: "armid in (25,32,45) and p.STATUSCODE!= 'AA' and selectexam.unit4 != p.unit3 and p.unit3 = " + refnum, sort: "p.unit3,p.statuscode,p.unit1,p.unit2", groupby: "", have: "" })
                }
                break;
            case 6:
                if (refnum == 31) {
                    this.props.fetchData({ where: "armid in (25,32,45) and p.STATUSCODE= 'AA' and selectexam.unit4 = p.unit3", sort: "p.unit3,p.statuscode,p.unit1,p.unit2", groupby: "", have: "" })
                }
                else {
                    this.props.fetchData({ where: "armid in (25,32,45) and p.STATUSCODE= 'AA' and selectexam.unit4 = p.unit3 and p.unit3 = " + refnum, sort: "p.unit3,p.statuscode,p.unit1,p.unit2", groupby: "", have: "" })
                }
                break;
            case 7:
                if (refnum == 31) {
                    this.props.fetchData({ where: "r.navyid is not null and p.STATUSCODE = 'AA' and selectexam.unit4 != p.unit3", sort: "p.unit3,p.statuscode,p.unit1,p.unit2", groupby: "", have: "" })
                }
                else {
                    this.props.fetchData({ where: "r.navyid is not null and p.STATUSCODE = 'AA' and selectexam.unit4 != p.unit3 and p.unit3 = " + refnum, sort: "p.unit3,p.statuscode,p.unit1,p.unit2", groupby: "", have: "" })
                }
                break;
            case 8:
                if (refnum == 31) {
                    this.props.fetchData({ where: "r.navyid is not null and p.STATUSCODE!= 'AA' and selectexam.unit4 != p.unit3", sort: "p.unit3,p.statuscode,p.unit1,p.unit2", groupby: "", have: "" })
                }
                else {
                    this.props.fetchData({ where: "r.navyid is not null and p.STATUSCODE!= 'AA' and selectexam.unit4 != p.unit3 and p.unit3 = " + refnum, sort: "p.unit3,p.statuscode,p.unit1,p.unit2", groupby: "", have: "" })
                }
                break;
            case 9:
                if (refnum == 31) {
                    this.props.fetchData({ where: "r.navyid is not null and p.STATUSCODE= 'AA' and selectexam.unit4 = p.unit3", sort: "p.unit3,p.statuscode,p.unit1,p.unit2", groupby: "", have: "" })
                }
                else {
                    this.props.fetchData({ where: "r.navyid is not null and p.STATUSCODE= 'AA' and selectexam.unit4 = p.unit3 and p.unit3 = " + refnum, sort: "p.unit3,p.statuscode,p.unit1,p.unit2", groupby: "", have: "" })
                }
                break;
            case 10:
                if (refnum == 31) {
                    this.props.fetchData({ where: "r.navyid is not null and p.STATUSCODE= 'AA' and selectexam.unit4 != p.unit3", sort: "p.unit3,p.statuscode,p.unit1,p.unit2", groupby: "", have: "" })
                }
                else {
                    this.props.fetchData({ where: "r.navyid is not null and p.STATUSCODE= 'AA' and selectexam.unit4 != p.unit3 and p.unit3 = " + refnum, sort: "p.unit3,p.statuscode,p.unit1,p.unit2", groupby: "", have: "" })
                }
                break;
            case 11:
                if (refnum == 31) {
                    this.props.fetchData({ where: "p.statuscode='AA' and selectexam.unit4 != p.unit3 and armid not in (25,32,45) and r.navyid is null", sort: "p.unit3,p.statuscode,p.unit1,p.unit2", groupby: "", have: "" })
                }
                else {
                    this.props.fetchData({ where: "p.statuscode='AA' and selectexam.unit4 != p.unit3 and armid not in (25,32,45) and r.navyid is null and p.unit3 = " + refnum, sort: "p.unit3,p.statuscode,p.unit1,p.unit2", groupby: "", have: "" })
                }
                break;
            case 12:
                if (refnum == 31) {
                    this.props.fetchData({ where: "", sort: "p.unit3,p.statuscode,p.unit1,p.unit2", groupby: "", have: "" })
                }
                else {
                    this.props.fetchData({ where: "p.unit3 = " + refnum, sort: "p.unit3,p.statuscode,p.unit1,p.unit2", groupby: "", have: "" })
                }
                break;
            default:
                if (refnum == 31) {
                    //this.props.fetchData({ where: "selectexam.unit4 != p.unit3 and selectexam.unit4 = p.unit3", sort: "selectexam.postcode,selectexam.unit4,selectexam.item", groupby: "", have: "" })
                }
                else {
                    //this.props.fetchData({ where: "selectexam.unit4 != p.unit3 and selectexam.postcode='" + postcode + "' and selectexam.unit4 = " + refnum, sort: "selectexam.postcode,selectexam.unit4,selectexam.item", groupby: "", have: "" })
                }
                break;
        }
        this.props.alert({ type: "success", headline: "กำลังโหลด...", message: < div className="loader" > </div>, timeout: 0, position: "top-right" })
        
    }
    handleSelect(key) {
        this.props.grouptab({ grouptab: key })
    }
    render() {
        return (
            <div>
                <Col md={9}>
                    <Panel header="สรุปการแบ่ง" bsStyle="primary">
                        <Tabs activeKey={this.props.view.grouptab} onSelect={this.handleSelect} id="uncontrolled-tab-example">
                            <Tab eventKey={1} title="ยอด">
                                <Row>
                                    <Col sm={12}>
                                        <Button onClick={this.refresh} className="pull-right" bsStyle="default"><FontAwesome name="refresh" />&nbsp;รีเฟรช</Button>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm={12}>
                                        <BootstrapTable
                                            className="table-responsive"
                                            data={groupcountData_all}
                                            search={false}
                                            height={450}>
                                            <TableHeaderColumn row='0' rowSpan="2" dataField='unitname' >หน่วย</TableHeaderColumn>
                                            <TableHeaderColumn row='0' dataFormat={this.cellformat_incident} rowSpan="2" thStyle={{ backgroundColor: '#e2e2e2' }} tdStyle={{ backgroundColor: '#e2e2e2' }} dataField='incident' >มีเหตุ</TableHeaderColumn>

                                            <TableHeaderColumn row='0' colSpan='2' >คัดเลือก</TableHeaderColumn>
                                            <TableHeaderColumn row='1' dataFormat={this.cellformat_selectexam} dataField='selectexam' thStyle={{ backgroundColor: '#e2e2e2' }} tdStyle={{ backgroundColor: '#e2e2e2' }}>ไม่ซ้ำ</TableHeaderColumn>
                                            <TableHeaderColumn row='1' dataFormat={this.cellformat_selectexam_incident} dataField='selectexam_incident' >ซ้ำมีเหตุ</TableHeaderColumn>

                                            <TableHeaderColumn row='0' colSpan="3" >3จชต</TableHeaderColumn>
                                            <TableHeaderColumn row='1' dataFormat={this.cellformat_province} dataField='province' thStyle={{ backgroundColor: '#e2e2e2' }} tdStyle={{ backgroundColor: '#e2e2e2' }}>ไม่ซ้ำ</TableHeaderColumn>
                                            <TableHeaderColumn row='1' dataFormat={this.cellformat_province_incident} dataField='province_incident' >ซ้ำมีเหตุ</TableHeaderColumn>
                                            <TableHeaderColumn row='1' dataFormat={this.cellformat_province_selectexam} dataField='province_selectexam' >ซ้ำคัดเลือก</TableHeaderColumn>

                                            <TableHeaderColumn row='0' colSpan="4" >ร้องขอ</TableHeaderColumn>
                                            <TableHeaderColumn row='1' dataFormat={this.cellformat_request} dataField='request' thStyle={{ backgroundColor: '#e2e2e2' }} tdStyle={{ backgroundColor: '#e2e2e2' }}>ไม่ซ้ำ</TableHeaderColumn>
                                            <TableHeaderColumn row='1' dataFormat={this.cellformat_request_incident} dataField='request_incident' >ซ้ำมีเหตุ</TableHeaderColumn>
                                            <TableHeaderColumn row='1' dataFormat={this.cellformat_request_selectexam} dataField='request_selectexam' >ซ้ำคัดเลือก</TableHeaderColumn>
                                            <TableHeaderColumn row='1' dataFormat={this.cellformat_request_province} dataField='request_province' >ซ้ำ3จชต</TableHeaderColumn>

                                            <TableHeaderColumn row='0' dataFormat={this.cellformat_change} rowSpan="2" dataField='change' thStyle={{ backgroundColor: '#e2e2e2' }} tdStyle={{ backgroundColor: '#e2e2e2' }}>อื่นๆ</TableHeaderColumn>
                                            <TableHeaderColumn row='0' dataFormat={this.cellformat_total} rowSpan="2" dataField='total' thStyle={{ backgroundColor: '#e2e2e2' }} tdStyle={{ backgroundColor: '#e2e2e2' }}>รวม</TableHeaderColumn>
                                            <TableHeaderColumn dataField='refnum' isKey hidden ></TableHeaderColumn>
                                        </BootstrapTable>
                                    </Col>
                                </Row>
                            </Tab>
                            <Tab eventKey={2} title="จัดแบ่ง">
                                <Table_changeUnit3 data={this.props.tableData} type="Group" />
                            </Tab>
                        </Tabs>
                    </Panel>
                </Col>
                <Col md={3}>
                    <Hisrequest tabactive={5} />
                </Col>
            </div>
        );
    }
}
function mapStatetoProps(state) {
    // console.log("เช็ค mapstatetoprops" + JSON.stringify(requestData))
    return {
        view: state.view,
        groupcountData: state.groupcountData,
    }
}

function mapDispatchToProps(dispatch) {
    return {
        fetchGroup: () => dispatch({ type: "FETCH_GROUPCOUNT_DATA" }),
        grouptab: (
            belongtab => dispatch({ type: "CHANGE_VIEW_DATA", payload: belongtab }),
            state => dispatch({ type: "CHANGE_VIEW_DATA", payload: state })
        ),
        fetchData: (
            where => dispatch({ type: "FETCH_TABLE_DATA", payload: where }),
            sort => dispatch({ type: "FETCH_TABLE_DATA", payload: sort }),
            groupby => dispatch({ type: "FETCH_TABLE_DATA", payload: groupby }),
            have => dispatch({ type: "FETCH_TABLE_DATA", payload: have })
        ),
        alert: (
            type => dispatch({ type: "ALERTS", payload: type }),
            headline => dispatch({ type: "ALERTS", payload: headline }),
            message => dispatch({ type: "ALERTS", payload: message }),
            timeout => dispatch({ type: "ALERTS", payload: timeout }),
            position => dispatch({ type: "ALERTS", payload: position })
        ),
    }
}

const ConnectedGroup = connect(mapStatetoProps, mapDispatchToProps)(Group)
export default ConnectedGroup