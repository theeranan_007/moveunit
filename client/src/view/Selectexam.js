import React, { Component } from 'react';
import Select from 'react-select';
import {
  Col,
  Row,
  Tab,
  Tabs,
  Panel,
  ControlLabel,
  Button,
  FormControl
} from 'react-bootstrap';
import Hisrequest from '../component/Hisrequest'
import Table_changeUnit3 from '../component/Table_changeUnit3';
import Search from '../component/Search'
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import FontAwesome from 'react-fontawesome';
import { AlertList, Alert, AlertContainer } from "react-bs-notifier";
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import Selectexamfrom from '../component/Selectexamfrom'
var query = 0, refunit = 31, state = 1
class Selectexam extends Component {
  constructor(props) {
    super(props)
    this.state = {
      valuePosition: "",
      valueUnit4: "",
      valueUnit3: "",
      data: [],
      optionsstatus: [],
      position: "top-left",
      alerts: [],
      timeout: 0,
      newMessage: "This is a test of the Emergency Broadcast System. This is only a test.",
      inputvalidate: null,
      valueChangeAllUnit3: "",
      limit: "",
    }
    this.handleLimit = this.handleLimit.bind(this)
    this.logchangePosition = this.logchangePosition.bind(this)
    this.logchangeUnit4 = this.logchangeUnit4.bind(this)
    this.logchangeUnit3 = this.logchangeUnit3.bind(this)
    this.handleSelect = this.handleSelect.bind(this)
    this.submit = this.submit.bind(this)
    this.CountFormat1 = this.CountFormat1.bind(this)
    this.CountFormat2 = this.CountFormat2.bind(this)
    this.CountFormat3 = this.CountFormat3.bind(this)
    this.querytab2 = this.querytab2.bind(this)
    this.refreshselectexam2 = this.refreshselectexam2.bind(this)
  }
  refreshselectexam2(){
    this.props.fetchselectexam2();
  }
  handleLimit(event) {
    this.setState({ limit: event.target.value })
  }
  handleSelect(key) {
    this.props.selectexamtab({ selectexamtab: key })
  }
  logchangeUnit4(val) {
    if (val != null) {
      this.setState({ valueUnit4: val.value })
    }
    else {
      this.setState({ valueUnit4: "" })
    }
  }
  logchangeUnit3(val) {
    if (val != null) {
      this.setState({ valueUnit3: val.value })
    }
    else {
      this.setState({ valueUnit3: "" })
    }
  }
  logchangePosition(val) {
    if (val != null) {
      this.setState({ valuePosition: val.value })
    }
    else {
      this.setState({ valuePosition: "" })
    }

  }
  submit() {
    this.props.fetchData({ where: "p.unit3 = " + this.state.valueUnit3 + " and selectexam.unit4=" + this.state.valueUnit4 + " and selectexam.postcode='" + this.state.valuePosition + "'", sort: "selectexam.item", groupby: "", have: "" })

  }
  componentDidMount() {
    if (this.props.positiontabData == "" || this.props.positiontabData == null) {
      this.props.fetchPositiontab();
    }
    this.props.fetchselectexam2();
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.view.state == 1) {
      if (nextProps.view.unit == 31) {
        this.props.fetchData({ where: "(selectexam.unit4!=null or selectexam.unit4!='') and p.statuscode='AA' and p.batt<5", sort: "selectexam.postcode,selectexam.item", groupby: "", have: "" })
      } else {
        this.props.fetchData({ where: "p.statuscode='AA' and p.batt<5 and selectexam.unit4=" + nextProps.view.unit, sort: "selectexam.postcode,selectexam.item", groupby: "", have: "" })
      }
      this.props.selectexamtab({ selectexamtab: 2, unit: nextProps.view.unit, state: 0 })
      this.props.alert({ type: "", timeout: 1 }) // ปิด alert โหลดข้อมูล
    }
    // if (nextProps.changeAllUnit3.hasOwnProperty('success') && query == 1) {
    //   query = 0
    //   // this.submit()
    // }
  }
  querytab2(postcode, refnum, sw) {
    this.handleSelect(2)
    switch (sw) {
      case 1:
        if (postcode == 31) {
          this.props.fetchData({ where: "selectexam.navyid is not null", sort: "selectexam.postcode,selectexam.unit4,selectexam.item", groupby: "", have: "" })
        }
        else {
          this.props.fetchData({ where: "selectexam.postcode='" + postcode + "' and selectexam.unit4 = " + refnum, sort: "selectexam.postcode,selectexam.unit4,selectexam.item", groupby: "", have: "" })
        }
        break;
      case 2:
        if (postcode == 31) {
          this.props.fetchData({ where: "selectexam.unit4 = p.unit3 and selectexam.navyid is not null", sort: "selectexam.postcode,selectexam.unit4,selectexam.item", groupby: "", have: "" })
        }
        else {
          this.props.fetchData({ where: "selectexam.unit4 = p.unit3 and selectexam.postcode='" + postcode + "' and selectexam.unit4 = " + refnum, sort: "selectexam.postcode,selectexam.unit4,selectexam.item", groupby: "", have: "" })
        }
        break;

      default:
        if (postcode == 31) {
          this.props.fetchData({ where: "selectexam.unit4 != p.unit3 and selectexam.navyid is not null", sort: "selectexam.postcode,selectexam.unit4,selectexam.item", groupby: "", have: "" })
        }
        else {
          this.props.fetchData({ where: "selectexam.unit4 != p.unit3 and selectexam.postcode='" + postcode + "' and selectexam.unit4 = " + refnum, sort: "selectexam.postcode,selectexam.unit4,selectexam.item", groupby: "", have: "" })
        }
        break;
    }
  }
  CountFormat1(cell, row) {
    refunit = cell
    state = 1
    return <a onClick={() => this.querytab2(cell, row['refnum'], 1)}>{row['count1']}</a>
  }
  CountFormat2(cell, row) {
    refunit = cell
    state = 2
    return <a onClick={() => this.querytab2(cell, row['refnum'], 2)}>{row['count2']}</a>
  }
  CountFormat3(cell, row) {
    refunit = cell
    state = 3
    return <a onClick={() => this.querytab2(cell, row['refnum'], 3)}>{row['count3']}</a>
  }
  render() {
    return (
      <div>
        <Col md={9}>
          <Panel header="คัดเลือกหน่วย" bsStyle="primary">
            <Tabs activeKey={this.props.view.selectexamtab} onSelect={this.handleSelect} id="uncontrolled-tab-example">
              <Tab eventKey={1} title="เพิ่มข้อมูล">
                <br />
                <Row>
                  <Col sm={6}>
                    <ControlLabel>ตำแหน่ง</ControlLabel>
                  </Col>
                  <Col sm={6}>
                    <ControlLabel>หน่วย</ControlLabel>
                  </Col>
                </Row>
                <Row>
                  <Col sm={6}>
                    <Select
                      placeholder="ตำแหน่ง"
                      name="position"
                      value={this.state.valuePosition}
                      onChange={this.logchangePosition}
                      options={this.props.positiontabData} />
                  </Col>
                  <Col sm={6}>
                    <Select
                      placeholder="หน่วย"
                      name="unittab"
                      value={this.state.valueUnit4}
                      onChange={this.logchangeUnit4}
                      options={this.props.unittabData} />
                  </Col>
                </Row>
                <Search type="selectexam" position={this.state.valuePosition} unit4={this.state.valueUnit4} content={<Selectexamfrom type="modal" position={this.state.valuePosition} unit4={this.state.valueUnit4} />} />
              </Tab>
              <Tab eventKey={2} title="จัดแบ่ง">
                <Row>
                  <Col sm={12}>
                    <Table_changeUnit3 data={this.props.tableData} type="Selectexam" />
                  </Col>
                </Row>
              </Tab>
              <Tab eventKey={3} title="ยอดคัดเลือก">
                <Row>
                  <Col sm={12}>
                    <Button onClick={this.refreshselectexam2} className="pull-right" bsStyle="default"><FontAwesome name="refresh" />&nbsp;รีเฟรช</Button>
                  </Col>
                </Row>
                <Row>
                  <Col sm={12}>
                    <BootstrapTable
                      className="table-responsive"
                      data={this.props.hisrequestselectexam2Data.response}
                      search={false}>
                      <TableHeaderColumn dataField='postname' >ตำแหน่ง</TableHeaderColumn>
                      <TableHeaderColumn dataField='unitname' >หน่วย</TableHeaderColumn>
                      <TableHeaderColumn dataField='Postcode' dataFormat={this.CountFormat1}>จำนวนทั้งสิ้น</TableHeaderColumn>
                      <TableHeaderColumn dataField='Postcode' dataFormat={this.CountFormat2}>ได้ตามคัดเลือก</TableHeaderColumn>
                      <TableHeaderColumn dataField='Postcode' dataFormat={this.CountFormat3}>ไม่ได้ตามคัดเลือก</TableHeaderColumn>
                      <TableHeaderColumn isKey={true} hidden={true} dataField='Postcode'>Postcode</TableHeaderColumn>
                      <TableHeaderColumn hidden={true} dataField='refnum'>unit</TableHeaderColumn>
                    </BootstrapTable>
                  </Col>
                </Row>
              </Tab>
            </Tabs>
          </Panel>
        </Col>
        <Col md={3}>
          <Hisrequest tabactive={3} />
        </Col>
      </div>
    );
  }
}
function mapStatetoProps(state) {
  return {
    positiontabData: state.positiontabData,
    unittabData: state.unittabData,
    view: state.view,
    tableData: state.tableData,
    changeAllUnit3: state.changeAllUnit3,
    hisrequestselectexam2Data: state.hisrequestselectexam2Data,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    fetchHisrequest_request: navyid => dispatch({ type: "FETCH_HISREQUESTREQUEST_DATA" }),
    fetchHisrequest_incident: navyid => dispatch({ type: "FETCH_HISREQUESTINCIDENT_DATA" }),
    fetchHisrequest_selectexam: navyid => dispatch({ type: "FETCH_HISREQUESTSELECTEXAM_DATA" }),
    fetchHisrequest_province: armid => dispatch({ type: "FETCH_HISREQUESTPROVINCE_DATA", payload: armid }),
    fetchHisrequest_all: navyid => dispatch({ type: "FETCH_HISREQUESTALL_DATA" }),
    fetchChangeAllUnit3: navyid => dispatch({ type: "FETCH_CHANGEALLUNIT3_DATA", payload: navyid }),
    fetchChangeAllUnit3: where => dispatch({ type: "FETCH_CHANGEALLUNIT3_DATA", payload: where }),
    fetchChangeAllUnit3: sort => dispatch({ type: "FETCH_CHANGEALLUNIT3_DATA", payload: sort }),
    fetchChangeAllUnit3: have => dispatch({ type: "FETCH_CHANGEALLUNIT3_DATA", payload: have }),
    fetchChangeAllUnit3: name => dispatch({ type: "FETCH_CHANGEALLUNIT3_DATA", payload: name }),
    fetchChangeAllUnit3: sname => dispatch({ type: "FETCH_CHANGEALLUNIT3_DATA", payload: sname }),
    fetchChangeAllUnit3: id8 => dispatch({ type: "FETCH_CHANGEALLUNIT3_DATA", payload: id8 }),
    fetchChangeAllUnit3: limit => dispatch({ type: "FETCH_CHANGEALLUNIT3_DATA", payload: limit }),
    fetchChangeAllUnit3: unit3 => dispatch({ type: "FETCH_CHANGEALLUNIT3_DATA", payload: unit3 }),

    alert: type => dispatch({ type: "ALERTS", payload: type }),
    alert: headline => dispatch({ type: "ALERTS", payload: headline }),
    alert: message => dispatch({ type: "ALERTS", payload: message }),
    alert: timeout => dispatch({ type: "ALERTS", payload: timeout }),
    alert: position => dispatch({ type: "ALERTS", payload: position }),
    fetchselectexam2: () => dispatch({ type: "FETCH_HISREQUESTSELECTEXAM2_DATA" }),

    fetchPositiontab: navyid => dispatch({ type: "FETCH_POSITIONTAB_DATA", payload: navyid }),
    fetchUnittab: navyid => dispatch({ type: "FETCH_UNITTAB_DATA", payload: navyid }),
    selectexamtab: selectexamtab => dispatch({ type: "CHANGE_VIEW_DATA", payload: selectexamtab }),
    selectexamtab: unit => dispatch({ type: "CHANGE_VIEW_DATA", payload: unit }),
    selectexamtab: state => dispatch({ type: "CHANGE_VIEW_DATA", payload: state }),
    fetchData: where => dispatch({ type: "FETCH_TABLE_DATA", payload: where }),
    fetchData: sort => dispatch({ type: "FETCH_TABLE_DATA", payload: sort }),
    fetchData: groupby => dispatch({ type: "FETCH_TABLE_DATA", payload: groupby }),
    fetchData: have => dispatch({ type: "FETCH_TABLE_DATA", payload: have }),
  }
}

const ConnectedSelectexam = connect(mapStatetoProps, mapDispatchToProps)(Selectexam)
export default ConnectedSelectexam
