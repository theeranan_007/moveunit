import React, { Component } from 'react';
import Hisrequest from '../component/Hisrequest'
import { Col, Panel, Tab, Tabs, Row } from 'react-bootstrap'
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import Select from 'react-select';
import queryAmount from '../queryclient/queryAmount';
import Table_changeUnit3 from '../component/Table_changeUnit3';
import { request } from 'http';
import { connect } from 'react-redux'
var tableData=[],query=0
class Education extends Component {
  constructor(Props) {
    super(Props)
    this.state = {
      ecode:""
    }
    this.handleSelect = this.handleSelect.bind(this)
    this.submiteducation = this.submiteducation.bind(this)
    this.submiteducation2 = this.submiteducation2.bind(this)
    this.submiteducation3 = this.submiteducation3.bind(this)
    this.submitskill = this.submitskill.bind(this)
    this.submitskill2 = this.submitskill2.bind(this)
    this.submitskill3 = this.submitskill3.bind(this)
    this.eduformat = this.eduformat.bind(this)
    this.eduformat2 = this.eduformat2.bind(this)
    this.eduformat3 = this.eduformat3.bind(this)
    this.skillformat = this.skillformat.bind(this)
    this.skillformat2 = this.skillformat2.bind(this)
    this.skillformat3 = this.skillformat3.bind(this)
  }
  eduformat(cell, row) {
    
    if (cell == row['ecode']) {
      //
      return <a onClick={() => this.submiteducation(cell)}>{row['counteducation']}</a>
    }
  }
  eduformat2(cell, row) {
    
    if (cell == row['ecode']) {
      //
      return <a onClick={() => this.submiteducation2(cell)}>{row['counteducation2']}</a>
    }
  }
  eduformat3(cell, row) {
    
    if (cell == row['ecode']) {
      //
      return <a onClick={() => this.submiteducation3(cell)}>{row['counteducation3']}</a>
    }
  }
  skillformat(cell, row) {
    
    if (cell == row['skillcode']) {
      //
      return <a onClick={() => this.submitskill(cell)}>{row['countskill']}</a>
    }
  }
  skillformat2(cell, row) {
    
    if (cell == row['skillcode']) {
      //
      return <a onClick={() => this.submitskill2(cell)}>{row['countskill2']}</a>
    }
  }
  skillformat3(cell, row) {
    
    if (cell == row['skillcode']) {
      //
      return <a onClick={() => this.submitskill3(cell)}>{row['countskill3']}</a>
    }
  }
  submiteducation(ecode){
    this.handleSelect(2)
    this.props.fetchData({where:"p.educode1 = "+ecode.substring(0,1)+" and p.educode2 = "+ecode.substring(1),groupby:"",sort:"p.percent desc",have:""})
  }
  submiteducation2(ecode){
    this.handleSelect(2)
    this.props.fetchData({where:"p.unit3 = 0 and p.educode1 = "+ecode.substring(0,1)+" and p.educode2 = "+ecode.substring(1),groupby:"",sort:"p.percent desc",have:""})
  }
  submiteducation3(ecode){
    this.handleSelect(2)
    this.props.fetchData({where:"p.unit3 != 0 and p.educode1 = "+ecode.substring(0,1)+" and p.educode2 = "+ecode.substring(1),groupby:"",sort:"p.percent desc",have:""})
  }
  submitskill(skillcode){
    this.handleSelect(2)
    this.props.fetchData({where:"p.skillcode = '"+skillcode+"'",groupby:"",sort:"p.percent desc",have:""})
  }
  submitskill2(skillcode){
    this.handleSelect(2)
    this.props.fetchData({where:"p.unit3 = 0 and p.skillcode = '"+skillcode+"'",groupby:"",sort:"p.percent desc",have:""})
  }
  submitskill3(skillcode){
    this.handleSelect(2)
    this.props.fetchData({where:"p.unit3 != 0 and p.skillcode = '"+skillcode+"'",groupby:"",sort:"p.percent desc",have:""})
  }
  componentDidMount() {
    this.props.counteducation()
    this.props.countskill()
  }
  componentWillReceiveProps(nextProps){
    if(tableData!=nextProps.tableData){
      tableData = nextProps.tableData
      this.props.alert({type:"", timeout: 1 }) // ปิด alert โหลดข้อมูล
    }
    if (nextProps.view.state == 1) {
      
      if (nextProps.view.unit == 31) {
        this.props.fetchData({ where: "p.statuscode = 'AA' and p.educode1 > 3", sort: "p.educode1 desc,p.educode2,p.percent desc", groupby: "", have: "", name: "", sname: "", id8: "" })
      } else {
        this.props.fetchData({ where: "p.unit3=" + nextProps.view.unit + " and p.statuscode = 'AA' and p.educode1 > 3", sort: "p.educode1 desc,p.educode2,p.percent desc", groupby: "", have: "", name: "", sname: "", id8: "" })
      }
      this.props.changetab({ changetab: 2, unit: nextProps.view.unit, state: 0 })
      
    }
    if (nextProps.changeAllUnit3.hasOwnProperty('success') && query == 1) {
      query = 0
    }
  }
  handleSelect(key) {
    this.props.educationtab({ changetab: key })
  }
  render() {
    return (
      <div>
        <Col md={9}>
          <Panel header="วุฒิ/ความสามารถพิเศษ" bsStyle="primary">
            <Tabs activeKey={this.props.view.changetab} onSelect={this.handleSelect} id="uncontrolled-tab-example">
              <Tab eventKey={1} title="วุฒิ">
                <BootstrapTable
                  className="table-responsive"
                  data={this.props.counteducationData}
                  search={true} height="500">
                  <TableHeaderColumn dataField='educname' dataSort={true}>วุฒิ</TableHeaderColumn>
                  <TableHeaderColumn dataField='ecode' dataFormat={this.eduformat} dataSort={true}>จำนวน</TableHeaderColumn>
                  <TableHeaderColumn dataField='ecode' dataFormat={this.eduformat3} dataSort={true}>จัดแล้ว</TableHeaderColumn>
                  <TableHeaderColumn dataField='ecode' dataFormat={this.eduformat2} dataSort={true}>ยังไม่จัด</TableHeaderColumn>
                  <TableHeaderColumn
                    dataField="ecode"
                    isKey={true}
                    hidden></TableHeaderColumn>
                  <TableHeaderColumn
                    dataField="counteducation"
                    hidden></TableHeaderColumn>
                </BootstrapTable>

              </Tab>
              <Tab eventKey={3} title="ความสามารถพิเศษ">
              <BootstrapTable
                  className="table-responsive"
                  data={this.props.countskillData}
                  search={true} height="500">
                  <TableHeaderColumn dataField='skill' dataSort={true}>วุฒิ</TableHeaderColumn>
                  <TableHeaderColumn dataField='skillcode' dataFormat={this.skillformat} dataSort={true}>จำนวน</TableHeaderColumn>
                  <TableHeaderColumn dataField='skillcode' dataFormat={this.skillformat3} dataSort={true}>จัดแล้ว</TableHeaderColumn>
                  <TableHeaderColumn dataField='skillcode' dataFormat={this.skillformat2} dataSort={true}>ยังไม่จัด</TableHeaderColumn>
                  <TableHeaderColumn
                    dataField="skillcode"
                    isKey={true}
                    hidden></TableHeaderColumn>
                  <TableHeaderColumn
                    dataField="countskill"
                    hidden></TableHeaderColumn>
                    </BootstrapTable>
              </Tab>
              <Tab eventKey={2} title="จัดแบ่ง">
                <Table_changeUnit3 data={this.props.tableData}  type="Education"/>
              </Tab>
            </Tabs>
          </Panel>
        </Col>
        <Col md={3}>
          <Hisrequest tabactive={5} />
        </Col>
      </div>
    );
  }
}
function mapStatetoProps(state) {
  return {
    view: state.view,
    counteducationData: state.counteducationData,
    countskillData: state.countskillData,
    tableData: state.tableData,
    changeAllUnit3:state.changeAllUnit3
  }
}

function mapDispatchToProps(dispatch) {
  return {
    
    educationtab: educationtab => dispatch({ type: "CHANGE_VIEW_DATA", payload: educationtab }),
    countskill: () => dispatch({ type: "FETCH_COUNTSKILL_DATA" }),
    counteducation: () => dispatch({ type: "FETCH_COUNTEDUCATION_DATA" }),
    fetchData: where => dispatch({ type: "FETCH_TABLE_DATA", payload: where }),
    fetchData: sort => dispatch({ type: "FETCH_TABLE_DATA", payload: sort }),
    fetchData: groupby => dispatch({ type: "FETCH_TABLE_DATA", payload: groupby }),
    fetchData: have => dispatch({ type: "FETCH_TABLE_DATA", payload: have }),
    alert: type => dispatch({ type: "ALERTS", payload: type }),
    alert: headline => dispatch({ type: "ALERTS", payload: headline }),
    alert: message => dispatch({ type: "ALERTS", payload: message }),
    alert: timeout => dispatch({ type: "ALERTS", payload: timeout }),
    alert: position => dispatch({ type: "ALERTS", payload: position }),
    changetab: changetab => dispatch({ type: "CHANGE_VIEW_DATA", payload: changetab }),
    changetab: unit => dispatch({ type: "CHANGE_VIEW_DATA", payload: unit }),
    changetab: state => dispatch({ type: "CHANGE_VIEW_DATA", payload: state }),

  }
}

const Connectedrequest_Education = connect(mapStatetoProps, mapDispatchToProps)(Education)

export default Connectedrequest_Education
