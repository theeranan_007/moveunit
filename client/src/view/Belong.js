import React, { Component } from 'react';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import Hisrequest from '../component/Hisrequest'
import { Col, Panel, Tab, Tabs, Row } from 'react-bootstrap'
import Table_changeUnit3 from '../component/Table_changeUnit3';
import { connect } from 'react-redux'
class Belong extends Component {
  constructor(props) {
    super(props)
    this.state = {

    }
    this.handleSelect = this.handleSelect.bind(this)
    this.CountFormat1 = this.CountFormat1.bind(this)
    this.CountFormat2 = this.CountFormat2.bind(this)
    this.querytab2 = this.querytab2.bind(this)
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.view.state >= 1) {
      if (nextProps.view.unit == 31) {
        this.props.fetchData({ where: "p.batt=5 and p.oldyearin is null", sort: "p.statuscode", groupby: "", have: "" })
      } else {
        this.props.fetchData({ where: "p.batt=5 and p.oldyearin is null and p.unit3=" + nextProps.view.unit, sort: "p.statuscode", groupby: "", have: "" })
      }

    this.props.belongtab({ belongtab: 1, unit: nextProps.view.unit, state: 0 })
    this.props.alert({ type: "", timeout: 1 }) // ปิด alert โหลดข้อมูล
  }
}
querytab2(refnum, sw) {
  this.props.belongtab({ belongtab: 1, state: sw, unit: refnum })
}
CountFormat1(cell, row) {
  //this.props.requesttab({ requesttab: 2 ,state:1})
  return <a onClick={() => this.querytab2(cell, 1)}>{row['total']}</a>
}
CountFormat2(cell, row) {
  //this.props.requesttab({ requesttab: 2 ,state:1})
  return <a onClick={() => this.querytab2(cell, 1)}>{row['count1']}</a>
}
handleSelect(key) {
  this.props.belongtab({ belongtab: key })
}
render() {
  return (
    <div>
      <Col md={9}>
        <Panel header="ร้อยเตรียม" bsStyle="primary">
          <Tabs activeKey={this.props.view.belongtab} onSelect={this.handleSelect} id="uncontrolled-tab-example">
            <Tab eventKey={1} title="จัดแบ่ง">
              <Table_changeUnit3 data={this.props.tableData} type="Belong"/>
            </Tab>
            <Tab eventKey={2} title="ยอด">
              <Row>
                <Col sm={12}>
                  <BootstrapTable
                    className="table-responsive"
                    data={this.props.hisrequestbelongData.response}
                    search={false}>
                    <TableHeaderColumn dataField='unitname' >หน่วย</TableHeaderColumn>
                    <TableHeaderColumn dataField='refnum' dataFormat={this.CountFormat1}>จำนวนทั้งสิ้น</TableHeaderColumn>
                    <TableHeaderColumn dataField='refnum' dataFormat={this.CountFormat2}>ร้อยเตรียม</TableHeaderColumn>
                    <TableHeaderColumn isKey={true} hidden={true} dataField='refnum'>unit</TableHeaderColumn>
                  </BootstrapTable>
                </Col>
              </Row>
            </Tab>
          </Tabs>
        </Panel>
      </Col>
      <Col md={3}>
        <Hisrequest tabactive={7} />
      </Col>
    </div>
  );
}
}
function mapStatetoProps(state) {
  return {
    view: state.view,
    hisrequestbelongData: state.hisrequestbelongData,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    belongtab: belongtab => dispatch({ type: "CHANGE_VIEW_DATA", payload: belongtab }),
    belongtab: state => dispatch({ type: "CHANGE_VIEW_DATA", payload: state }),
    belongtab: unit => dispatch({ type: "CHANGE_VIEW_DATA", payload: unit }),
    fetchData: where => dispatch({ type: "FETCH_TABLE_DATA", payload: where }),
    fetchData: sort => dispatch({ type: "FETCH_TABLE_DATA", payload: sort }),
    fetchData: groupby => dispatch({ type: "FETCH_TABLE_DATA", payload: groupby }),
    fetchData: have => dispatch({ type: "FETCH_TABLE_DATA", payload: have }),
    alert: type => dispatch({ type: "ALERTS", payload: type }),
    alert: headline => dispatch({ type: "ALERTS", payload: headline }),
    alert: message => dispatch({ type: "ALERTS", payload: message }),
    alert: timeout => dispatch({ type: "ALERTS", payload: timeout }),
    alert: position => dispatch({ type: "ALERTS", payload: position }),
  }
}
const Connectedrequest_Belong = connect(mapStatetoProps, mapDispatchToProps)(Belong)

export default Connectedrequest_Belong
