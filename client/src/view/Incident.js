import React, { Component } from 'react';
import Select from 'react-select';
import {
  Col,
  Row,
  Tab,
  Tabs,
  Panel,
  ControlLabel,
  Button
} from 'react-bootstrap';
import Hisrequest from '../component/Hisrequest'
import Table_changeUnit3 from '../component/Table_changeUnit3';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import FontAwesome from 'react-fontawesome';

var switchquery = 0;
var queryValue = [];
var query = 0
class Incident extends Component {
  constructor(props) {
    super(props)
    this.state = {
      valuestatus: "",
      data: [],
      optionsstatus: [],
      position: "top-left",
      alerts: [],
      timeout: 0,
      newMessage: "This is a test of the Emergency Broadcast System. This is only a test.",
      inputvalidate: null
    }
    this.submit = this.submit.bind(this)
    this.logChangestatus = this.logChangestatus.bind(this)
    this.handleSelect = this.handleSelect.bind(this)
    this.loadstatus = this.loadstatus.bind(this)
  }

  componentDidMount() {
    this.props.fetchData({where: "p.statuscode!='AA'", sort: "p.statuscode asc,p.percent desc", groupby: "p.statuscode"})
    switchquery = 0;
  }
  loadstatus(){
    
  }
  componentWillReceiveProps(nextProps) {
    console.log(nextProps.view)
    if (nextProps.view.state == 1) {
      if (nextProps.view.unit == 31) {
        this.props.fetchData({ where: "p.statuscode != 'AA'", sort: "p.statuscode,p.batt,p.company,p.id8", groupby: "", have: "", name: "", sname: "", id8: "" })
      } else {
        this.props.fetchData({ where: "p.statuscode != 'AA' and p.unit3=" + nextProps.view.unit, sort: "p.statuscode,p.batt,p.company,p.id8", groupby: "", have: "", name: "", sname: "", id8: "" })
      }
      this.props.incidenttab({ incidenttab: 2, unit: nextProps.view.unit, state: 0 })
    }
    if (nextProps.changeAllUnit3.hasOwnProperty('success') && query == 1) {
      query = 0
    }
    if (switchquery == 0) {
      if (nextProps.incidentData.length != 0) {
        this.props.alert({type:"", timeout: 1 }) // ปิด alert โหลดข้อมูล
        var tmp = []
        this.props.fetchData({ where: "p.statuscode = '1234' ", sort: "", groupby: "", have: "", name: "", sname: "", id8: "" })
        tmp = nextProps.incidentData;
        queryValue = tmp;
        tmp = [];
        var c = 0;
        var total = 0
        for (var i = 0; i < nextProps.incidentData.length; i++) {
          tmp[i] = {
            label: nextProps.incidentData[i].title + "(" + nextProps.incidentData[i].count + ")",
            value: nextProps.incidentData[i].statuscode
          }
          total += nextProps.incidentData[i].count;
          c = i;
        }
        tmp[nextProps.incidentData.length] = {
          label: "รวม (" + total + ")",
          value: -1
        }
        this.setState({ optionsstatus: tmp })
        switchquery = 2;
      }
    }
    else {
      this.props.alert({type:"", timeout: 1 }) // ปิด alert โหลดข้อมูล
    }
  }
  logChangestatus(val) {
    this.setState({ valuestatus: val.value })

  }
  submit() {
    this.props.alert({type:"success",headline:"กำลังโหลด...",message:< div className="loader" > </div>, timeout:0,position:"top-right"})
    switchquery = 1;
    if (this.state.valuestatus == -1) {
      this
        .props
        .fetchData({ where: "p.statuscode!='AA'", sort: "p.statuscode,p.batt,p.company,p.id8", groupby: "" })
    } else {
      this
        .props
        .fetchData({
          where: "p.statuscode='" + this.state.valuestatus + "' ",
          sort: "p.statuscode,p.batt,p.company,p.id8",
          groupby: ""
        })
    }
  }
  handleSelect(key) {
    this.props.incidenttab({ incidenttab: key })
  }
  render() {
    return (
      <div>
        <Col md={9}>
          <Panel header="เหตุ" bsStyle="primary">
            {/* <Tabs activeKey={this.props.view.incidenttab} onSelect={this.handleSelect} id="uncontrolled-tab-example">
              <Tab eventKey={1} title="ค้นหา"> */}
                <br />
                <Row>
                  <Col sm={1}>
                    <ControlLabel>เหตุ</ControlLabel>
                  </Col>
                  <Col sm={10} >
                    <Select
                      placeholder="สถานะ"
                      name="status"
                      clearable={false}
                      value={this.state.valuestatus}
                      options={this.state.optionsstatus}
                      onChange={this.logChangestatus}
                      onClick={this.loadstatus}
                      ref="status" />
                  </Col>
                  <Col sm={1}>
                    <Button bsStyle="primary" className=" pull-right" onClick={this.submit}><FontAwesome name="search" />&nbsp;ค้นหา</Button>
                  </Col>
                </Row>
                <br />
                <Table_changeUnit3
                  data={this.props.tableData} type="Incident"/>
              {/* </Tab>
            </Tabs> */}
          </Panel>
        </Col>
        <Col md={3}>
          <Hisrequest tabactive={2} />
        </Col>

      </div>
    );
  }
}
function mapStatetoProps(state) {
  return {
    unittabData: state.unittabData,
    incidentData: state.incidentData,
    tableData: state.tableData,
    view: state.view,
    changeAllUnit3: state.changeAllUnit3,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    incidenttab: incidenttab => dispatch({ type: "CHANGE_VIEW_DATA", payload: incidenttab }),
    incidenttab: unit => dispatch({ type: "CHANGE_VIEW_DATA", payload: unit }),
    incidenttab: state => dispatch({ type: "CHANGE_VIEW_DATA", payload: state }),
    fetchData: where => dispatch({ type: "FETCH_TABLE_DATA", payload: where }),
    fetchData: sort => dispatch({ type: "FETCH_TABLE_DATA", payload: sort }),
    fetchData: groupby => dispatch({ type: "FETCH_TABLE_DATA", payload: groupby }),
    fetchData: have => dispatch({ type: "FETCH_TABLE_DATA", payload: have }),
    fetchUnittab: navyid => dispatch({ type: "FETCH_UNITTAB_DATA" }),
    fetchIncident: dispatch({ type: "FETCH_INCIDENT_DATA" }),
    alert: type => dispatch({ type: "ALERTS", payload: type }),
    alert: headline => dispatch({ type: "ALERTS", payload: headline }),
    alert: message => dispatch({ type: "ALERTS", payload: message }),
    alert: timeout => dispatch({ type: "ALERTS", payload: timeout }),
    alert: position => dispatch({ type: "ALERTS", payload: position }),
  }
}

const Connectedrequest_Incident = connect(mapStatetoProps, mapDispatchToProps)(Incident)

export default Connectedrequest_Incident
