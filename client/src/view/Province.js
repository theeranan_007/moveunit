import React, { Component } from 'react';
import Search from '../component/Search'
import Hisrequest from '../component/Hisrequest'
import { Col, Panel, Tab, Tabs, Row, ControlLabel, Button, Table, FormControl } from 'react-bootstrap'
import { connect } from 'react-redux'
import FontAwesome from 'react-fontawesome';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import Select from 'react-select';
import queryAmount from '../queryclient/queryAmount';
import Table_changeUnit3 from '../component/Table_changeUnit3';
import pdfMake from "pdfmake/build/pdfmake";
import pdfFonts from "pdfmake/build/vfs_fonts";
pdfMake.vfs = pdfFonts.pdfMake.vfs;
var valueArmtown = ["01", "02"];
var tmp = [], providecount = [], amountData = [], religioncount = [];
let calculator = false;
var all = 0, count1 = 0, count_total = 0, pdfdata = [], count3 = 0, unit_2 = 0, count_update = 0, balance = 0, queryAmountstate = 1
class Province extends Component {
  constructor(props) {
    super(props)
    this.onBeforeSaveCell = this.onBeforeSaveCell.bind(this)
    this.onAfterSaveCell = this.onAfterSaveCell.bind(this)
    this.handlePercent_unit2 = this.handlePercent_unit2.bind(this)
    this.handleNonChange = this.handleNonChange.bind(this)
    this.state = {
      cellEditProp: {
        mode: 'click',
        blurToSave: true,
        beforeSaveCell: this.onBeforeSaveCell, // a hook for before saving cell
        afterSaveCell: this.onAfterSaveCell  // a hook for after saving cell
      },
      valueArmtown: ['25', '32', '45'],
      providecount: [],
      amountData: [],
      tabactive: 1,
      banch: 1,
      valueChangeAllUnit3: "",
      valuelimit: "",
      armtownitem: "25,32,45",
      valueNonChange: 0,
      valuePercent_unit2: 75
    }
    this.handleLimit = this.handleLimit.bind(this)
    this.submitunit2 = this.submitunit2.bind(this)
    this.submitchangeunit = this.submitchangeunit.bind(this)
    this.logChangeArmtown = this.logChangeArmtown.bind(this)
    this.submit = this.submit.bind(this)
    this.handleSelect = this.handleSelect.bind(this)
    this.logChangeAllUnit3 = this.logChangeAllUnit3.bind(this)
    this.REG1 = this.REG1.bind(this)
    this.REG2 = this.REG2.bind(this)
    this.REGALL = this.REGALL.bind(this)
    this.armtownformat = this.armtownformat.bind(this)
    this.submitarmtown = this.submitarmtown.bind(this)
    this.submitcalculator = this.submitcalculator.bind(this)
    this.refresh = this.refresh.bind(this)
  }
  exportPDF() {
    // const { vehicleData } = this.props.parkedVehicle; const { plate_no, max_time,
    //   entry_date_time,   exit_date_time,   expiry_time,   address1, address2,
    // city,   state,   zip,   country,   parking_status } = vehicleData; var
    // converter = new pdfConverter(); var doc = converter.jsPDF('p', 'pt');
    if (this.state.amountData.length == 0) {
      this.props.alert({ type: "danger", headline: "ผิดพลาด", message: "กรุณาคำนวนก่อน", timeout: 2000, position: "top-right" })
      //return false
    }
    var data_province = [], data_amount = [],
      contstr = [],
      j = 0
    var data_province = [
      [
        {
          border: [
            true, true, true, true
          ],
          style: 'center',
          text: 'จังหวัด'
        }, {
          border: [
            false, true, true, true
          ],
          style: 'center',
          text: 'จำนวน'
        }, {
          border: [
            false, false, false, false
          ],
          text: ''
        }
      ]
    ]
    //data.push(header);
    this.state.providecount.map((item, i) => {
      if (item.ARMNAME == "รวม") {
        data_province.push([
          {
            border: [true, true, true, true],
            styles: { alignment: 'left' },
            fillColor: item.hilight == 1 ? '#cccccc' : '',
            noWrap: true,
            text: item.ARMNAME
          },
          {
            border: [false, true, true, true],
            style: 'center',
            fillColor: item.hilight == 1 ? '#cccccc' : '',
            noWrap: true,
            text: item.all
          },
          {
            border: [false, false, false, false],
            style: 'center',
            fillColor: item.hilight == 1 ? '#cccccc' : '',
            noWrap: true,
            text: ""
          },
        ])
      }
      else {
        switch (i) {
          case 0: {
            data_province.push([
              {
                border: [true, false, true, false],
                styles: { alignment: 'left' },
                fillColor: item.hilight == 1 ? '#cccccc' : '',
                noWrap: true,
                text: item.ARMNAME
              },
              {
                border: [false, false, true, false],
                style: 'center',
                fillColor: item.hilight == 1 ? '#cccccc' : '',
                noWrap: true,
                text: item.all
              },
              {
                border: [false, false, false, false],
                style: 'center',
                fillColor: item.hilight == 1 ? '#cccccc' : '',
                noWrap: true,
                text: "    ไม่จัดแบ่ง  " + this.state.valueNonChange
              },
            ])
          } break;
          case 1: {
            data_province.push([
              {
                border: [true, false, true, false],
                styles: { alignment: 'left' },
                fillColor: item.hilight == 1 ? '#cccccc' : '',
                noWrap: true,
                text: item.ARMNAME
              },
              {
                border: [false, false, true, false],
                style: 'center',
                fillColor: item.hilight == 1 ? '#cccccc' : '',
                noWrap: true,
                text: item.all
              },
              {
                border: [false, false, false, false],
                style: 'center',
                fillColor: item.hilight == 1 ? '#cccccc' : '',
                noWrap: true,
                text: "    แบ่งให้ นย.  " + this.state.valuePercent_unit2
              },
            ])
          } break;
          default: {
            data_province.push([
              {
                border: [true, false, true, false],
                styles: { alignment: 'left' },
                fillColor: item.hilight == 1 ? '#cccccc' : '',
                noWrap: true,
                text: item.ARMNAME
              },
              {
                border: [false, false, true, false],
                style:'center',
                fillColor: item.hilight == 1 ? '#cccccc' : '',
                noWrap: true,
                text: item.all
              },
              {
                border: [false, false, false, false],
                style:  'center' ,
                fillColor: item.hilight == 1 ? '#cccccc' : '',
                noWrap: true,
                text: ""
              },
            ])
          } break;
        }

      }

    })
    var data_amount = [
      [
        {
          border: [
            true, true, true, false
          ],
          style: 'center',
          text: 'หน่วย'
        }, {
          border: [
            false, true, true, false
          ],
          style: 'center',
          text: 'จำนวน'
        }, {
          border: [
            false, true, true, false
          ],
          style: 'center',
          text: 'เฉลี่ย'
        }, {
          border: [
            false, true, true, false
          ],
          style: 'center',
          text: 'จัดให้'
        }, {
          colSpan: 3,
          border: [
            false, true, true, true
          ],
          style: 'center',
          text: 'จัดแบ่งจริง'
        }, '', ''
      ], [
        {
          border: [
            true, false, true, true
          ],
          style: 'center',
          text: ''
        }, {
          border: [
            false, false, true, true
          ],
          style: 'center',
          text: ''
        }, {
          border: [
            false, false, true, true
          ],
          text: ''
        }, {
          border: [
            false, false, true, true
          ],
          text: ''
        }, {
          border: [
            false, false, true, true
          ],
          style: 'center',
          text: 'อิสลาม'
        }, {
          border: [
            false, false, true, true
          ],
          style: 'center',
          text: 'อื่นๆ'
        }, {
          border: [
            false, false, true, true
          ],
          style: 'center',
          text: 'รวม'
        }
      ]
    ]
    this.state.amountData.map((item, i) => {
      if (item.unitname == "รวม") {
        data_amount.push([
          {
            border: [
              true, true, true, true
            ],
            text: item.unitname
          }, {
            border: [
              false, true, true, true
            ],
            text: item.count
          }, {
            border: [
              false, true, true, true
            ],
            text: item.percent
          }, {
            border: [
              false, true, true, true
            ],
            text: item.manual_percent
          }, {
            border: [
              false, true, true, true
            ],
            text: item.REG1
          }, {
            border: [
              false, true, true, true
            ],
            text: item.REG2
          }, {
            border: [
              false, true, true, true
            ],
            text: item.REGALL
          }
        ])
      }
      else if (item.count != 0) {
        data_amount.push([
          {
            border: [
              true, false, true, false
            ],
            text: item.unitname
          }, {
            border: [
              false, false, true, false
            ],
            text: item.count
          }, {
            border: [
              false, false, true, false
            ],
            text: item.percent
          }, {
            border: [
              false, false, true, false
            ],
            text: item.manual_percent
          }, {
            border: [
              false, false, true, false
            ],
            text: item.REG1
          }, {
            border: [
              false, false, true, false
            ],
            text: item.REG2
          }, {
            border: [
              false, false, true, false
            ],
            text: item.REGALL
          }
        ])
      }
    })
    var docDefinition = {
      content: [
        {
          text: 'จัดแบ่งทหาร 3 จชต',
          style: 'center'
        },
        {
          table: {
            widths: [
              '35%',
              '35%',
              '30%',

            ],
            body: data_province
          }
        }, {
          height: "40",
          text: '.',
          color: '#fff',
          style: 'center'
        }, {
          table: {
            headerRows: 2,
            widths: [
              '22%',
              '13%',
              '13%',
              '13%',
              '13%',
              '13%',
              '13%',

            ],
            body: data_amount
          }
        }
      ],

      defaultStyle: {
        font: 'THSarabun',
        fontSize: 16
      },
      styles: {
        center: {
          alignment: 'center'
        },
        tableExample: {
          widths: '100%'
        },
        tableHeader: {
          // bold: true, fontSize: 10,
        }
      },
      // a string or { width: number, height: number }
      pageSize: 'A4',

      // by default we use portrait, you can change it to landscape if you wish
      pageOrientation: 'portrait',
      // pageOrientation: 'landscape',

      // [left, top, right, bottom] or [horizontal, vertical] or just a number for
      // equal margins
      pageMargins: [5, 10, 5, 0]
    };
    pdfMake.fonts = {
      THSarabun: {
        normal: 'THSarabun.ttf',
        bold: 'THSarabun Bold.ttf',
        italics: 'THSarabun Italic.ttf',
        bolditalics: 'THSarabun BoldItalic.ttf'
      }
    }
    // pdfMake.createPdf(docDefinition).download();
    console.log(docDefinition)
    pdfMake.createPdf(docDefinition).download();

    // pdfMake.createPdf(docDefinition).print(); var doc = new jsPDF('L','mm','A4');
    // doc.addFont('THSarabun.ttf', 'THSarabun', 'normal'); doc.setFont("THSarabun")
    // doc.text(20, 50, 'Park Entry Ticketทดสอบ'); function
    // open_data_uri_window(url) {   var html = '<html>' +     '<style>html, body {
    // padding: 0; margin: 0; } iframe { width: 100%; height: 100%; border: 0;}
    // </style>' +     '<body>' +     '<iframe src="' + url + '"></iframe>' +
    // '</body></html>';   var open = window.open("","_blank")
    // open.document.write(html) } doc.save('custom_fonts.pdf');
    // open_data_uri_window(doc.output('datauri')); doc.text(20, 80, 'Address1: ' +
    // address1); doc.text(20, 100, 'Address2: ' + address2); doc.text(20, 120,
    // 'Entry Date & time: ' + entry_date_time); doc.text(20, 140, 'Expiry date &
    // time: ' + exit_date_time); doc.viewerPreferences({'FitWindow': true}, true)
    // doc.output('save', 'filename.pdf'); //Try to save PDF as a file (not works on
    // ie before 10, and some mobile devices) doc.output('datauristring');
    // //returns the data uri string doc.output('datauri');              //opens the
    // data uri in current window doc.output('dataurlnewwindow');     //opens the
    // data uri in new window doc.viewerPreferences({ 'HideWindowUI': true,
    // 'PrintArea': 'CropBox', 'NumCopies': 10 })
  }
  handleNonChange(event) {
    this.setState({ valueNonChange: event.target.value })
  }
  handlePercent_unit2(event) {
    this.setState({ valuePercent_unit2: event.target.value })
  }
  REG1(cell, row, formatExtraData, rowIdx) {
    return <a onClick={() => this.submitchangeunit(rowIdx, 'REG1')}>{cell}</a>
  }
  REG2(cell, row, formatExtraData, rowIdx) {
    return <a onClick={() => this.submitchangeunit(rowIdx, 'REG2')}>{cell}</a>
  }
  REGALL(cell, row, formatExtraData, rowIdx) {
    return <a onClick={() => this.submitchangeunit(rowIdx, 'REGALL')}>{cell}</a>
  }
  armtownformat(cell, row, formatExtraData, rowIdx) {
    if (row.ARMID == cell) {
      return <a onClick={() => this.submitarmtown(cell)}>{row.all}</a>
    }
  }
  submitcalculator() {
    if (queryAmountstate == 0)
      this.props.alert({ type: "success", headline: "กำลังโหลด...", message: < div className="loader" > </div>, timeout: 0, position: "top-right" })
    if (this.state.valueNonChange == null || this.state.valueNonChange == "") {
      this.setState({ valueNonChange: 0 })
    }
    this.setState({ amountData: [] })
    var tmp = this.props.providecount
    providecount = this.props.providecount
    all = 0
    let length = 0
    this.props.providecount.forEach((element, i) => {
      if (element.ARMNAME != 'รวม') {
        all += element.all
      }
      if (element.ARMNAME == 'รวม') {
        length = i;
        return
      }
      else {
        length = i + 1;
        return
      }
    });
    count_total = all - this.state.valueNonChange;
    tmp[length] = {
      'ARMNAME': 'รวม',
      'ARMID': -1,
      'all': all,
    }
    this.setState({ providecount: tmp })
    tmp = this.props.amountData
    amountData = this.props.amountData;
    var check_update_amount = queryAmountstate;
    queryAmountstate = 0
    var tmp2 = []
    var count = 0, percent = 0, manual_percent = 0, REG1 = 0, REG2 = 0, REG3 = 0, REGALL = 0
    tmp.forEach((element, i) => {
      tmp2[i] = { ...element, fullname: element.refnum + "." + element.unitname }
      count += parseInt(tmp2[i].count);
      percent += parseInt(tmp2[i].percent);
      manual_percent += parseInt(tmp2[i].manual_percent);
      REG1 += parseFloat(tmp2[i].REG1);
      REG2 += parseFloat(tmp2[i].REG2);
      REG3 += parseFloat(tmp2[i].REG3);
      REGALL += parseFloat(tmp2[i].REGALL);
      console.log(tmp2[i].manual_percent)
    });
    tmp2[tmp2.length] = {
      refnum: 31, unitname: "รวม", fullname: "รวม", count: count, percent: percent, manual_percent: manual_percent,
      REG1: REG1, REG2: REG2, REG3: REG3, REGALL: REGALL
    }
    var balance_province = count_total - parseInt(this.state.valuePercent_unit2);
    console.log(balance_province + " = " + count_total + " - " + parseInt(this.state.valuePercent_unit2))
    var balance_hisrequest = tmp2[31].count - tmp2[2].count - this.state.valueNonChange
    var all_percent = 0;
    tmp2.forEach((element, i) => {
      if (element.refnum != 2 && element.refnum != 0) {
        var balance_average = parseFloat(balance_province / balance_hisrequest)
        var total = (balance_average) * tmp2[i].count
        tmp2[i].percent = balance_average * 100
        tmp2[i].percent = total.toFixed(2)
      }
      if (element.refnum == 2) {
        tmp2[i].percent = (this.state.valuePercent_unit2).toFixed(2);
      }
      if (i != 31) {
        all_percent = (parseFloat(tmp2[i].percent) + parseFloat(all_percent)).toFixed(2);
        console.log(parseInt(tmp2[i].percent))
        if (check_update_amount == 0) {
          this.props.fetchUpdateAmount({ unit: i, count: "-1", percent: parseInt(tmp2[i].percent) })
        }
      }
      if (i == 31) {
        tmp2[i].percent = all_percent;
      }
    });
    queryAmountstate = 0;
    this.setState({ amountData: tmp2 })
    this.props.fetchHisrequest_province({ armid: this.state.armtownitem })
  }
  submitarmtown(armid) {
    this.handleSelect(2)
    if (armid == -1) {
      this.props.fetchData({ where: "p.armid in (" + this.state.armtownitem + ")", sort: "p.unit1,p.unit2,p.percent desc,p.id8", groupby: "" })
    }
    else {
      this.props.fetchData({ where: "p.armid in (" + armid + ")", sort: "p.unit1,p.unit2,p.percent desc,p.id8", groupby: "" })
    }
    this.props.alert({ type: "success", headline: "กำลังโหลด...", message: < div className="loader" > </div>, timeout: 0, position: "top-right" })

  }
  handleLimit(event) {
    this.setState({ valuelimit: event.target.value })
  }
  logChangeAllUnit3(val) {
    if (val != null) {
      this.setState({ valueChangeAllUnit3: val.value })
      this.state.amountData.forEach(element => {
        if (element.refnum == val.value) {
          this.setState({ valuelimit: parseInt(element.percent2) - parseInt(element.REGALL) })
        }
      });
    }
    else
      this.setState({ valueChangeAllUnit3: "" })
  }
  submitunit2() {
    this.handleSelect(2)
    // this.props.fetchData({ where: "p.unit3 in (0,2) and p.armid in (" + this.state.valueArmtown + ")", sort: "p.unit1,p.unit2,p.percent desc", groupby: "" })
  }
  submitchangeunit(refnum, reg) {
    this.handleSelect(2)
    if (refnum != 31) {
      if (reg == "REG1") {
        this.props.fetchData({ where: "p.unit3 = " + refnum + " and p.regcode != 2 and p.armid in (" + this.state.valueArmtown + ")", sort: "p.unit1,p.unit2,p.percent desc", groupby: "" })
      }
      else if (reg == "REG2") {
        this.props.fetchData({ where: "p.unit3 = " + refnum + " and p.regcode = 2 and p.armid in (" + this.state.valueArmtown + ")", sort: "p.unit1,p.unit2,p.percent desc", groupby: "" })
      }
      else {
        this.props.fetchData({ where: "p.unit3 = " + refnum + " and p.armid in (" + this.state.valueArmtown + ")", sort: "p.unit1,p.unit2,p.percent desc", groupby: "" })

      }
    }
    else {
      if (reg == "REG1") {
        this.props.fetchData({ where: "p.regcode != 2 and p.armid in (" + this.state.valueArmtown + ")", sort: "p.unit1,p.unit2,p.percent desc", groupby: "" })
      }
      else if (reg == "REG2") {
        this.props.fetchData({ where: "p.regcode = 2 and p.armid in (" + this.state.valueArmtown + ")", sort: "p.unit1,p.unit2,p.percent desc", groupby: "" })
      }
      else {
        this.props.fetchData({ where: "p.armid in (" + this.state.valueArmtown + ")", sort: "p.unit1,p.unit2,p.percent desc", groupby: "" })

      }
    }
    this.props.alert({ type: "success", headline: "กำลังโหลด...", message: < div className="loader" > </div>, timeout: 0, position: "top-right" })

    // this.props.fetchData({ where: "p.unit3 != 2 and p.armid in (" + this.state.valueArmtown + ")", sort: "p.unit1,p.unit2,p.percent desc", groupby: "" })
  }
  onBeforeSaveCell(row, cellName, cellValue) {
    if (cellName == 'count') {
      if (cellValue == "") {
        return false
      }
    }
    if (cellName == 'manual_percent') {
      if (cellValue == "") {
        return false
      }
    }
    return true;
  }
  onAfterSaveCell(row, cellName, cellValue) {
    if (cellName == 'count') {
      this.props.fetchUpdateAmount({ unit: row['refnum'], count: cellValue, percent: "-1" })
    }
    if (cellName == 'manual_percent') {
      this.props.fetchUpdateAmount({ unit: row['refnum'], count: "-2", manual_percent: cellValue })
    }
    var count = 0, percent = 0, manual_percent = 0, REG1 = 0, REG2 = 0, REG3 = 0, REGALL = 0
    let tmp = this.state.amountData;
    tmp.forEach((element, i) => {
      if (i < 31) {
        count += parseInt(tmp[i].count);
        percent += parseInt(tmp[i].percent);
        manual_percent += parseInt(tmp[i].manual_percent);
        REG1 += parseFloat(tmp[i].REG1);
        REG2 += parseFloat(tmp[i].REG2);
        REG3 += parseFloat(tmp[i].REG3);
        REGALL += parseFloat(tmp[i].REGALL);
      }
    });
    tmp[31] = {
      refnum: 31, unitname: "รวม", fullname: "รวม", count: count, percent: percent, manual_percent: manual_percent,
      REG1: REG1, REG2: REG2, REG3: REG3, REGALL: REGALL
    }
    this.setState({ amountData: tmp })
    this.props.fetchHisrequest_province({ armid: this.state.armtownitem })
    queryAmountstate = 1
    return true
  }
  componentDidMount() {
    this.props.fetchAmount({ armid: "32,25,45" })
    this.props.fetchArmTown({ navyid: "" })
    queryAmountstate = 1
    this.props.fetchProvidecount({ armid: "32,25,45" })
    this.props.fetchData({ where: "p.armid in (32,25,45)", sort: "p.unit1,p.unit2,p.percent desc,p.id8", groupby: "" })
  }
  refresh() {
    queryAmountstate = 1
    this.props.fetchAmount({ armid: "32,25,45" })
  }
  componentWillReceiveProps(nextProps) {
    if (amountData != nextProps.amountData && queryAmountstate == 1) {

      this.submitcalculator();
    }
    if (nextProps.view.state == 1) {
      this.submitchangeunit(nextProps.view.unit, "")
      this.handleSelect(2)
      this.props.providetab({ providetab: 2, unit: nextProps.view.unit, state: 0 })
    }
    if (nextProps.updateamountData.hasOwnProperty('success') && count_update <= 30) {
      if (count_update == 30) {
        this.props.alert({ type: "", timeout: 1 })
        count_update = 0;
      } else {
        count_update++;
      }


    }

    if (providecount != nextProps.providecount) {
      var tmp = nextProps.providecount
      providecount = nextProps.providecount
      all = 0
      let length = 0
      nextProps.providecount.forEach((element, i) => {
        if (element.ARMNAME != 'รวม') {
          all += element.all
        }
        if (element.ARMNAME == 'รวม') {
          length = i;
          return
        }
        else {
          length = i + 1;
          return
        }
      });
      tmp[length] = {
        'ARMNAME': 'รวม',
        'ARMID': -1,
        'all': all,
      }
      this.setState({ providecount: tmp })
    }
    if (queryAmountstate) {
      if (nextProps.updateamountData.hasOwnProperty('success')) {
        this.props.fetchAmount({ armid: this.state.armtownitem })
        queryAmountstate = 0
      }
    }
  }
  logChangeArmtown(val) {
    var tmp = []

    val.forEach((element, i) => {
      tmp[i] = element.value;
    });
    this.setState({ valueArmtown: tmp })

  }
  submit(refnum) {
    var armtownitem = ""

    this.state.valueArmtown.forEach(element => {
      if (armtownitem == "")
        armtownitem = "\'" + element + "\'";
      else
        armtownitem += ",\'" + element + "\'";
    });
    this.setState({ armtownitem: armtownitem })
    this.props.fetchProvidecount({ armid: armtownitem })
    this.props.fetchHisrequest_province({ armid: armtownitem })
    this.props.fetchHisrequest_all()
    this.props.fetchAmount({ armid: armtownitem })
    this.props.fetchData({ where: "p.armid in (" + armtownitem + ")", sort: "p.unit1,p.unit2,p.percent desc,p.id8", groupby: "" })
    this.props.alert({ type: "success", headline: "กำลังโหลด...", message: < div className="loader" > </div>, timeout: 0, position: "top-right" })

  }
  handleSelect(key) {
    this.setState({ tabactive: key })
  }
  render() {
    return (
      <div>
        <Col md={9}>
          <Panel header="จังหวัด" bsStyle="primary">
            <Tabs activeKey={this.state.tabactive} onSelect={this.handleSelect} id="uncontrolled-tab-example">
              <Tab eventKey={1} title="คุมยอด">

                <Row>
                  <Col sm={11}>
                    <ControlLabel>เลือกจังหวัด</ControlLabel>
                  </Col>
                </Row>
                <Row>
                  <Col sm={11}>
                    <Select
                      placeholder="เลือกจังหวัด"
                      name="armtown"
                      multi
                      clearable={true}
                      value={this.state.valueArmtown}
                      options={this.props.armtown}
                      onChange={this.logChangeArmtown}
                      ref="armtown" />
                  </Col>
                  <Col sm={1}>
                    <Button
                      bsStyle="primary"
                      className=" pull-right"
                      onClick={this.submit}
                      ref={(armtownsubmit) => {
                        this.armtownsubmit = armtownsubmit;
                      }}><FontAwesome name="search" />&nbsp;ค้นหา</Button>
                  </Col>
                </Row>
                <Row>
                  <Col sm={12}>
                    <Button
                      bsStyle="default"
                      className=" pull-right"
                      onClick={() => this.refresh()}><FontAwesome name="refresh" />&nbsp;รีเฟรช</Button>
                    <Button
                      bsStyle="success"
                      className=" pull-right"
                      onClick={() => this.exportPDF()}><FontAwesome name="print" />&nbsp;&nbsp;ปริ้น&nbsp;</Button>
                  </Col>
                </Row>
                <Row>
                  <Col sm={10}>
                    <BootstrapTable
                      className="table-responsive"
                      data={this.state.providecount}
                      search={false}>
                      <TableHeaderColumn dataField='ARMNAME' >จังหวัด</TableHeaderColumn>
                      <TableHeaderColumn isKey={true} hidden dataField='ARMID' >จังหวัด</TableHeaderColumn>
                      <TableHeaderColumn hidden dataField='all' >จำนวนทั้งสิ้น</TableHeaderColumn>
                      <TableHeaderColumn hidden dataField='count1' >รร.ศึกษาผู้ใหญ่</TableHeaderColumn>
                      <TableHeaderColumn hidden dataField='count3' >ลาศึกษาต่อ</TableHeaderColumn>
                      <TableHeaderColumn dataField='ARMID' dataFormat={this.armtownformat} >จำนวนทั้งสิ้น</TableHeaderColumn>
                    </BootstrapTable>
                  </Col>
                  <Col sm={2}>
                    <Row>
                      <br />
                      <ControlLabel>ไม่ย้ายหน่วย</ControlLabel>
                      <FormControl
                        placeholder="ไม่ย้ายหน่วย"
                        type="number"
                        onChange={this.handleNonChange}
                        value=
                        {this.state.valueNonChange}
                      />
                    </Row>
                    <Row>
                      <br />
                      <ControlLabel>แบ่งให้ นย.</ControlLabel>
                      <FormControl
                        placeholder="%"
                        type="number"
                        onChange={this.handlePercent_unit2}
                        value=
                        {this.state.valuePercent_unit2} />
                    </Row>
                    <Row>
                      <Button
                        bsStyle="primary"
                        block
                        onClick={this.submitcalculator}>คำนวน</Button>
                    </Row>
                  </Col>
                </Row>
                <br />
                <Row>
                  <Col sm={12}>
                    <BootstrapTable
                      className="table-responsive"
                      data={this.state.amountData}
                      search={false}
                      cellEdit={this.state.cellEditProp}>
                      <TableHeaderColumn dataField='fullname' editable={false} row={0} rowSpan={2}>หน่วย</TableHeaderColumn>
                      <TableHeaderColumn dataField='count' row={0} rowSpan={2}>จำนวนทั้งสิ้น</TableHeaderColumn>
                      <TableHeaderColumn dataField='percent' editable={false} row={0} rowSpan={2}>เฉลี่ยให้หน่วย</TableHeaderColumn>
                      <TableHeaderColumn dataField='manual_percent' row={0} rowSpan={2}>จัดให้</TableHeaderColumn>
                      <TableHeaderColumn colSpan={3} row={0} editable={false}>จัดแบ่งจริง</TableHeaderColumn>
                      <TableHeaderColumn dataField='REG2' dataFormat={this.REG2} editable={false} row={1}>อิสลาม</TableHeaderColumn>
                      <TableHeaderColumn dataField='REG1' dataFormat={this.REG1} editable={false} row={1}>อื่นๆ</TableHeaderColumn>
                      <TableHeaderColumn dataField='REGALL' dataFormat={this.REGALL} editable={false} row={1}>รวม</TableHeaderColumn>
                      <TableHeaderColumn isKey={true} hidden={true} editable={false} dataField='refnum'>unit</TableHeaderColumn>
                    </BootstrapTable>
                  </Col>
                </Row>
              </Tab>
              <Tab eventKey={2} title="จัดแบ่ง">
                <Row>
                  <Col sm={12}>
                    <Table_changeUnit3 data={this.props.tableData} type="Province" param={this.state.armtownitem} func={() => this.submit()} />
                  </Col>
                </Row>
              </Tab>
            </Tabs>
          </Panel>
        </Col>
        <Col md={3}>
          <Hisrequest tabactive={4} />
        </Col>
      </div>
    );
  }
}
function mapStatetoProps(state) {
  return {
    unittabData: state.unittabData,
    tableData: state.tableData,
    amountData: state.amountData,
    updateamountData: state.updateamountData,
    incidentData: state.incidentData,
    providecount: state.providecount,
    tableData: state.tableData,
    armtown: state.armtown,
    view: state.view
  }
}

function mapDispatchToProps(dispatch) {
  return {
    fetchProvidecount: armid => dispatch({ type: "FETCH_PROVIDECOUNT_DATA", payload: armid }),
    fetchData: where => dispatch({ type: "FETCH_TABLE_DATA", payload: where }),
    fetchData: sort => dispatch({ type: "FETCH_TABLE_DATA", payload: sort }),
    fetchData: groupby => dispatch({ type: "FETCH_TABLE_DATA", payload: groupby }),
    fetchData: have => dispatch({ type: "FETCH_TABLE_DATA", payload: have }),
    fetchUnittab: navyid => dispatch({ type: "FETCH_UNITTAB_DATA" }),
    fetchArmTown: navyid => dispatch({ type: "FETCH_ARMTOWN_DATA" }),
    fetchAmount: armid => dispatch({ type: "FETCH_AMOUNT_DATA", payload: armid }),
    fetchUpdateAmount: (
      unit => dispatch({ type: "FETCH_UPDATEAMOUNT_DATA", payload: unit }),
      count => dispatch({ type: "FETCH_UPDATEAMOUNT_DATA", payload: count }),
      percent => dispatch({ type: "FETCH_UPDATEAMOUNT_DATA", payload: percent }),
      manual_percent => dispatch({ type: "FETCH_UPDATEAMOUNT_DATA", payload: manual_percent })),
    fetchData: where => dispatch({ type: "FETCH_TABLE_DATA", payload: where }),
    fetchData: sort => dispatch({ type: "FETCH_TABLE_DATA", payload: sort }),
    fetchData: groupby => dispatch({ type: "FETCH_TABLE_DATA", payload: groupby }),
    fetchData: have => dispatch({ type: "FETCH_TABLE_DATA", payload: have }),
    fetchHisrequest_province: armid => dispatch({ type: "FETCH_HISREQUESTPROVINCE_DATA", payload: armid }),
    fetchHisrequest_all: navyid => dispatch({ type: "FETCH_HISREQUESTALL_DATA" }),

    providetab: providetab => dispatch({ type: "CHANGE_VIEW_DATA", payload: providetab }),
    providetab: unit => dispatch({ type: "CHANGE_VIEW_DATA", payload: unit }),
    providetab: state => dispatch({ type: "CHANGE_VIEW_DATA", payload: state }),
    alert: (
      type => dispatch({ type: "ALERTS", payload: type }),
      headline => dispatch({ type: "ALERTS", payload: headline }),
      message => dispatch({ type: "ALERTS", payload: message }),
      timeout => dispatch({ type: "ALERTS", payload: timeout }),
      position => dispatch({ type: "ALERTS", payload: position })
    ),
  }
}

const Connectedrequest_Province = connect(mapStatetoProps, mapDispatchToProps)(Province)

export default Connectedrequest_Province
