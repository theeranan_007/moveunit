import React, { Component } from 'react';
import {
  Col,
  Row,
  Tab,
  Tabs,
  Panel,
  ControlLabel,
  FormControl,
  Button
} from 'react-bootstrap';
import Hisrequest from '../component/Hisrequest'
import Table_changeUnit3 from '../component/Table_changeUnit3';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import FontAwesome from 'react-fontawesome';
var tableData = [], query = 0
class Height extends Component {
  constructor(props) {
    super(props)
    this.state = {
      valueHeightfrom: 170,
      valueHeightto: 180,
    }
    this.keypressHeightfrom = this.keypressHeightfrom.bind(this)
    this.handleHeightfrom = this.handleHeightfrom.bind(this)
    this.keypressHeightto = this.keypressHeightto.bind(this)
    this.handleHeightto = this.handleHeightto.bind(this)
    this.submit = this.submit.bind(this)
  }
  submit() {
    this.props.fetchData({ where: "p.statuscode = 'AA' and p.height between " + this.state.valueHeightfrom + " and " + this.state.valueHeightto, sort: "p.height desc,p.percent desc", groupby: "", have: "", name: "", sname: "", id8: "" })
    this.props.alert({ type: "success", headline: "กำลังโหลด...", message: < div className="loader" > </div>, timeout: 0, position: "top-right" })
    query = 1
  }
  keypressHeightfrom(event) {

  }
  handleHeightfrom(event) {
    this.setState({ valueHeightfrom: event.target.value })
  }
  keypressHeightto(event) {

  }
  handleHeightto(event) {
    this.setState({ valueHeightto: event.target.value })
  }
  componentWillReceiveProps(nextProps) {
    if (tableData != nextProps.tableData) {
      tableData = nextProps.tableData
      this.props.alert({ type: "", timeout: 1 }) // ปิด alert โหลดข้อมูล
    }
    if (nextProps.view.state == 1) {

      if (nextProps.view.unit == 31) {
        this.props.fetchData({ where: "p.statuscode = 'AA' and p.height between " + this.state.valueHeightfrom + " and " + this.state.valueHeightto, sort: "p.height desc,p.percent desc", groupby: "", have: "", name: "", sname: "", id8: "" })
      } else {
        this.props.fetchData({ where: "p.unit3=" + nextProps.view.unit + " and p.statuscode = 'AA' and p.height between " + this.state.valueHeightfrom + " and " + this.state.valueHeightto, sort: "p.height desc,p.percent desc", groupby: "", have: "", name: "", sname: "", id8: "" })
      }
      this.props.changetab({ changetab: 2, unit: nextProps.view.unit, state: 0 })

    }
    if (nextProps.changeAllUnit3.hasOwnProperty('success') && query == 1) {
      query = 0
    }
  }
  render() {
    return (
      <div>
        <Col md={9}>
          <Panel header="ความสูง" bsStyle="primary">
            <Row>
              <Col sm={2}>
                <ControlLabel>ความสูงระหว่าง</ControlLabel>
              </Col>
              <Col sm={3}>
                <FormControl
                  name="heightfrom"
                  type="number"
                  onChange={this.handleHeightfrom}
                  onKeyPress={this.keypressHeightfrom}
                  name="heightfrom"
                  value={this.state.valueHeightfrom}
                />
              </Col>
              <Col sm={1}>
                <ControlLabel style={{ textAlign: "center" }}>ถึง</ControlLabel>
              </Col>
              <Col sm={3}>
                <FormControl
                  name="heightto"
                  type="number"
                  onChange={this.handleHeightto}
                  onKeyPress={this.keypressHeightto}
                  name="height"
                  value={this.state.valueHeightto}
                />
              </Col>
              <Col sm={2}>
                <Button bsStyle="primary" className="pull-right" onClick={this.submit}><FontAwesome name="search" /> ค้นหา</Button>
              </Col>
            </Row>

            <hr />
            <Table_changeUnit3 type="Height" />
          </Panel>
        </Col>
        <Col md={3}>
          <Hisrequest tabactive={5} />
        </Col>
      </div>
    );
  }
}
function mapStatetoProps(state) {
  return {
    unittabData: state.unittabData,
    incidentData: state.incidentData,
    tableData: state.tableData,
    view: state.view,
    changeAllUnit3: state.changeAllUnit3,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    fetchData: where => dispatch({ type: "FETCH_TABLE_DATA", payload: where }),
    fetchData: sort => dispatch({ type: "FETCH_TABLE_DATA", payload: sort }),
    fetchData: groupby => dispatch({ type: "FETCH_TABLE_DATA", payload: groupby }),
    fetchData: have => dispatch({ type: "FETCH_TABLE_DATA", payload: have }),
    alert: type => dispatch({ type: "ALERTS", payload: type }),
    alert: headline => dispatch({ type: "ALERTS", payload: headline }),
    alert: message => dispatch({ type: "ALERTS", payload: message }),
    alert: timeout => dispatch({ type: "ALERTS", payload: timeout }),
    alert: position => dispatch({ type: "ALERTS", payload: position }),
    changetab: changetab => dispatch({ type: "CHANGE_VIEW_DATA", payload: changetab }),
    changetab: unit => dispatch({ type: "CHANGE_VIEW_DATA", payload: unit }),
    changetab: state => dispatch({ type: "CHANGE_VIEW_DATA", payload: state }),
  }
}

const Connectedrequest_Height = connect(mapStatetoProps, mapDispatchToProps)(Height)

export default Connectedrequest_Height
