import React, { Component } from 'react';
import {
    Popover,
    Tooltip,
    Button,
    Modal,
    OverlayTrigger,
    Grid,
    Row,
    Col,
    Panel,
    ControlLabel
} from 'react-bootstrap';
import Select from 'react-select';
import { connect } from 'react-redux'
class ModalBuilder extends Component {
    constructor(props) {
        super(props)
        this.state = {
            valueUnit3: 0,
            valueUnit1: "",
            valueUnit2: "",
            valueUnit0: "",
        }
        this.close = this.close.bind(this);
        this.logChangeUnit0 = this.logChangeUnit0.bind(this)
        this.logChangeUnit1 = this.logChangeUnit1.bind(this)
        this.logChangeUnit2 = this.logChangeUnit2.bind(this)
        this.logChangeUnit3 = this.logChangeUnit3.bind(this)
    }
    logChangeUnit0(val) {
        var tmp = []
        val.forEach((element, i) => {
            tmp[i] = element.value;
        });
        this.setState({ valueUnit0: tmp })
    }
    logChangeUnit1(val) {
        var tmp = []
        val.forEach((element, i) => {
            tmp[i] = element.value;
        });
        this.setState({ valueUnit1: tmp })
    }
    logChangeUnit2(val) {
        var tmp = []
        val.forEach((element, i) => {
            tmp[i] = element.value;
        });
        this.setState({ valueUnit2: tmp })
    }
    logChangeUnit3(val) {
        var tmp = []
        val.forEach((element, i) => {
            tmp[i] = element.value;
        });
        this.setState({ valueUnit3: tmp })
    }
    close() {
        this.props.fetchbuilder({
            action:1,
            unit0:this.state.valueUnit0.toString(),
            unit1:this.state.valueUnit1.toString(),
            unit2:this.state.valueUnit2.toString(),
            unit3:this.state.valueUnit3.toString(),
        })
        this.props.close()
    }
    render() {
        return (
            <div>
                <Modal
                    key="2"
                    show={this.props.showModalBuilder}
                    onHide={this.close}
                    style={{
                        fontSize: "13px"
                    }}
                    dialogClassName="custom-modal">

                    <Modal.Header closeButton>
                        <Modal.Title id="contained-modal-title-lg">รายละเอียด</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Row>
                            <Col sm={3}>
                                <ControlLabel>หน่วยปัจจุบัน</ControlLabel>
                                <Select
                                    placeholder="เลือกหน่วย"
                                    name="Unit3"
                                    multi
                                    value={this.state.valueUnit3}
                                    options={this.props.unittabData}
                                    onChange={this.logChangeUnit3}
                                    ref="changeUnit3" />
                            </Col>
                            <Col sm={3}>
                                <ControlLabel>หน่วยสมัครใจ1</ControlLabel>
                                <Select
                                    placeholder="เลือกหน่วย"
                                    name="Unit1"
                                    multi
                                    value={this.state.valueUnit1}
                                    options={this.props.unittabData}
                                    onChange={this.logChangeUnit1}
                                    ref="changeUnit1" />
                            </Col>
                            <Col sm={3}>
                                <ControlLabel>หน่วยสมัครใจ2</ControlLabel>
                                <Select
                                    placeholder="เลือกหน่วย"
                                    name="Unit2"
                                    multi
                                    value={this.state.valueUnit2}
                                    options={this.props.unittabData}
                                    onChange={this.logChangeUnit2}
                                    ref="changeUnit2" />
                            </Col>
                            <Col sm={3}>
                                <ControlLabel>หน่วยร้องขอ</ControlLabel>
                                <Select
                                    placeholder="เลือกหน่วย"
                                    name="Unit0"
                                    multi
                                    value={this.state.valueUnit0}
                                    options={this.props.unittabData}
                                    onChange={this.logChangeUnit0}
                                    ref="changeUnit0" />
                            </Col>
                        </Row>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={this.close}>Close</Button>
                    </Modal.Footer>
                </Modal>
            </div>
        );
    }
}
function mapStatetoProps(state) {
    return {
        requestreportData: state.requestreportData,
        unittabData: state.unittabData,
        statustabData: state.statustabData,
        positiontabData: state.positiontabData,
        searchData: state.searchData,
        modalData: state.modalData,
        view: state.view,
        tableData: state.tableData,
        changeAllUnit3: state.changeAllUnit3,
        builderData: state.builderData,
    }
}

function mapDispatchToProps(dispatch) {
    return {
        fetchbuilder: action => dispatch({ type: "CHANGE_BUILDER_DATA", payload: action }),
        fetchbuilder: unit3 => dispatch({ type: "CHANGE_BUILDER_DATA", payload: unit3 }),
        fetchbuilder: unit1 => dispatch({ type: "CHANGE_BUILDER_DATA", payload: unit1 }),
        fetchbuilder: unit2 => dispatch({ type: "CHANGE_BUILDER_DATA", payload: unit2 }),
        fetchbuilder: unit0 => dispatch({ type: "CHANGE_BUILDER_DATA", payload: unit0 }),
        modal: showModalBuilder => dispatch({ type: "CHANGE_VIEW_DATA", payload: showModalBuilder }),
        fetchUnittab: navyid => dispatch({ type: "FETCH_UNITTAB_DATA" }),

    }
}

const ConnectedModalBuilder = connect(mapStatetoProps, mapDispatchToProps)(ModalBuilder)

export default ConnectedModalBuilder
