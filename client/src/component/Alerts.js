import React, { Component } from 'react';
import { AlertList, AlertContainer } from "react-bs-notifier";
import { connect } from 'react-redux'
import { Spin , notification, Alert } from 'antd';
class Alerts extends Component {
  constructor(props) {
    super(props)
    this.state = {
      position: "top-right",
      alerts: [],
      timeout: 0
    }
    this.onAlertDismissed = this.onAlertDismissed.bind(this)
    this.clearAlerts = this.clearAlerts.bind(this)
    this.generate = this.generate.bind(this)
    this.openNotificationWithIcon = this.openNotificationWithIcon.bind(this)
  }
  onAlertDismissed(alert) {
    const alerts = this.state.alerts;
    const idx = alerts.indexOf(alert);
    if (idx >= 0) {
      this.setState({
        alerts: [
          ...alerts.slice(0, idx),
          ...alerts.slice(idx + 1)
        ]
      });
    }
  }
  clearAlerts() {
    this.setState({
      alerts: []
    });
  }
  componentWillReceiveProps(nextProps) {
    this.setState({ position: nextProps.Alerts.position })
    this.generate(nextProps.Alerts.type, nextProps.Alerts.headline, nextProps.Alerts.message, nextProps.Alerts.timeout)
  }
  generate(type, headline, message, timeout) {
    if (type == "") {
      this.setState({ timeout: timeout })
    }
    else {
      const newAlert = {
        id: (new Date()).getTime(),
        type: type,
        headline: `${headline}`,
        message: message
      };
      this.setState({ timeout: timeout })
      this.setState({
        alerts: [
          ...this.state.alerts,
          newAlert
        ]
      });
    }
  }
  openNotificationWithIcon(type) {
    notification.config({
      placement: 'bottomRight',
      bottom: 50,
      duration: 3,
    });
    notification[type]({
      message: null,
      description: <Alert message="Success Tips" type="success" showIcon />,
    });
  };
  render() {
    return (
      <div>
        <AlertList
          position={this.state.position}
          alerts={this.state.alerts}
          timeout={this.state.timeout}
          dismissTitle="Begone!"
          onDismiss={this.onAlertDismissed} />
          {/* {this.openNotificationWithIcon('success')} */}
      </div>
    );
  }
}
function mapStatetoProps(state) {
  return {
    Alerts: state.Alerts,

  }
}
function mapDispatchToProps(dispatch) {
  return {
  }
}

const ConnectedAlerts = connect(mapStatetoProps, mapDispatchToProps)(Alerts)

export default ConnectedAlerts
