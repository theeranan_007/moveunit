import React, { Component } from 'react';
import FontAwesome from 'react-fontawesome'
import { Nav, Navbar, NavItem } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap'

class HeaderNavigation extends Component {
  render() {
    return (
      <div>
        <Navbar inverse collapseOnSelect>
          <Navbar.Header>
            <Navbar.Brand>
              <a href="/" style={{ color: '#ffffff' }} ><FontAwesome name="achor" />&nbsp;แบ่งทหาร</a>
            </Navbar.Brand>
            <Navbar.Toggle />
          </Navbar.Header>
          <Navbar.Collapse>
            <Nav>
            <LinkContainer to='/selectexam'>
                <NavItem eventKey={1} >คัดเลือก</NavItem>
              </LinkContainer>
              <LinkContainer to='/request'>
                <NavItem eventKey={2} >ร้องขอ</NavItem>
              </LinkContainer>
              <LinkContainer to='/incident'>
                <NavItem eventKey={3} >มีเหตุ</NavItem>
              </LinkContainer>
              <LinkContainer to='/provide'>
                <NavItem eventKey={4} >จังหวัด</NavItem>
              </LinkContainer>
              <LinkContainer to='/education'>
                <NavItem eventKey={5} >วุฒิ/ความสามารถ</NavItem>
              </LinkContainer>
               <LinkContainer to='/height'>
                <NavItem eventKey={6} >ความสูง</NavItem>
              </LinkContainer>
              <LinkContainer to='/change'>
                <NavItem eventKey={7} >สับเปลี่ยน</NavItem>
              </LinkContainer>
              <LinkContainer to='/belong'>
                <NavItem eventKey={8} >ร้อยเตรียม</NavItem>
              </LinkContainer>
              <LinkContainer to='/group_report'>
                <NavItem eventKey={9} >สรุปการแบ่ง</NavItem>
              </LinkContainer>
            </Nav>
          </Navbar.Collapse>
        </Navbar>

      </div>
    );
  }
}
export default HeaderNavigation
