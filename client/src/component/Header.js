import React, { Component } from 'react';
import HeaderNavigation from '../component/HeaderNavigation.js';

class Header extends Component{
  render() {
    return (
      <div>
        <HeaderNavigation/>
      </div>
    );
  }
}
export default Header
