import React, { Component } from 'react'
import ReactDOM from 'react-dom';
import {
    Row,
    Panel,
    Col,
    Form,
    Button,
    FormGroup,
    FormControl,
    ControlLabel,
    Table,
    Tab,
    Tabs
} from 'react-bootstrap'
import Toggle from 'react-bootstrap-toggle';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import Select from 'react-select';

import MdSave from 'react-icons/lib/md/save';
import { connect } from 'react-redux'
import FontAwesome from 'react-fontawesome'
import MdDelete from 'react-icons/lib/md/delete'
import FaEdit from 'react-icons/lib/fa/edit';
import { AlertList, Alert, AlertContainer } from "react-bs-notifier";
import Confirm from "../component/Confirm";
var tmp = "";
var selectid = "",
    confirm = "",
    oldtabledata = [],
    deletestate = 0;
let save = false,deleted = false;
class Selectexamfrom extends Component {
    constructor(props) {
        super(props)
        this.state = {
            inputvalidate: "",
            valueUnit: 0,
            valueUnit3: 0,
            valueAskcode: "",
            valueAskname: "",
            valueNum: null,
            valueRemark: "",
            valueRemark2: "",
            valueItem: "",
            optionsAskcode: [],
            navyid: "",
            position: "top-left",
            alerts: [],
            timeout: 0,
            newMessage: "This is a test of the Emergency Broadcast System. This is only a test.",
            inputvalidate: null,
            request: [],
            unittabdata: [],
            modalData: [],
            valueUnit4: this.props.typeOpenModal.unit4,
            valuePosition: this.props.typeOpenModal.postcode
        }
        this.handleItem = this.handleItem.bind(this);
        this.keypressItem = this.keypressItem.bind(this);
        this.submitunit3 = this.submitunit3.bind(this);
        this.deleteselectexam = this.deleteselectexam.bind(this);
        this.generate = this.generate.bind(this);
        this.logChangeUnit3 = this.logChangeUnit3.bind(this);
        this.deleterequest = this.deleterequest.bind(this);
        this.submitselectexam = this.submitselectexam.bind(this);
        this.logchangePosition = this.logchangePosition.bind(this)
        this.logchangeUnit4 = this.logchangeUnit4.bind(this)
    }
    logchangePosition(val) {
        this.setState({
            valuePosition: val == null
                ? 0
                : val.value
        })
        if (this.state.valueUnit4 != 0) {
            this.props.fetchData({
                where: "selectexam.unit4 = " + this.state.valueUnit4 + " and selectexam.postcode='" + val.value + "'",
                sort: "selectexam.item desc",
                groupby: ""
            })
        }
    }
    logchangeUnit4(val) {
        this.setState({
            valueUnit4: val == null
                ? 0
                : val.value
        })
        if (this.state.valuePosition != 0) {
            this.props.fetchData({
                where: "selectexam.unit4 = " + val.value + " and selectexam.postcode='" + this.state.valuePosition + "'",
                sort: "selectexam.item desc",
                groupby: ""
            })
        }
    }
    handleItem(val) {
        this.setState({ valueItem: val.target.value });
    }
    keypressItem(val) { }
    deleteselectexam(navyid) {
        let unit4 = "",postcode = "",item = "";
        oldtabledata.forEach(function (element) {
            if (element.NAVYID == navyid) {
                unit4 = element.unit4;
                postcode = element.postcode;
                item = element.item;
            }
        });
        this.props.fetchdeleteselectexam({ navyid:navyid,unit4:unit4 ,postcode:postcode,item:item})
        deletestate = 0;
        deleted = true;
    }
    submitselectexam() {
        // if (save && this.props.modalData.UNIT4 == null) {
        //     save = false;
        //     this.props.modal({ showModalSelectexam: false });
        //     return true;
        // }
        if(this.state.valueItem==null||this.state.valueItem==0){
            this.generate("danger", "", "ลำดับผิดพลาด", 2000, '')
            return false
        }
        if (this.props.modalData.NAVYID != "" && this.props.modalData.NAVYID != null) {
            this.props.fetchinsertselectexam({ navyid: this.props.modalData.NAVYID, postcode: this.state.valuePosition, unit4: this.state.valueUnit4, item: this.state.valueItem })
            save = true;
        }
        else {
            this.generate("danger", "", "ผิดพลาด", 2000, '')
        }
        deletestate = 0;
        save = true;
    }
    componentDidMount() {
        selectid = "";
        this.setState({ unittabdata: this.props.unittabData })
        var tmp = []
        for (var i = 1; i <= 20; i++) {
            tmp[i - 1] = {
                value: i < 10
                    ? "0" + i.toString()
                    : i.toString(),
                label: i < 10
                    ? "0" + i
                    : i
            }

        }
        this.setState({ optionsAskcode: tmp })
    }
    logChangeUnit3(val) {
        this.setState({
            valueUnit3: val == null
                ? 0
                : val.value
        })
    }

    componentWillReceiveProps(nextProps) {
        if (deletestate == 0) {
            this.props.fetchData({
                where: "selectexam.unit4 = " + this.state.valueUnit4 + " and selectexam.postcode='" + this.state.valuePosition + "'",
                sort: "selectexam.item desc",
                groupby: ""
            })
            deletestate = 1;
        }
        if (nextProps.numrequestData.Num != undefined) {
            if (this.state.valueNum == null && this.state.valueAskcode != "") {
                this.setrequestnum(nextProps.numrequestData.Num)
            }
        }
        if (nextProps.checknumrequestData[0] != undefined) {
            if (this.state.valueNum != null && this.state.valueAskcode != "") {
                nextProps.checknumrequestData.map((item, i) => {
                    this.generate("danger", "ลำดับซ้ำ", "พลฯ " + item.name + " " + item.sname + " หน่วย " + item.unitname + " ผู้ขอ " + item.askby + " ลำดับ " + item.num + " หมายเหตุ " + item.remark + " ขอต่อ " + item.remark2, 2000, '')
                })
            }
        }
        if (nextProps.tableData != oldtabledata) {
            
            if(save){
                this.props.fetchmodalData({ navyid: this.state.navyid })
                this.setState({navyid:""})
                save=false;
                this.generate("success", "", "บันทึกเสร็จสิ้น", 2000, '')
            }
            if(deleted){
                this.props.fetchmodalData({ navyid: this.state.navyid })
                this.setState({navyid:""})
                deleted=false;
                this.generate("danger", "", "ลบเสร็จสิ้น", 2000, '')
            }
            oldtabledata = nextProps.tableData;
            if (nextProps.modalData.valueUnit4 == this.props.typeOpenModal.unit4 && this.state.valueUnit4 == this.props.typeOpenModal.unit4 && nextProps.modalData.postcode == this.props.typeOpenModal.postcode) {
                this.setState({ valueItem: nextProps.modalData.item })
            }
            else {
                if (nextProps.tableData.length > 0) {
                    var item = 0;
                    nextProps.tableData.forEach(function (element) {
                        if (element.NAVYID == nextProps.modalData.NAVYID) {
                            item = element.item;
                        }
                    }, this);
                    this.setState({ valueItem: item == 0 ? (nextProps.tableData[0].item + 1) : item })
                }
                else {
                    this.setState({ valueItem: 1 })
                }
            }
            

        }
        if (nextProps.modalData.NAVYID != this.state.navyid) {
            deletestate = 0;
            this.props.fetchmodalData({ navyid: nextProps.modalData.NAVYID })
            this.setState({ navyid: nextProps.modalData.NAVYID })
            this.props.fetchRequest({ navyid: nextProps.modalData.NAVYID })
            this.setState({ request: this.props.requestData })
            this.setState({ valueUnit3: nextProps.modalData.valueUnit3 })
            if (nextProps.modalData.valueUnit4 == this.props.typeOpenModal.unit4 && nextProps.modalData.postcode == this.props.typeOpenModal.postcode) {

                this.setState({ valueUnit4: nextProps.modalData.valueUnit4 })
                this.setState({ valuePosition: nextProps.modalData.postcode })
                this.setState({ valueItem: nextProps.modalData.item })
            }
            else {
                this.setState({ valueUnit4: this.props.typeOpenModal.unit4 })
                this.setState({ valuePosition: this.props.typeOpenModal.postcode })
                if (nextProps.tableData.length > 0) {
                    var item = 0;

                    nextProps.tableData.forEach(function (element) {
                        if (element.NAVYID == nextProps.modalData.NAVYID) {
                            item = element.item;
                        }
                    }, this);
                    this.setState({ valueItem: item == 0 ? (nextProps.tableData[0].item + 1) : item })
                }
                else {
                    this.setState({ valueItem: 1 })
                }
            }
        }
        if (nextProps.modalData != this.state.modalData) {
            this.setState({ modalData: nextProps.modalData })
            if (this.state.valueUnit3 != nextProps.modalData.valueUnit3)
                this.setState({ valueUnit3: nextProps.modalData.valueUnit3 })
            this.props.fetchHisrequest({});
        }
    }
    componentDidMount() {
        ReactDOM.findDOMNode(this.refsubmitselectexam).focus()
    }
    generate(type, headline, message, timeout, headtype) {
        const newAlert = {
            id: (new Date()).getTime(),
            type: type,
            headline: `${headline}`,
            message: message
        };
        this.setState({ timeout: timeout })
        this.setState({ inputvalidate: headtype })
        this.setState({
            alerts: [
                ...this.state.alerts,
                newAlert
            ]
        });
    }
    setrequestnum(Num) {
        this.setState({
            valueNum: Num + 1
        })
    }
    onAlertDismissed(alert) {
        const alerts = this.state.alerts;
        const idx = alerts.indexOf(alert);

        if (idx >= 0) {
            this.setState({
                alerts: [
                    ...alerts.slice(0, idx),
                    ...alerts.slice(idx + 1)
                ]
            });
        }
    }

    clearAlerts() {
        this.setState({ alerts: [] });
    }
    changerequest(id) {
        this.props.fetchchangeRequest({ navyid: this.state.navyid, selectid: id })
        selectid = "";
        this.setState({ navyid: "" })
    }
    editrequest(id) {
        if (selectid == id) {
            this.setState({ valueNum: null })
            this.setState({ valueUnit: 0 })
            this.setState({ valueAskcode: "" })
            this.setState({ valueAskname: "" })
            this.setState({ valueRemark: "" })
            this.setState({ valueRemark2: "" })
            selectid = "";
        } else {
            if (this.props.requestData.length != 0) {
                for (var i = 0; i < this.props.requestData.length; i++) {
                    if (id == this.props.requestData[i].selectid) {
                        selectid = id;
                        this.setState({
                            valueUnit: parseInt(this.props.requestData[i].unit),
                            valueAskcode: this.props.requestData[i].askcode,
                            valueAskname: this.props.requestData[i].askname,
                            valueNum: this.props.requestData[i].num,
                            valueRemark: this.props.requestData[i].remark,
                            valueRemark2: this.props.requestData[i].remark2
                        });
                    }
                }
            }
        }
    }
    deleterequest(id) {
        if (this.props.requestData.length != 0) {
            for (var i = 0; i < this.props.requestData.length; i++) {
                if (id == this.props.requestData[i].selectid) {
                    this.generate("danger", "ลบเสร็จสิ้น", "หน่วย: \"" + this.props.requestData[i].unit + "\" ผู้ขอ:\"" + this.props.requestData[i].askcode + " " + this.props.requestData[i].askname + "\" ลำดับ:\"" + this.props.requestData[i].num + "\" หมายเหตุ:\"" + this.props.requestData[i].remark + "\" ขอต่อ:\"" + this.props.requestData[i].remark2 + "\"", 2000, "success")
                }
            }
        }
        this.props.fetchdeleteRequest({ navyid: this.state.navyid, selectid: id })
        this.setState({ navyid: "" })
    }
    submitunit3() {
        // if (this.props.requestData.length != 0) {
        //     this.generate("danger", "แจ้งเตือน", "ไม่สามารถแก้ไขหน่วยได้ เนื่องจากมีผู้ขอ โปรดแก้ไขข้อมูลในแท็บร้องขอ แทนการเปลี่ย" +
        //         "นหน่วยโดยตรง",
        //         0, "success")
        //     return false
        // } else {
        this.props.fetchchangeunit3({ navyid: this.state.navyid, unit3: this.state.valueUnit3 })
        var navyid = this.state.navyid
        this.setState({ navyid: "" })
        this.props.fetchmodalData({ navyid: navyid })
        this.generate("success", "เสร็จสิ้น", "เปลี่ยนหน่วยเสร็จสิ้น", 2000, "success")
        // }

    }
    render() {
        return (
            <div>

                <AlertList
                    position={this.state.position}
                    alerts={this.state.alerts}
                    timeout={this.state.timeout}
                    dismissTitle="Begone!"
                    onDismiss={this
                        .onAlertDismissed
                        .bind(this)} />
                {/* {this.props.type=="modal"? */}
                <Panel bsStyle="primary" >
                    <Row>
                        <Col xs={3}>
                            <div>
                                <b>ตำแหน่ง</b>
                            </div>
                        </Col>
                        <Col xs={9}>
                            <Select
                                placeholder="ตำแหน่ง"
                                name="position"
                                value={this.state.valuePosition}
                                onChange={this.logchangePosition}
                                clearable={false}
                                options={this.props.positiontabData} />
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={3}>
                            <div>
                                <b>หน่วย</b>
                            </div>
                        </Col>
                        <Col xs={9}>
                            <Select
                                placeholder="หน่วย"
                                name="unittab"
                                value={this.state.valueUnit4}
                                onChange={this.logchangeUnit4}
                                clearable={false}
                                options={this.props.unittabData} />
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={3}>
                            <div>
                                <b>ลำดับ</b>
                            </div>
                        </Col>
                        <Col xs={9}>
                            <FormControl
                                placeholder="ลำดับ"
                                type="number"
                                onChange={this.handleItem}
                                onKeyPress={this.keypressItem}
                                name="item"
                                value={this.state.valueItem}
                                ref={(selectexamItem) => {
                                    this.selectexamItem = selectexamItem;
                                }} />
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={3}></Col>
                        <Col xs={3}></Col>
                        <Col xs={3}></Col>
                        <Col xs={3}>
                            {this.props.modalData.UNIT4 != null?
                                <Confirm
                                    onConfirm={this.submitselectexam}
                                    body={"มีการคัดเลือก " + this.props.modalData.POSTNAME + " " + this.props.modalData.UNIT4 + " (" + this.props.modalData.item + ")"}
                                    confirmText="บันทึก"
                                    cancelText="ยกเลิก"
                                    title="แจ้งเตือน">
                                    <Button bsStyle="primary" bsSize="sm" className=" pull-right"
                                        ref={(refsubmitselectexam) => {
                                            this.refsubmitselectexam = refsubmitselectexam;
                                        }}
                                    >
                                        <FontAwesome name="plus" />
                                        บันทึก</ Button>
                                </Confirm> :
                                <Button
                                    bsStyle="primary"
                                    className=" pull-right"
                                    onClick={this.submitselectexam}
                                    ref={(refsubmitselectexam) => {
                                        this.refsubmitselectexam = refsubmitselectexam;
                                    }}>
                                    <FontAwesome name="plus" />&nbsp;บันทึก</Button>}
                        </Col>
                    </Row>
                </ Panel>
                < Panel bsStyle="primary">
                    <Tabs defaultActiveKey={1} id="uncontrolled-tab-example">
                        <Tab eventKey={1} title="คัดเลือกหน่วย">
                            <Row>
                                <Col xs={12} className="scroll-table">
                                    <Table striped bordered condensed>
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>ชื่อ - สกุล</th>
                                                <th>ตำแหน่ง</th>
                                                <th>หน่วย</th>
                                                <th>ลำดับ</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {this
                                                .props
                                                .tableData
                                                .map((item, i) => {
                                                    return (
                                                        <tr key={item.NAVYID}>
                                                            <td>{i + 1}</td>
                                                            <td>{item.fullname}</td>
                                                            <td>{item.postname}</td>
                                                            <td>{item.refnum4}</td>
                                                            <td>{item.item}</td>
                                                            <td>
                                                                <Confirm
                                                                    onConfirm={() => this.deleteselectexam(item.NAVYID)}
                                                                    body="ต้องการลบข้อมูลจริงหรือไม่"
                                                                    confirmText="ลบ"
                                                                    cancelText="ยกเลิก"
                                                                    title="ยืนยันการลบ">
                                                                    <Button bsStyle="danger" bsSize="sm">
                                                                        <FontAwesome name="trash" />
                                                                        ลบ</ Button>
                                                                </Confirm>
                                                            </td>
                                                        </tr>
                                                    )
                                                })}
                                        </tbody>
                                    </Table>
                                </Col>
                            </Row>
                        </Tab>
                        <Tab eventKey={2} title="หน่วยปัจจุบัน">
                            <br />
                            <Form onSubmit={this.handlechangeUnit3}>
                                <Row>
                                    <Col xs={3}>
                                        <div>
                                            <b>หน่วยร้องขอ</b>
                                        </div>
                                    </Col>
                                    <Col xs={6}>
                                        <Select
                                            placeholder="เลือกหน่วย"
                                            name="Unit3"
                                            clearable={false}
                                            value={this.state.valueUnit3}
                                            options={this.props.unittabData}
                                            onChange={this.logChangeUnit3}
                                            ref="Unit3" />
                                    </Col>
                                    <Confirm
                                        onConfirm={this.submitunit3}
                                        body="มีการร้องขอลงหน่วย"
                                        confirmText="บันทึก"
                                        cancelText="ยกเลิก"
                                        confirmBSStyle="primary"
                                        title="แจ้งเตือน">
                                        <Button bsStyle="primary" bsSize="sm">
                                            <FontAwesome name="save" />
                                            &nbsp;บันทึก</ Button>
                                    </Confirm>
                                </Row>
                            </Form>
                        </Tab>
                    </Tabs>
                </ Panel>
            </div>
        )
    }
}

function mapStatetoProps(state) {
    // console.log("เช็ค mapstatetoprops" + JSON.stringify(requestData))
    return {
        insertselectexamData: state.insertselectexamData,
        editrequestData: state.editrequestData,
        deleterequestData: state.deleterequestData,
        changerequestData: state.changerequestData,
        requestData: state.requestData,
        modalData: state.modalData,
        unittabData: state.unittabData,
        changeUnit3: state.changeUnit3,
        numrequestData: state.numrequestData,
        checknumrequestData: state.checknumrequestData,
        tableData: state.tableData,
        deleteselectexamData: state.deleteselectexamData,
        positiontabData: state.positiontabData,
        unittabData: state.unittabData,
        hisrequestData: state.hisrequestData,
        typeOpenModal: state.typeOpenModal
    }
}

function mapDispatchToProps(dispatch) {
    return {
        fetchHisrequest: navyid => dispatch({ type: "FETCH_HISREQUEST_DATA" }),

        fetchPositiontab: navyid => dispatch({ type: "FETCH_POSITIONTAB_DATA", payload: navyid }),
        fetchUnittab: navyid => dispatch({ type: "FETCH_UNITTAB_DATA", payload: navyid }),

        fetchRequest: navyid => dispatch({ type: "FETCH_REQUEST_DATA", payload: navyid }),

        fetchData: where => dispatch({ type: "FETCH_TABLE_DATA", payload: where }),
        fetchData: sort => dispatch({ type: "FETCH_TABLE_DATA", payload: sort }),
        fetchData: groupby => dispatch({ type: "FETCH_TABLE_DATA", payload: groupby }),

        fetchinsertselectexam: navyid => dispatch({ type: "FETCH_INSERTSELECTEXAM_DATA", payload: navyid }),
        fetchinsertselectexam: unit4 => dispatch({ type: "FETCH_INSERTSELECTEXAM_DATA", payload: unit4 }),
        fetchinsertselectexam: postcode => dispatch({ type: "FETCH_INSERTSELECTEXAM_DATA", payload: postcode }),
        fetchdeleteselectexam: navyid => dispatch({ type: "FETCH_DELETESELECTEXAM_DATA", payload: navyid }),
        fetchdeleteselectexam: unit4 => dispatch({ type: "FETCH_DELETESELECTEXAM_DATA", payload: unit4 }),
        fetchdeleteselectexam: postcode => dispatch({ type: "FETCH_DELETESELECTEXAM_DATA", payload: postcode }),
        fetchdeleteselectexam: item => dispatch({ type: "FETCH_DELETESELECTEXAM_DATA", payload: item }),
        fetchchangeunit3: navyid => dispatch({ type: "FETCH_CHANGEUNIT3_DATA", payload: navyid }),
        fetchchangeunit3: unit3 => dispatch({ type: "FETCH_CHANGEUNIT3_DATA", payload: unit3 }),
        fetchmodalData: navyid => dispatch({ type: "FETCH_MODAL_DATA", payload: navyid }),
        fetchUnittab: navyid => dispatch({ type: "FETCH_UNITTAB_DATA" }),
        modal: showModalSelectexam => dispatch({ type: "CHANGE_VIEW_DATA", payload: showModalSelectexam }),
    }
}

const ConnectedSelectexam = connect(mapStatetoProps, mapDispatchToProps)(Selectexamfrom)
export default ConnectedSelectexam
