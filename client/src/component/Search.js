import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import {
  Panel,
  Col,
  Form,
  Button,
  FormGroup,
  FormControl,
  ControlLabel,
  Row
} from 'react-bootstrap'
import FaSearch from 'react-icons/lib/fa/search'
import ModalDetail from '../component/ModalDetail'
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import { AlertList, Alert, AlertContainer } from "react-bs-notifier";
import { connect } from 'react-redux'
import { request } from 'http';
var navyid = "",statetogglemodal=0
let searching = false;
let countsearching = 0;
class Search extends Component {
  constructor(props) {
    super(props)
    this.state = {
      name: "",
      sname: "",
      id8: "",
      position: "top-right",
      alerts: [],
      timeout: 0,
      newMessage: "This is a test of the Emergency Broadcast System. This is only a test.",
      inputvalidate: null,
      tmp: 0
    }
    this.keypressName = this.keypressName.bind(this)
    this.keypressSname = this.keypressSname.bind(this)
    this.keypressId8 = this.keypressId8.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleName = this.handleName.bind(this);
    this.handleSname = this.handleSname.bind(this);
    this.handleId8 = this.handleId8.bind(this);
    this.buttonFormatter = this.buttonFormatter.bind(this);
    this.modal = this.modal.bind(this)
    this.close = this.close.bind(this)
    this.focusname = this.focusname.bind(this)
  }

  handleSubmit(event) {
    event.preventDefault();
    if (this.props.type == "selectexam") {
      if (this.props.position == "") {
        this.props.alert({ type: "danger", headline: "ผิดพลาด", message: "ระบุตำแหน่งคัดเลือก", timeout: 2000, position: "top-right" })
        return false;
      }
      if (this.props.unit4 == "") {
        this.props.alert({ type: "danger", headline: "ผิดพลาด", message: "ระบุหน่วยคัดเลือก", timeout: 2000, position: "top-right" })
        return false;
      }
    }
    if (navyid == "") {
      this.props.alert({ type: "success", headline: "กำลังโหลด...", message: < div className="loader" > </div>, timeout: 0, position: "top-right" })
      if (this.state.name != "" || this.state.sname != "" || this.state.id8 != "") {
        this.props.fetchData({ name: this.state.name, sname: this.state.sname, id8: this.state.id8 })
        statetogglemodal = 1
          this.props.alert({type:"",timeout:1})
      } else {
        
        this.props.alert({ type: "danger", headline: "ผิดพลาด", message: "ไม่ค้นหาช่องว่าง", timeout: 2000, position: "top-right" })
        this.focusname();
      }
    } else if (navyid != "") {
      if (this.props.type == "selectexam") {
        this.props.typeModal({type:"selectexam",unit4:this.props.unit4,postcode:this.props.position})
        this.props.modalSelectexam({ showModalSelectexam: true ,selectexamtab:this.props.view.selectexamtab,requesttab:1})
      }
      else {
        this.props.modal({ showModal: true ,requesttab:this.props.view.requesttab,selectexamtab:1})
      }
      this.props.fetchModalData({ navyid: navyid })
      statetogglemodal = 1
      ReactDOM.findDOMNode(this.refname).focus();
      
    }
    searching = true;
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.searchData.length > 0 && this.props.view.showModal == false) {
      if (navyid != nextProps.searchData[0].NAVYID) {
        navyid = nextProps.searchData[0].NAVYID
      }
      this.props.alert({type:"",timeout:1})
    }
    if(nextProps.searchData.length==0 && searching == true){
      searching = false;
      this.props.alert({ type: "danger", headline: "", message: "ไม่พบการค้นหา", timeout: 2000, position: "top-right" })
      countsearching++;
      
    }
    if(statetogglemodal == 1 && (nextProps.view.showModal ==true ||nextProps.view.showModalSelectexam == true)){
      statetogglemodal = 2
    }
    if(statetogglemodal == 2&& (nextProps.view.showModal ==false &&nextProps.view.showModalSelectexam == false)){
      statetogglemodal = 0
      countsearching=2;
      // ReactDOM.findDOMNode(this.refname).focus()
    }
  }
  componentDidUpdate(){
    if(countsearching>=2){
      this.setState({name:"",sname:"",id8:""})
      this.focusname();
      countsearching = 0;
    }
  }
  focusname(){
    // this.refname.focus(); 
    ReactDOM.findDOMNode(this.refname).focus()
  }
  componentDidMount() {
    if (this.props.type == "selectexam") {
      this.props.typeModal({type:"selectexam",unit4:this.props.unit4,postcode:this.props.position})
      this.props.modalSelectexam({ showModalSelectexam: false })
    }
    else {
      this.props.typeModal({type:"request",unit4:null,postcode:null})
      this.props.modal({ showModal: false })
    }
    // console.log(ReactDOM.findDOMNode(this.refname));
    this.focusname();
  }
  keypressName(event) {
    if (event.key == "Enter") {
      event.preventDefault();
      ReactDOM.findDOMNode(this.refsname).focus()
    }
  }
  keypressSname(event) {
    if (event.key == "Enter") {
      event.preventDefault();
      ReactDOM
        .findDOMNode(this.refid8)
        .focus()
    }
  }
  keypressId8(event) {
    if (event.key == "Enter") {
      event.preventDefault();
      ReactDOM
        .findDOMNode(this.refsubmit)
        .focus()
    }
  }
  handleName(event) {

    this.setState({ name: event.target.value })
    navyid = ""
  }
  handleSname(event) {
    this.setState({ sname: event.target.value })
    navyid = ""
  }
  handleId8(event) {
    this.setState({ id8: event.target.value })
    navyid = ""
  }
  modal(navyid) {
    if (this.props.type == "selectexam") {
      if (this.props.position == "") {
        this.props.alert({ type: "danger", headline: "ผิดพลาด", message: "ระบุตำแหน่งคัดเลือก", timeout: 2000, position: "top-right" })
        return false;
      }
      if (this.props.unit4 == "") {
        this.props.alert({ type: "danger", headline: "ผิดพลาด", message: "ระบุหน่วยคัดเลือก", timeout: 2000, position: "top-right" })
        return false;
      }
    }
    if (this.props.type == "selectexam") {
      this.props.typeModal({type:"selectexam",unit4:this.props.unit4,postcode:this.props.position})
      this.props.modalSelectexam({ showModalSelectexam: true ,selectexamtab:this.props.view.selectexamtab,requesttab:1})
    }
    else {
      this.props.modal({ showModal: true ,requesttab:this.props.view.requesttab,selectexamtab:1})
    }
    statetogglemodal = 1
    this.props.fetchModalData({ navyid: navyid })
  }
  buttonFormatter(cell, row) {
    return <a href="#" onClick={() => this.modal(cell)}><FaSearch /></a>
  }
  close() {
    if (this.props.type == "selectexam") {
      this.props.modalSelectexam({ showModalSelectexam: false })
    }
    else {
      this
        .props
        .modal({ showModal: false })
    }
    navyid = ""
  }

  render() {

    return (
      <div>
        
        < Form onSubmit={this.handleSubmit}>
          <Row>
            <Col sm={4}>
              <ControlLabel>ชื่อ</ControlLabel>
            </Col>
            <Col sm={3}>
              <ControlLabel>นามสกุล</ControlLabel>
            </Col>
            <Col sm={3}>
              <ControlLabel>ทะเบียน</ControlLabel>
            </Col>
          </Row>
          <Row>
            <Col sm={4}>
              <FormGroup controlId="Search_name" validationState={this.state.inputvalidate}>
                <FormControl
                  type="text"
                  placeholder="ชื่อ"
                  onKeyPress={this.keypressName}
                  onChange={this.handleName}
                  name="Search_name"
                  value={this.state.name}
                  ref={(input) => {
                    this.refname = input;
                  }} />
              </FormGroup>
            </Col>
            <Col sm={3}>
              <FormGroup controlId="Search_sname" validationState={this.state.inputvalidate}>

                <FormControl
                  type="text"
                  placeholder="นามสกุล"
                  onKeyPress={this.keypressSname}
                  onChange={this.handleSname}
                  value={this.state.sname}
                  name="Search_sname"
                  ref={(refsname) => {
                    this.refsname = refsname;
                  }} />
              </FormGroup>
            </Col>
            <Col sm={3}>
              <FormGroup controlId="Search_id8" validationState={this.state.inputvalidate}>

                <FormControl
                  type="text"
                  placeholder="ทะเบียน"
                  onKeyPress={this.keypressId8}
                  onChange={this.handleId8}
                  value={this.state.id8}
                  name="Search_id8"
                  ref={(refid8) => {
                    this.refid8 = refid8;
                  }} />
              </FormGroup>
            </Col>
            <Col sm={1}>
              <Button
                type="submit"
                bsStyle="primary"
                ref={(refsubmit) => {
                  this.refsubmit = refsubmit;
                }}>
                <FaSearch />&nbsp;ค้นหา
                </Button>
            </Col>
          </Row>
        </Form>
        <hr />
        <BootstrapTable
          responsive
          data={this.props.searchData}
          search={false}
          pagination>
          <TableHeaderColumn dataField='NAME' dataSort={true}>ชื่อ</TableHeaderColumn>
          <TableHeaderColumn dataField='SNAME' dataSort={true}>นามสกุล</TableHeaderColumn>
          <TableHeaderColumn dataField='ID8' dataSort={true}>ทะเบียน</TableHeaderColumn>
          <TableHeaderColumn dataField='YEARIN' dataSort={true}>ผลัด</TableHeaderColumn>
          <TableHeaderColumn
            dataField="NAVYID"
            isKey={true}
            dataFormat={this.buttonFormatter}></TableHeaderColumn>
        </BootstrapTable>

      </div>
    )
  }

}
function mapStatetoProps(state) {
  return { searchData: state.searchData, typeOpenModal:state.typeOpenModal,modalData: state.modalData, view: state.view, requestData: state.requestData }
}

function mapDispatchToProps(dispatch) {
  return {
    fetchData: name => dispatch({ type: 'FETCH_SEARCH_DATA', payload: name }),
    fetchData: sname => dispatch({ type: 'FETCH_SEARCH_DATA', payload: sname }),
    fetchData: id8 => dispatch({ type: 'FETCH_SEARCH_DATA', payload: id8 }),
    fetchModalData: navyid => dispatch({ type: "FETCH_MODAL_DATA", payload: navyid }),
    modal: showModal => dispatch({ type: "CHANGE_VIEW_DATA", payload: showModal }),
    modal: requesttab => dispatch({ type: "CHANGE_VIEW_DATA", payload: requesttab }),
    modal: selectexamtab => dispatch({ type: "CHANGE_VIEW_DATA", payload: selectexamtab }),
    modalSelectexam: showModalSelectexam => dispatch({ type: "CHANGE_VIEW_DATA", payload: showModalSelectexam }),
    fetchRequest: navyid => dispatch({ type: "FETCH_REQUEST_DATA", payload: navyid }),
    alert: type => dispatch({ type: "ALERTS", payload: type }),
    alert: headline => dispatch({ type: "ALERTS", payload: headline }),
    alert: message => dispatch({ type: "ALERTS", payload: message }),
    alert: timeout => dispatch({ type: "ALERTS", payload: timeout }),
    alert: position => dispatch({ type: "ALERTS", payload: position }),
    typeModal: type => dispatch({type:"CHANGE_TYPEMODAL_DATA",payload:type}),
    typeModal: unit4 => dispatch({type:"CHANGE_TYPEMODAL_DATA",payload:unit4}),
    typeModal: postcode => dispatch({type:"CHANGE_TYPEMODAL_DATA",payload:postcode})
  }
}

const ConnectedSearch = connect(mapStatetoProps, mapDispatchToProps)(Search)

export default ConnectedSearch
