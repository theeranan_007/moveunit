import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import {
    Popover,
    Tooltip,
    Button,
    Modal,
    OverlayTrigger,
    Grid,
    Row,
    Col,
    Panel
} from 'react-bootstrap'
import { connect } from 'react-redux'
import Requestfrom from '../component/Requestfrom'
import Selectexamfrom from '../component/Selectexamfrom'
class ModalDetail extends Component {
    constructor(props) {

        super(props)
        this.state = {
            showModal: "",
            yearin: "",
            belong: "",
            selectexam: "",
            navyid: "",
            is_request: "",
            gridlabel: 4,
            gridcontent: 8,
            content:<Requestfrom/>
        }
        this.close = this
            .close
            .bind(this);

    }
    close (){
        this.props.modal({showModal:false})
    }
    componentDidMount(){
       
    }
    componentWillReceiveProps(nextProps) {
        if(nextProps.typeOpenModal.hasOwnProperty('type')){
            if(nextProps.typeOpenModal.type == "selectexam"){
                this.setState({content:<Selectexamfrom/>})
            }
            else{
                this.setState({content:<Requestfrom/>})
            }
        }
        if (nextProps.modalData != {}) {
            if (nextProps.modalData.UNIT4 != null) {
                this.setState({
                    selectexam: nextProps.modalData.POSTNAME + " " + nextProps.modalData.UNIT4 + " (" + nextProps.modalData.ITEM + ")"
                })
            } else {
                this.setState({ selectexam: "-" })
            }
            if (parseInt(nextProps.modalData.PSEQ) < 10) {
                this.setState({
                    belong: nextProps.modalData.COMPANY + "/" + nextProps.modalData.BATT + "(" + nextProps.modalData.PLATOON + "0" + nextProps.modalData.PSEQ + ")"
                })
            } else {
                this.setState({
                    belong: nextProps.modalData.COMPANY + "/" + nextProps.modalData.BATT + "(" + nextProps.modalData.PLATOON + nextProps.modalData.PSEQ + ")"
                })
            }
            if (nextProps.modalData.OLDYEARN == "") {
                this.setState({
                    yearin: nextProps.modalData.YEARIN + "(" + nextProps.modalData.OLDYEARN + ")"
                })
            } else {
                this.setState({ yearin: nextProps.modalData.YEARIN })
            }
            this.setState({ navyid: nextProps.modalData.NAVYID })
            if (parseInt(nextProps.modalData.IS_REQUEST) > 0) {
                this.setState({ is_request: "ร้องขอ" })
            } else {
                this.setState({ is_request: "" })
            }
        }
        else {
            //console.log(nextProps)
        }
    }
    render() {

        return (
            <div>
                <Modal
                    key="1"
                    show={this.props.typeOpenModal.type == "selectexam"?this.props.view.showModalSelectexam:this.props.view.showModal}
                    onHide={this.close}
                    style={{
                        fontSize: "13px"
                    }}

                    dialogClassName="custom-modal">
                    {/* bsSize="large"> */}

                    <Modal.Header closeButton>
                        <Modal.Title id="contained-modal-title-lg">รายละเอียด</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Row>
                            <Col xs={4}>
                                < Panel bsStyle="primary">
                                    <Row>
                                        <Col xs={this.state.gridlabel}>
                                            <div>
                                                <b>ชื่อ:
                                                </b>
                                            </div>
                                        </Col>
                                        <Col xs={8}>
                                            <div>{this.props.modalData.NAME}&nbsp;&nbsp;{this.props.modalData.SNAME}</div>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col xs={this.state.gridlabel}>
                                            <div>
                                                <b>ผลัด:
                                                </b>
                                            </div>
                                        </Col>
                                        <Col xs={this.state.gridlabel}>
                                            <div>{this.state.yearin}</div>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col xs={this.state.gridlabel}>
                                            <div>
                                                <b>ทะเบียน:
                                                </b>
                                            </div>
                                        </Col>
                                        <Col xs={this.state.gridcontent}>
                                            <div>{this.props.modalData.ID8}</div>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col xs={this.state.gridlabel}>
                                            <div>
                                                <b>สังกัด:
                                                </b>
                                            </div>
                                        </Col>
                                        <Col xs={this.state.gridlabel}>
                                            <div>{this.state.belong}</div>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col xs={this.state.gridlabel}>
                                            <div>
                                                <b>สถานะภาพ:
                                                </b>
                                            </div>
                                        </Col>
                                        <Col xs={this.state.gridcontent}>
                                            <div>{this.props.modalData.STATUS}</div>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col xs={this.state.gridlabel}>
                                            <div>
                                                <b>ผลตรวจปัสสาวะ:
                                                </b>
                                            </div>
                                        </Col>
                                        <Col
                                            xs={this.state.gridcontent}
                                            className={this.props.modalData.addictive <= 2 || this.props.modalData.addictive == null 
                                                ? ""
                                                : "tdanger"}>
                                            <div>{"กลุ่ม "+this.props.modalData.addictive+"."+this.props.modalData.addname}</div>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col xs={this.state.gridlabel}>
                                            <div>
                                                <b>ยาเสพติด:
                                                </b>
                                            </div>
                                        </Col>
                                        <Col xs={this.state.gridlabel}>
                                            <div>{this.props.modalData.PATIENT == null
                                                ? "-"
                                                : this.props.modalData.PATIENT}</div>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col xs={this.state.gridlabel}>
                                            <div>
                                                <b>ร้องขอ:
                                                </b>
                                            </div>
                                        </Col>
                                        <Col
                                            xs={this.state.gridcontent}
                                            className={this.state.is_request != ""
                                                ? "tsuccess"
                                                : ""}>
                                            <div>{this.state.is_request}</div>
                                        </Col>
                                    </Row>
                                    {/* </ Panel>
                                < Panel bsStyle="primary"> */}
                                    <hr />
                                    <Row>
                                        <Col xs={this.state.gridlabel}>
                                            <div>
                                                <b>วุฒิการศึกษา:
                                                </b>
                                            </div>
                                        </Col>
                                        <Col xs={this.state.gridcontent}>
                                            <div>{this.props.modalData.EDUCNAME}</div>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col xs={this.state.gridlabel}>
                                            <div>
                                                <b>ความรู้พิเศษ:
                                                </b>
                                            </div>
                                        </Col>
                                        <Col xs={this.state.gridlabel}>
                                            <div>{this.props.modalData.SKILL}</div>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col xs={this.state.gridlabel}>
                                            <div>
                                                <b>คะแนน:
                                                </b>
                                            </div>
                                        </Col>
                                        <Col xs={this.state.gridcontent}>
                                            <div>{this.props.modalData.PERCENT}</div>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col xs={this.state.gridlabel}>
                                            <div>
                                                <b>คัดเลือก:
                                                </b>
                                            </div>
                                        </Col>
                                        <Col xs={this.state.gridlabel}>
                                            <div>{this.state.selectexam}</div>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col xs={this.state.gridlabel}>
                                            <div>
                                                <b>สมัครใจ1:
                                                </b>
                                            </div>
                                        </Col>
                                        <Col xs={this.state.gridcontent}>
                                            <div>{this.props.modalData.UNIT1}</div>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col xs={this.state.gridlabel}>
                                            <div>
                                                <b>สมัครใจ2:
                                                </b>
                                            </div>
                                        </Col>
                                        <Col xs={this.state.gridcontent}>
                                            <div>{this.props.modalData.UNIT2}</div>
                                        </Col>
                                    </Row>
                                </Panel>
                                < Panel bsStyle="primary">
                                    <Row>
                                        <Col xs={parseInt(this.state.gridlabel) + 3}>
                                            <div>
                                                <h4><b>หน่วยปัจจุบัน:</b></h4>
                                            </div>
                                        </Col>
                                        <Col xs={parseInt(this.state.gridcontent) - 3}>
                                            <div><h4>{this.props.modalData.UNIT3}</h4></div>
                                        </Col>
                                    </Row>
                                </ Panel>
                            </Col>
                            <Col xs={8}>
                                {this.state.content}
                            </Col>
                        </Row>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={this.close}>Close</Button>
                    </Modal.Footer>
                </Modal>
            </div>
        );
    }
}
function mapStatetoProps(state) {
    // console.log("เช็ค mapstatetoprops" + JSON.stringify(requestData))
    return {
        insertrequestData: state.insertrequestData,
        editrequestData: state.editrequestData,
        deleterequestData: state.deleterequestData,
        changerequestData: state.changerequestData,
        requestData: state.requestData,
        modalData: state.modalData,
        unittabData: state.unittabData,
        changeUnit3: state.changeUnit3,
        numrequestData: state.numrequestData,
        checknumrequestData: state.checknumrequestData,
        view: state.view,
        typeOpenModal:state.typeOpenModal
    }
}

function mapDispatchToProps(dispatch) {
    return {
        fetchHisrequest_all:navyid =>  dispatch({ type: "FETCH_HISREQUESTALL_DATA" }),
        fetchHisrequest_request:navyid =>  dispatch({ type: "FETCH_HISREQUESTREQUEST_DATA" }),

        fetchRequest: navyid => dispatch({ type: "FETCH_REQUEST_DATA", payload: navyid }),
        fetchinsertRequest: navyid => dispatch({ type: "FETCH_INSERTREQUEST_DATA", payload: navyid }),
        fetchinsertRequest: unit => dispatch({ type: "FETCH_INSERTREQUEST_DATA", payload: unit }),
        fetchinsertRequest: askcode => dispatch({ type: "FETCH_INSERTREQUEST_DATA", payload: askcode }),
        fetchinsertRequest: askname => dispatch({ type: "FETCH_INSERTREQUEST_DATA", payload: askname }),
        fetchinsertRequest: num => dispatch({ type: "FETCH_INSERTREQUEST_DATA", payload: num }),
        fetchinsertRequest: remark => dispatch({ type: "FETCH_INSERTREQUEST_DATA", payload: remark }),

        fetcheditRequest: remark2 => dispatch({ type: "FETCH_EDITREQUEST_DATA", payload: remark2 }),
        fetcheditRequest: navyid => dispatch({ type: "FETCH_EDITREQUEST_DATA", payload: navyid }),
        fetcheditRequest: unit => dispatch({ type: "FETCH_EDITREQUEST_DATA", payload: unit }),
        fetcheditRequest: askcode => dispatch({ type: "FETCH_EDITREQUEST_DATA", payload: askcode }),
        fetcheditRequest: askname => dispatch({ type: "FETCH_EDITREQUEST_DATA", payload: askname }),
        fetcheditRequest: num => dispatch({ type: "FETCH_EDITREQUEST_DATA", payload: num }),
        fetcheditRequest: remark => dispatch({ type: "FETCH_EDITREQUEST_DATA", payload: remark }),
        fetcheditRequest: remark2 => dispatch({ type: "FETCH_EDITREQUEST_DATA", payload: remark2 }),
        fetcheditRequest: navyid => dispatch({ type: "FETCH_EDITREQUEST_DATA", payload: navyid }),

        fetchdeleteRequest: navyid => dispatch({ type: "FETCH_DELETEREQUEST_DATA", payload: navyid }),
        fetchdeleteRequest: selectid => dispatch({ type: "FETCH_DELETEREQUEST_DATA", payload: selectid }),
        fetchchangeRequest: navyid => dispatch({ type: "FETCH_CHANGEREQUEST_DATA", payload: navyid }),
        fetchchangeRequest: selectid => dispatch({ type: "FETCH_CHANGEREQUEST_DATA", payload: selectid }),
        fetchchangeunit3: navyid => dispatch({ type: "FETCH_CHANGEUNIT3_DATA", payload: navyid }),
        fetchchangeunit3: unit3 => dispatch({ type: "FETCH_CHANGEUNIT3_DATA", payload: unit3 }),
        fetchmodalData: navyid => dispatch({ type: "FETCH_MODAL_DATA", payload: navyid }),
        fetchUnittab: navyid => dispatch({ type: "FETCH_UNITTAB_DATA" }),
        fetchNumRequet: Unit => dispatch({ type: "FETCH_NUMREQUEST_DATA", payload: Unit }),
        fetchNumRequet: Askcode => dispatch({ type: "FETCH_NUMREQUEST_DATA", payload: Askcode }),
        fetchCheckNum: Unit => dispatch({ type: "FETCH_CHECKNUMREQUEST_DATA", payload: Unit }),
        fetchCheckNum: Askcode => dispatch({ type: "FETCH_CHECKNUMREQUEST_DATA", payload: Askcode }),
        fetchCheckNum: Num => dispatch({ type: "FETCH_CHECKNUMREQUEST_DATA", payload: Num }),

        modal: showModal => dispatch({ type: "CHANGE_VIEW_DATA", payload: showModal }),

    }
}

const ConnectedModalDetail = connect(mapStatetoProps, mapDispatchToProps)(ModalDetail)
export default ConnectedModalDetail