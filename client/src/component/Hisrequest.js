import React, { Component } from 'react';
import {
    Panel,
    Tab,
    Tabs,
    Grid,
    Row,
    Col,
    Button
} from 'react-bootstrap'
import { connect } from 'react-redux'
import { select } from 'redux-saga/effects';
import FontAwesome from 'react-fontawesome';
var totalHisrequest = 0
var request = [], incident = [], selectexam = [], province = [], all = [], belong = []
class Hisrequest extends Component {
    constructor(props) {
        super(props)
        this.state = {
            totalHisrequest: "",
            totalRequest: "",
            totalBalance: "",
            tabactive: 1,
            request: [],
            incident: [],
            selectexam: [],
            province: [],
            all: [],
            belong: []

        }
        this.request = this.request.bind(this)
        this.selectexam = this.selectexam.bind(this)
        this.provide = this.provide.bind(this)
        this.change = this.change.bind(this)
        this.incident = this.incident.bind(this)
        this.belong = this.belong.bind(this)
        this.refresh = this.refresh.bind(this)
    }
    componentDidMount() {
        this.refresh();
    }
    refresh(){
        switch (this.props.tabactive) {
            case 1:
                this.props.fetchHisrequest_request({})
                break;
            case 2:
                this.props.fetchHisrequest_incident({})
                break;
            case 3:
                this.props.fetchHisrequest_selectexam({})
                break;
            case 4:
                this.props.fetchHisrequest_province({ armid: "25,32,45" })
                break;
            case 5:
                this.props.fetchHisrequest_all({})
                break;
            case 7:
                this.props.fetchHisrequest_belong({})
                break;
            default:
                this.props.fetchHisrequest_request({})
                this.props.fetchHisrequest_incident({})
                this.props.fetchHisrequest_selectexam({})
                this.props.fetchHisrequest_province({ armid: "25,32,45" })
                this.props.fetchHisrequest_all({})
                break;
        }
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.hisrequestbelongData.hasOwnProperty('response')) {
            if (nextProps.hisrequestbelongData.response != belong) {
                belong = nextProps.hisrequestbelongData.response
                var count1 = 0, count = 0, total = 0
                var tmp = []
                belong.forEach((element, i) => {
                    tmp[i] = { ...element, unitname: this.props.unittabData[i].label }
                    count1 += parseInt(element.count1)
                    count += parseInt(element.count)
                    total += parseInt(element.total)
                });
                tmp[tmp.length] = { refnum: 31, unitname: "รวม", count1: count1, count: count, total: total, groupCount: "belong" }
                this.setState({ belong: tmp })
            }
        }
        if (nextProps.hisrequestrequestData != request && nextProps.hisrequestrequestData.length > 0) {
            request = nextProps.hisrequestrequestData
            var count2 = 0, count = 0, total = 0
            var tmp = []
            request.forEach((element, i) => {
                tmp[i] = { ...element, unitname: this.props.unittabData[i].label }
                count2 += parseInt(element.count2)
                count += parseInt(element.count)
                total += parseInt(element.total)
            });
            tmp[tmp.length] = { refnum: 31, unitname: "รวม", count2: count2, count: count, total: total, groupCount: "request" }
            this.setState({ request: tmp })
        }
        if (nextProps.hisrequestincidentData != incident && nextProps.hisrequestincidentData.length > 0) {
            incident = nextProps.hisrequestincidentData
            var c = 0, count = 0, total = 0
            var tmp = []
            incident.forEach((element, i) => {
                tmp[i] = { ...element, unitname: this.props.unittabData[i].label }
                c += parseInt(element.c)
                count += parseInt(element.count)
                total += parseInt(element.total)
            });
            tmp[tmp.length] = { refnum: 31, unitname: "รวม", c: c, count: count, total: total, groupCount: "incident" }
            this.setState({ incident: tmp })
        }
        if (nextProps.hisrequestselectexamData != selectexam && nextProps.hisrequestselectexamData.length > 0) {
            selectexam = nextProps.hisrequestselectexamData
            var c = 0, count = 0, total = 0
            var tmp = []
            selectexam.forEach((element, i) => {
                tmp[i] = { ...element, unitname: this.props.unittabData[i].label }
                c += parseInt(element.c)
                count += parseInt(element.count)
                total += parseInt(element.total)
            });
            tmp[tmp.length] = { refnum: 31, unitname: "รวม", c: c, count: count, total: total, groupCount: "selectexam" }
            this.setState({ selectexam: tmp })

        }
        if (nextProps.hisrequestprovinceData != province && nextProps.hisrequestprovinceData.length > 0) {
            province = nextProps.hisrequestprovinceData
            var c = 0, count = 0, total = 0
            var tmp = []
            province.forEach((element, i) => {
                tmp[i] = { ...element, unitname: this.props.unittabData[i].label }
                c += parseInt(element.c)
                count += parseInt(element.count)
                total += parseInt(element.total)
            });
            tmp[tmp.length] = { refnum: 31, unitname: "รวม", c: c, count: count, total: total, groupCount: "province" }
            this.setState({ province: tmp })
        }
        if (nextProps.hisrequestallData != all && nextProps.hisrequestallData.length > 0) {
            all = nextProps.hisrequestallData
            var c = 0, count = 0, total = 0
            var tmp = []
            all.forEach((element, i) => {
                tmp[i] = { ...element, unitname: this.props.unittabData[i].label }
                c += parseInt(element.c)
                count += parseInt(element.count)
                total += parseInt(element.total)
            });
            tmp[tmp.length] = { refnum: 31, unitname: "รวม", c: c, count: count, total: total, groupCount: "unit3" }
            this.setState({ all: tmp })
        }
    }
    belong(unit) {
        // if(unit == 31){
        //     this.props.fetchRequestReport({ where: "r.selectcode = 1" , groupby: "r.navyid", have: "", sort: "r.unit,substring(r.askby,1,2),r.num" })
        // }else{
        //     this.props.fetchRequestReport({ where: "r.selectcode = 1 and r.unit = "+unit , groupby: "r.navyid", have: "", sort: "substring(r.askby,1,2),r.num" })
        // }
        this.props.belongtab({ belongtab: 1, unit: unit, state: 1 })
        this.props.alert({ type: "success", headline: "กำลังโหลด...", message: < div className="loader" > </div>, timeout: 0, position: "top-right" })
    }
    request(unit) {
        // if(unit == 31){
        //     this.props.fetchRequestReport({ where: "r.selectcode = 1" , groupby: "r.navyid", have: "", sort: "r.unit,substring(r.askby,1,2),r.num" })
        // }else{
        //     this.props.fetchRequestReport({ where: "r.selectcode = 1 and r.unit = "+unit , groupby: "r.navyid", have: "", sort: "substring(r.askby,1,2),r.num" })
        // }
        this.props.requesttab({ requesttab: 2, unit: unit, state: 1 })
        //this.props.alert({ type: "success", headline: "กำลังโหลด...", message: < div className="loader" > </div>, timeout: 0, position: "top-right" })
    }
    selectexam(unit) {
        // if(unit == 31){
        //     this.props.fetchRequestReport({ where: "r.selectcode = 1" , groupby: "r.navyid", have: "", sort: "r.unit,substring(r.askby,1,2),r.num" })
        // }else{
        //     this.props.fetchRequestReport({ where: "r.selectcode = 1 and r.unit = "+unit , groupby: "r.navyid", have: "", sort: "substring(r.askby,1,2),r.num" })
        // }
        this.props.selectexamtab({ selectexamtab: 2, unit: unit, state: 1 })
        this.props.alert({ type: "success", headline: "กำลังโหลด...", message: < div className="loader" > </div>, timeout: 0, position: "top-right" })
    }
    provide(unit) {
        // if(unit == 31){
        //     this.props.fetchRequestReport({ where: "r.selectcode = 1" , groupby: "r.navyid", have: "", sort: "r.unit,substring(r.askby,1,2),r.num" })
        // }else{
        //     this.props.fetchRequestReport({ where: "r.selectcode = 1 and r.unit = "+unit , groupby: "r.navyid", have: "", sort: "substring(r.askby,1,2),r.num" })
        // }
        this.props.providetab({ providetab: 2, unit: unit, state: 1 })
        this.props.alert({ type: "success", headline: "กำลังโหลด...", message: < div className="loader" > </div>, timeout: 0, position: "top-right" })
    }
    change(unit) {
        this.props.changetab({ changetab: 2, unit: unit, state: 1 })
        this.props.alert({ type: "success", headline: "กำลังโหลด...", message: < div className="loader" > </div>, timeout: 0, position: "top-right" })
    }
    incident(unit) {
        this.props.incidenttab({ incidenttab: 2, unit: unit, state: 1 })
        this.props.alert({ type: "success", headline: "กำลังโหลด...", message: < div className="loader" > </div>, timeout: 0, position: "top-right" })
    }
    render() {
        return (
            <div>
                <Panel header="ยอด" bsStyle="primary" expanded={false}>
                    <Tabs defaultActiveKey={this.props.tabactive} id="uncontrolled-tab-example">
                        {this.props.tabactive == 3 ?
                            <Tab eventKey={3} title="คัดเลือก">
                            
                    <Row>
                    <Col sm={12}>
                        <Button onClick={this.refresh} className="pull-right" bsStyle="default"><FontAwesome name="refresh" />&nbsp;รีเฟรช</Button>
                    </Col>
                </Row>
                                <Row>
                                    <Col xs={4}>
                                        <div>
                                            <h5>
                                                <b>หน่วย</b>
                                            </h5>
                                        </div>
                                    </Col>
                                    <Col xs={2}>
                                        <div>
                                            <h5>
                                                <b>อัตรา</b>
                                            </h5>
                                        </div>
                                    </Col>
                                    <Col xs={2}>
                                        <div>
                                            <h5>
                                                <b>คัดเลือก</b>
                                            </h5>
                                        </div>
                                    </Col>
                                    <Col xs={3}>
                                        <div>
                                            <h5>
                                                <b>คงเหลือ</b>
                                            </h5>
                                        </div>
                                    </Col>
                                </Row>
                                {this
                                    .state
                                    .selectexam
                                    .map((item, i) => {
                                        return (
                                            <Row key={item.groupCount + i}>
                                                <Col
                                                    xs={4}
                                                    className={item.count == item.c
                                                        ? "tsuccess hide-text"
                                                        : item.count < item.c
                                                            ? "twarning hide-text"
                                                            : "hide-text"}>
                                                    {item.unitname}
                                                </Col>
                                                <Col
                                                    xs={2}
                                                    className={item.count == item.c
                                                        ? "tsuccess"
                                                        : item.count < item.c
                                                            ? "twarning"
                                                            : ""}>
                                                    {item.count}
                                                </Col>
                                                <Col
                                                    xs={2}
                                                    className={item.count == item.c
                                                        ? "tsuccess"
                                                        : item.count < item.c
                                                            ? "twarning"
                                                            : ""}>
                                                    <a onClick={() => this.selectexam(item.refnum)}>{item.c}</a>

                                                </Col>
                                                <Col
                                                    xs={3}
                                                    className={item.count == item.c
                                                        ? "tsuccess"
                                                        : item.count < item.c
                                                            ? "twarning"
                                                            : ""}>
                                                    {item.total}

                                                </Col>
                                            </Row>
                                        )
                                    })}
                            </Tab>
                            : ""}
                        {this.props.tabactive == 1 ?
                            <Tab eventKey={1} title="ร้องขอ">
                            <Row>
                    <Col sm={12}>
                        <Button onClick={this.refresh} className="pull-right" bsStyle="default"><FontAwesome name="refresh" />&nbsp;รีเฟรช</Button>
                    </Col>
                </Row>
                                <Row>
                                    <Col xs={4}>
                                        <div>
                                            <h5>
                                                <b>หน่วย</b>
                                            </h5>
                                        </div>
                                    </Col>
                                    <Col xs={2}>
                                        <div>
                                            <h5>
                                                <b>อัตรา</b>
                                            </h5>
                                        </div>
                                    </Col>
                                    <Col xs={2}>
                                        <div>
                                            <h5>
                                                <b>ร้องขอ</b>
                                            </h5>
                                        </div>
                                    </Col>
                                    <Col xs={3}>
                                        <div>
                                            <h5>
                                                <b>คงเหลือ</b>
                                            </h5>
                                        </div>
                                    </Col>
                                </Row>
                                {this
                                    .state
                                    .request
                                    .map((item, i) => {

                                        return (
                                            <Row key={item.groupCount + i}>
                                                <Col
                                                    xs={4}
                                                    className={item.count == item.count2
                                                        ? "tsuccess hide-text"
                                                        : item.count < item.count2
                                                            ? "twarning hide-text"
                                                            : "hide-text"}>
                                                    {item.unitname}
                                                </Col>
                                                <Col
                                                    xs={2}
                                                    className={item.count == item.count2
                                                        ? "tsuccess"
                                                        : item.count < item.count2
                                                            ? "twarning"
                                                            : ""}>
                                                    {item.count}
                                                </Col>
                                                <Col
                                                    xs={2}
                                                    className={item.count == item.count2
                                                        ? "tsuccess"
                                                        : item.count < item.count2
                                                            ? "twarning"
                                                            : ""}>
                                                    <a onClick={() => this.request(item.refnum)}>{item.count2}</a>
                                                </Col>
                                                <Col
                                                    xs={3}
                                                    className={item.count == item.count2
                                                        ? "tsuccess"
                                                        : item.count < item.count2
                                                            ? "twarning"
                                                            : ""}>
                                                    {item.total}

                                                </Col>
                                            </Row>
                                        )
                                    })}
                            </Tab>
                            : ""}
                        {this.props.tabactive == 2 ?
                            <Tab eventKey={2} title="มีเหตุ">
                            <Row>
                    <Col sm={12}>
                        <Button onClick={this.refresh} className="pull-right" bsStyle="default"><FontAwesome name="refresh" />&nbsp;รีเฟรช</Button>
                    </Col>
                </Row>
                                <Row>
                                    <Col xs={4}>
                                        <div>
                                            <h5>
                                                <b>หน่วย</b>
                                            </h5>
                                        </div>
                                    </Col>
                                    <Col xs={2}>
                                        <div>
                                            <h5>
                                                <b>อัตรา</b>
                                            </h5>
                                        </div>
                                    </Col>
                                    <Col xs={2}>
                                        <div>
                                            <h5>
                                                <b>เหตุ</b>
                                            </h5>
                                        </div>
                                    </Col>
                                    <Col xs={3}>
                                        <div>
                                            <h5>
                                                <b>คงเหลือ</b>
                                            </h5>
                                        </div>
                                    </Col>
                                </Row>
                                {this
                                    .state
                                    .incident
                                    .map((item, i) => {
                                        return (
                                            <Row key={item.groupCount + i}>
                                                <Col
                                                    xs={4}
                                                    className={item.count == item.c
                                                        ? "tsuccess hide-text"
                                                        : item.count < item.c
                                                            ? "twarning hide-text"
                                                            : "hide-text"}>
                                                    {item.unitname}
                                                </Col>
                                                <Col
                                                    xs={2}
                                                    className={item.count == item.c
                                                        ? "tsuccess"
                                                        : item.count < item.c
                                                            ? "twarning"
                                                            : ""}>
                                                    {item.count}
                                                </Col>
                                                <Col
                                                    xs={2}
                                                    className={item.count == item.c
                                                        ? "tsuccess"
                                                        : item.count < item.c
                                                            ? "twarning"
                                                            : ""}>
                                                    <a onClick={() => this.incident(item.refnum)}>{item.c}</a>

                                                </Col>
                                                <Col
                                                    xs={3}
                                                    className={item.count == item.c
                                                        ? "tsuccess"
                                                        : item.count < item.c
                                                            ? "twarning"
                                                            : ""}>
                                                    {item.total}

                                                </Col>
                                            </Row>
                                        )
                                    })}

                            </Tab>
                            : ""}
                        {this.props.tabactive == 4 ?
                            <Tab eventKey={4} title="จังหวัด">
                            <Row>
                    <Col sm={12}>
                        <Button onClick={this.refresh} className="pull-right" bsStyle="default"><FontAwesome name="refresh" />&nbsp;รีเฟรช</Button>
                    </Col>
                </Row>
                                <Row>
                                    <Col xs={4}>
                                        <div>
                                            <h5>
                                                <b>หน่วย</b>
                                            </h5>
                                        </div>
                                    </Col>
                                    <Col xs={2}>
                                        <div>
                                            <h5>
                                                <b>เฉลี่ย</b>
                                            </h5>
                                        </div>
                                    </Col>
                                    <Col xs={2}>
                                        <div>
                                            <h5>
                                                <b>จชต</b>
                                            </h5>
                                        </div>
                                    </Col>
                                    <Col xs={3}>
                                        <div>
                                            <h5>
                                                <b>คงเหลือ</b>
                                            </h5>
                                        </div>
                                    </Col>
                                </Row>
                                {this
                                    .state
                                    .province
                                    .map((item, i) => {
                                        return (
                                            <Row key={item.groupCount + i}>
                                                <Col
                                                    xs={4}
                                                    className={item.count == item.c
                                                        ? "tsuccess hide-text"
                                                        : item.count < item.c
                                                            ? "twarning hide-text"
                                                            : "hide-text"}>
                                                    {item.unitname}
                                                </Col>
                                                <Col
                                                    xs={2}
                                                    className={item.count == item.c
                                                        ? "tsuccess"
                                                        : item.count < item.c
                                                            ? "twarning"
                                                            : ""}>
                                                    {item.count}
                                                </Col>
                                                <Col
                                                    xs={2}
                                                    className={item.count == item.c
                                                        ? "tsuccess"
                                                        : item.count < item.c
                                                            ? "twarning"
                                                            : ""}>
                                                    <a onClick={() => this.provide(item.refnum)}>{item.c}</a>

                                                </Col>
                                                <Col
                                                    xs={3}
                                                    className={item.count == item.c
                                                        ? "tsuccess"
                                                        : item.count < item.c
                                                            ? "twarning"
                                                            : ""}>
                                                    {item.total}

                                                </Col>
                                            </Row>
                                        )
                                    })}

                            </Tab>
                            : ""}
                        {this.props.tabactive == 5 ?
                            <Tab eventKey={5} title="ยอดรวม">
                            <Row>
                    <Col sm={12}>
                        <Button onClick={this.refresh} className="pull-right" bsStyle="default"><FontAwesome name="refresh" />&nbsp;รีเฟรช</Button>
                    </Col>
                </Row>
                                <Row>
                                    <Col xs={4}>
                                        <div>
                                            <h5>
                                                <b>หน่วย</b>
                                            </h5>
                                        </div>
                                    </Col>
                                    <Col xs={2}>
                                        <div>
                                            <h5>
                                                <b>อัตรา</b>
                                            </h5>
                                        </div>
                                    </Col>
                                    <Col xs={2}>
                                        <div>
                                            <h5>
                                                <b>ปัจจุบัน</b>
                                            </h5>
                                        </div>
                                    </Col>
                                    <Col xs={3}>
                                        <div>
                                            <h5>
                                                <b>คงเหลือ</b>
                                            </h5>
                                        </div>
                                    </Col>
                                </Row>
                                {this
                                    .state
                                    .all
                                    .map((item, i) => {
                                        return (
                                            <Row key={item.groupCount + i}>
                                                <Col
                                                    xs={4}
                                                    className={item.count == item.c
                                                        ? "tsuccess hide-text"
                                                        : item.count < item.c
                                                            ? "twarning hide-text"
                                                            : "hide-text"}>
                                                    {item.unitname}
                                                </Col>
                                                <Col
                                                    xs={2}
                                                    className={item.count == item.c
                                                        ? "tsuccess"
                                                        : item.count < item.c
                                                            ? "twarning"
                                                            : ""}>
                                                    {item.count}
                                                </Col>
                                                <Col
                                                    xs={2}
                                                    className={item.count == item.c
                                                        ? "tsuccess"
                                                        : item.count < item.c
                                                            ? "twarning"
                                                            : ""}>
                                                    <a onClick={() => this.change(item.refnum)}>{item.c}</a>

                                                </Col>
                                                <Col
                                                    xs={3}
                                                    className={item.count == item.c
                                                        ? "tsuccess"
                                                        : item.count < item.c
                                                            ? "twarning"
                                                            : ""}>
                                                    {item.total}

                                                </Col>
                                            </Row>
                                        )
                                    })}
                            </Tab>
                            : ""}
                        {this.props.tabactive == 7 ?
                            <Tab eventKey={7} title="ยอด">
                            <Row>
                    <Col sm={12}>
                        <Button onClick={this.refresh} className="pull-right" bsStyle="default"><FontAwesome name="refresh" />&nbsp;รีเฟรช</Button>
                    </Col>
                </Row>
                                <Row>
                                    <Col xs={4}>
                                        <div>
                                            <h5>
                                                <b>หน่วย</b>
                                            </h5>
                                        </div>
                                    </Col>
                                    <Col xs={2}>
                                        <div>
                                            <h5>
                                                <b>อัตรา</b>
                                            </h5>
                                        </div>
                                    </Col>
                                    <Col xs={2}>
                                        <div>
                                            <h5>
                                                <b>ปัจจุบัน</b>
                                            </h5>
                                        </div>
                                    </Col>
                                    <Col xs={3}>
                                        <div>
                                            <h5>
                                                <b>คงเหลือ</b>
                                            </h5>
                                        </div>
                                    </Col>
                                </Row>
                                {this
                                    .state
                                    .belong
                                    .map((item, i) => {
                                        return (
                                            <Row key={i}>
                                                <Col
                                                    xs={4}
                                                    className={item.count == item.count1
                                                        ? "tsuccess hide-text"
                                                        : item.count < item.count1
                                                            ? "twarning hide-text"
                                                            : "hide-text"}>
                                                    {item.unitname}
                                                </Col>
                                                <Col
                                                    xs={2}
                                                    className={item.count == item.count1
                                                        ? "tsuccess"
                                                        : item.count < item.count1
                                                            ? "twarning"
                                                            : ""}>
                                                    {item.count}
                                                </Col>
                                                <Col
                                                    xs={2}
                                                    className={item.count == item.count1
                                                        ? "tsuccess"
                                                        : item.count < item.count1
                                                            ? "twarning"
                                                            : ""}>
                                                    <a onClick={() => this.belong(item.refnum)}>{item.count1}</a>

                                                </Col>
                                                <Col
                                                    xs={3}
                                                    className={item.count == item.count1
                                                        ? "tsuccess"
                                                        : item.count < item.count1
                                                            ? "twarning"
                                                            : ""}>
                                                    {item.total}

                                                </Col>
                                            </Row>
                                        )
                                    })}
                            </Tab>
                            : ""}

                    </Tabs>
                </Panel>
            </div>
        )
    }
}
function mapStatetoProps(state) {
    return {
        unittabData: state.unittabData,
        hisrequestData: state.hisrequestData,
        hisrequestrequestData: state.hisrequestrequestData,
        hisrequestincidentData: state.hisrequestincidentData,
        hisrequestselectexamData: state.hisrequestselectexamData,
        hisrequestprovinceData: state.hisrequestprovinceData,
        hisrequestallData: state.hisrequestallData,
        hisrequestbelongData: state.hisrequestbelongData
    }
}

function mapDispatchToProps(dispatch) {
    return {
        fetchUnittab: dispatch({ type: "FETCH_UNITTAB_DATA" }),
        fetchHisrequest: dispatch({ type: "FETCH_HISREQUEST_DATA" }),
        fetchHisrequest_request: navyid => dispatch({ type: "FETCH_HISREQUESTREQUEST_DATA" }),
        fetchHisrequest_incident: navyid => dispatch({ type: "FETCH_HISREQUESTINCIDENT_DATA" }),
        fetchHisrequest_selectexam: navyid => dispatch({ type: "FETCH_HISREQUESTSELECTEXAM_DATA" }),
        fetchHisrequest_belong: navyid => dispatch({ type: "FETCH_HISREQUESTBELONG_DATA" }),
        fetchHisrequest_province: armid => dispatch({ type: "FETCH_HISREQUESTPROVINCE_DATA", payload: armid }),
        fetchHisrequest_all: navyid => dispatch({ type: "FETCH_HISREQUESTALL_DATA" }),
        requesttab: requesttab => dispatch({ type: "CHANGE_VIEW_DATA", payload: requesttab }),
        requesttab: unit => dispatch({ type: "CHANGE_VIEW_DATA", payload: unit }),
        requesttab: state => dispatch({ type: "CHANGE_VIEW_DATA", payload: state }),
        changetab: changetab => dispatch({ type: "CHANGE_VIEW_DATA", payload: changetab }),
        changetab: unit => dispatch({ type: "CHANGE_VIEW_DATA", payload: unit }),
        changetab: state => dispatch({ type: "CHANGE_VIEW_DATA", payload: state }),
        incidenttab: incidenttab => dispatch({ type: "CHANGE_VIEW_DATA", payload: incidenttab }),
        incidenttab: unit => dispatch({ type: "CHANGE_VIEW_DATA", payload: unit }),
        incidenttab: state => dispatch({ type: "CHANGE_VIEW_DATA", payload: state }),
        selectexamtab: selectexamtab => dispatch({ type: "CHANGE_VIEW_DATA", payload: selectexamtab }),
        selectexamtab: unit => dispatch({ type: "CHANGE_VIEW_DATA", payload: unit }),
        selectexamtab: state => dispatch({ type: "CHANGE_VIEW_DATA", payload: state }),
        providetab: providetab => dispatch({ type: "CHANGE_VIEW_DATA", payload: providetab }),
        providetab: unit => dispatch({ type: "CHANGE_VIEW_DATA", payload: unit }),
        providetab: state => dispatch({ type: "CHANGE_VIEW_DATA", payload: state }),
        belongtab: belongtab => dispatch({ type: "CHANGE_VIEW_DATA", payload: belongtab }),
        belongtab: unit => dispatch({ type: "CHANGE_VIEW_DATA", payload: unit }),
        belongtab: state => dispatch({ type: "CHANGE_VIEW_DATA", payload: state }),
        fetchRequestReport: where => dispatch({ type: "FETCH_REQUESTREPORT_DATA", payload: where }),
        fetchRequestReport: groupby => dispatch({ type: "FETCH_REQUESTREPORT_DATA", payload: groupby }),
        fetchRequestReport: have => dispatch({ type: "FETCH_REQUESTREPORT_DATA", payload: have }),
        fetchRequestReport: sort => dispatch({ type: "FETCH_REQUESTREPORT_DATA", payload: sort }),
        alert: type => dispatch({ type: "ALERTS", payload: type }),
        alert: headline => dispatch({ type: "ALERTS", payload: headline }),
        alert: message => dispatch({ type: "ALERTS", payload: message }),
        alert: timeout => dispatch({ type: "ALERTS", payload: timeout }),
        alert: position => dispatch({ type: "ALERTS", payload: position }),
    }
}

const ConnectedHisrequest = connect(mapStatetoProps, mapDispatchToProps)(Hisrequest)

export default ConnectedHisrequest