import React, { Component } from 'react'
import ReactDOM from 'react-dom';
import {
    Row,
    Panel,
    Col,
    Form,
    Button,
    FormGroup,
    FormControl,
    ControlLabel,
    Table,
    Tab,
    Tabs
} from 'react-bootstrap'
import Toggle from 'react-bootstrap-toggle';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import Select from 'react-select';

import MdSave from 'react-icons/lib/md/save';
import { connect } from 'react-redux'
import FontAwesome from 'react-fontawesome'
import MdDelete from 'react-icons/lib/md/delete'
import FaEdit from 'react-icons/lib/fa/edit';
import { AlertList, Alert, AlertContainer } from "react-bs-notifier";
import Confirm from "../component/Confirm";
import { Icon } from 'antd';
var tmp = "";
var modalData = [];
let selectid = "", checkdata = 0,
    confirm = "";
let save = false;
class Requestfrom extends Component {
    constructor(props) {
        super(props)
        this.state = {
            inputvalidate: "",
            valueUnit: 0,
            valueUnit3: 0,
            valueAskcode: "",
            valueAskname: "",
            valueNum: null,
            valueRemark: "",
            valueRemark2: "",
            optionsAskcode: [],
            navyid: "",
            position: "top-left",
            alerts: [],
            timeout: 0,
            newMessage: "This is a test of the Emergency Broadcast System. This is only a test.",
            inputvalidate: null,
            request: [],
            unittabdata: [],
            requestData: [],
            modalData: [],
        }
        this.submitunit3 = this.submitunit3.bind(this);
        this.changerequest = this
            .changerequest
            .bind(this)
        this.logChangeUnit = this
            .logChangeUnit
            .bind(this);
        this.logChangeUnit3 = this
            .logChangeUnit3
            .bind(this);
        this.logChangeAskcode = this
            .logChangeAskcode
            .bind(this);
        this.generate = this
            .generate
            .bind(this)
        this.submit = this
            .submit
            .bind(this)
        this.handleAskname = this
            .handleAskname
            .bind(this)
        this.handleNum = this
            .handleNum
            .bind(this)
        this.handleRemark = this
            .handleRemark
            .bind(this)
        this.handleRemark2 = this
            .handleRemark2
            .bind(this)
        this.keypressAskname = this
            .keypressAskname
            .bind(this)
        this.keypressNum = this
            .keypressNum
            .bind(this)
        this.keypressRemark = this
            .keypressRemark
            .bind(this)
        this.keypressRemark2 = this
            .keypressRemark2
            .bind(this)
        this.setrequestnum = this
            .setrequestnum
            .bind(this)
        this.editrequest = this
            .editrequest
            .bind(this)
        this.deleterequest = this
            .deleterequest
            .bind(this)
        this.lockaskby = this.lockaskby.bind(this)
    }
    componentDidMount() {
        selectid = "";
        this.setState({ unittabdata: this.props.unittabData })
        var tmp = []
        for (var i = 1; i <= 20; i++) {
            tmp[i - 1] = {
                value: i < 10
                    ? "0" + i.toString()
                    : i.toString(),
                label: i < 10
                    ? "0" + i
                    : i
            }
        }
        this.setState({ optionsAskcode: tmp })
        this.refs.RequestUnit.focus()
    }
    logChangeUnit3(val) {
        this.setState({
            valueUnit3: val == null
                ? 0
                : val.value
        })
    }
    logChangeUnit(val) {
        this.setState({
            valueUnit: val == null
                ? 0
                : val.value
        })
        if (val != null) {
            if (val.value != "0") {
                this.refs.RequestAskcode.focus()
                if (this.state.valueAskcode != "") {
                    this.props.fetchNumRequet({ Unit: val.value, Askcode: this.state.valueAskcode })
                }
            } else {
                this
                    .refs
                    .RequestUnit
                    .focus()
                this.generate("danger", "หน่วยร้องขอไม่ถูกต้อง", "หน่วยต้องไม่ใช่ \"ไม่ได้ระบุ\"", 2000, "error")
            }
        }
    }
    logChangeAskcode(val) {
        if (this.state.valueUnit == 0) {
            this
                .refs
                .RequestUnit
                .focus()
            this.generate("danger", "หน่วยร้องขอไม่ถูกต้อง", "หน่วยต้องไม่ใช่ \"ไม่ได้ระบุ\"", 2000, "error")
        } else {
            if (val.value != "0") {
                this
                    .props
                    .fetchNumRequet({ Unit: this.state.valueUnit, Askcode: val.value })
                ReactDOM
                    .findDOMNode(this.RequestAskname)
                    .focus()

            } else {
                this
                    .refs
                    .RequestAskcode
                    .focus()
                this.generate("danger", "รหัมผู้ขอไม่ถูกต้อง", "รหัสผู้ขอต้องเป็น \"01-20\"", 2000, "error")
            }
        }
        this.setState({ valueNum: null })
        this.setState({
            valueAskcode: val == null
                ? ""
                : val.value
        })
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.numrequestData.Num != undefined) {
            if (this.state.valueNum == null && this.state.valueAskcode != "") {
                this.setrequestnum(nextProps.numrequestData.Num)
            }
        }
        if (nextProps.checknumrequestData[0] != undefined) {
            if (this.state.valueNum != null && this.state.valueAskcode != "") {
                nextProps
                    .checknumrequestData
                    .map((item, i) => {
                        this.generate("danger", "ลำดับซ้ำ", "พลฯ " + item.name + " " + item.sname + " หน่วย " + item.unitname + " ผู้ขอ " + item.askby + " ลำดับ " + item.num + " หมายเหตุ " + item.remark + " ขอต่อ " + item.remark2, 2000, '')
                    })
            }
        }
        if (nextProps.modalData.NAVYID != this.state.navyid) {
            this.props.fetchmodalData({ navyid: nextProps.modalData.NAVYID })


            this.setState({ valueNum: null })
            this.setState({ valueUnit: 0 })
            this.setState({ valueAskcode: "" })
            this.setState({ valueAskname: "" })
            this.setState({ valueRemark: "" })
            this.setState({ valueRemark2: "" })
            this.setState({ navyid: nextProps.modalData.NAVYID })
            this.setState({ request: this.props.requestData })
            this
                .props
                .fetchRequest({ navyid: nextProps.modalData.NAVYID })
        }
        if (nextProps.modalData != modalData) {
            modalData = nextProps.modalData
            if (this.state.valueUnit3 != nextProps.modalData.valueUnit3)
                this.setState({ valueUnit3: nextProps.modalData.valueUnit3 })
            this.props.fetchHisrequest_request({});
            this.props.fetchHisrequest_all({});
        }
        if (nextProps.insertrequestData.success == "complete" && checkdata == 0) {
            this
                .props
                .fetchRequest({ navyid: nextProps.modalData.NAVYID })
            checkdata = 1;
        }
        if (nextProps.requestData != this.state.requestData) {
            if (!nextProps.requestData.hasOwnProperty('error'))
                this.setState({ requestData: nextProps.requestData })
        }
    }
    generate(type, headline, message, timeout, headtype) {
        const newAlert = {
            id: (new Date()).getTime(),
            type: type,
            headline: `${headline}`,
            message: message
        };
        this.setState({ timeout: timeout })
        this.setState({ inputvalidate: headtype })
        this.setState({
            alerts: [
                ...this.state.alerts,
                newAlert
            ]
        });
    }
    setrequestnum(Num) {
        this.setState({
            valueNum: Num + 1
        })
    }
    submit() {
        // if(save){
        //     save=false;
        //     this.props.modal({showModal:false});
        //     return true;
        // }
        checkdata = 0;
        if (this.state.valueUnit == 0) {
            this.generate("danger", "หน่วยร้องขอไม่ถูกต้อง", "หน่วยต้องไม่ใช่ \"ไม่ได้ระบุ\"", 2000, "error")
            return false;
        } else if (this.state.valueAskcode == "") {
            this.generate("danger", "รหัมผู้ขอไม่ถูกต้อง", "รหัสผู้ขอต้องเป็น \"01-20\"", 2000, "error")
            return false;
        } else if (this.state.valueNum == "") {
            this.generate("danger", "ลำดับไม่ถูกต้อง", "ลำดับต้องเป็นตัวเลขเท่านั้น", 2000, "error")
            return false;
        }
        if (selectid == "") {
            this.props.fetchCheckNum({ Unit: this.state.valueUnit, Askcode: this.state.valueAskcode, Num: this.state.valueNum });
            if (this.props.requestData.length >= 5) {
                this.generate("danger", "ผู้ขอเกิน", "ผู้ขอจะต้องไม่เกิน 5 คนเท่านั้น ต้องทำการลบ หรือ แก้ไขข้อมูลผู้ขอ แทนการเพิ่มข้อ" +
                    "มูลผู้ขอ",
                    0, "error");
                return false;
            } else {
                this.props.fetchinsertRequest({
                    navyid: this.state.navyid,
                    unit: this.state.valueUnit,
                    askcode: this.state.valueAskcode,
                    askname: this.state.valueAskname,
                    num: this.state.valueNum,
                    remark: this.state.valueRemark,
                    remark2: this.state.valueRemark2
                });
            }
            this.setState({ navyid: "" })
            this.generate("success", "บันทึกเสร็จสิ้น", "หน่วย: \"" + this.state.valueUnit + "\" ผู้ขอ:\"" + this.state.valueAskcode + " " + this.state.valueAskname + "\" ลำดับ:\"" + this.state.valueNum + "\" หมายเหตุ:\"" + this.state.valueRemark + "\" ขอต่อ:\"" + this.state.valueRemark2 + "\"", 2000, "success")
            save = true;
        } else {

            this
                .props
                .fetcheditRequest({
                    navyid: this.state.navyid,
                    unit: this.state.valueUnit,
                    askcode: this.state.valueAskcode,
                    askname: this.state.valueAskname,
                    num: this.state.valueNum,
                    remark: this.state.valueRemark,
                    remark2: this.state.valueRemark2,
                    selectid: selectid
                });
            this.setState({ navyid: "" })
            save = true;
            selectid = "";
            this.generate("warning", "แก้ไขเสร็จสิ้น", "หน่วย: \"" + this.state.valueUnit + "\" ผู้ขอ:\"" + this.state.valueAskcode + " " + this.state.valueAskname + "\" ลำดับ:\"" + this.state.valueNum + "\" หมายเหตุ:\"" + this.state.valueRemark + "\" ขอต่อ:\"" + this.state.valueRemark2 + "\"", 2000, "success")
        }


    }
    onAlertDismissed(alert) {
        const alerts = this.state.alerts;

        // find the index of the alert that was dismissed
        const idx = alerts.indexOf(alert);

        if (idx >= 0) {
            this.setState({
                // remove the alert from the array
                alerts: [
                    ...alerts.slice(0, idx),
                    ...alerts.slice(idx + 1)
                ]
            });
        }
    }
    handleAskname(val) {
        this.setState({ valueAskname: val.target.value })
    }
    handleNum(val) {
        this.setState({ valueNum: val.target.value })
    }
    handleRemark(val) {
        this.setState({ valueRemark: val.target.value })
    }
    handleRemark2(val) {
        this.setState({ valueRemark2: val.target.value })
    }
    keypressAskname(val) {
        if (val.key == "Enter") {
            ReactDOM
                .findDOMNode(this.RequestNum)
                .focus()
        }
    }
    keypressNum(val) {
        if (val.key == "Enter") {
            if (selectid == "") {
                this.props.fetchCheckNum({ Unit: this.state.valueUnit, Askcode: this.state.valueAskcode, Num: this.state.valueNum })

            }
            ReactDOM.findDOMNode(this.RequestRemark).focus()
        }
    }
    keypressRemark(val) {
        if (val.key == "Enter") {
            ReactDOM
                .findDOMNode(this.RequestRemark2)
                .focus()
        }
    }
    keypressRemark2(val) {
        if (val.key == "Enter") {
            ReactDOM
                .findDOMNode(this.RequestSubmit)
                .focus()
        }
    }
    clearAlerts() {
        this.setState({ alerts: [] });
    }
    changerequest(id) {
        if (this.props.requestData.length != 0) {
            for (var i = 0; i < this.props.requestData.length; i++) {
                if (1 == this.props.requestData[i].lockselectcode && 1 == this.props.requestData[i].selectcode) {
                    this.generate("danger", "ไม่สามารถเปลียนแปลงได้เนื่องจากถูกล็อกผู้ขอ", "หน่วย: \"" + this.props.requestData[i].unit + "\" ผู้ขอ:\"" + this.props.requestData[i].askcode + " " + this.props.requestData[i].askname + "\" ลำดับ:\"" + this.props.requestData[i].num + "\" หมายเหตุ:\"" + this.props.requestData[i].remark + "\" ขอต่อ:\"" + this.props.requestData[i].remark2 + "\"", 2000, "success")
                    return
                }
            }
        }
        this.props.fetchchangeRequest({ navyid: this.state.navyid, selectid: id })
        selectid = "";
        this.setState({ navyid: "" })

    }
    editrequest(id) {
        checkdata = 0;
        if (selectid == id) {
            this.setState({ valueNum: null })
            this.setState({ valueUnit: 0 })
            this.setState({ valueAskcode: "" })
            this.setState({ valueAskname: "" })
            this.setState({ valueRemark: "" })
            this.setState({ valueRemark2: "" })
            selectid = "";
        } else {
            if (this.props.requestData.length != 0) {
                for (var i = 0; i < this.props.requestData.length; i++) {
                    if (id == this.props.requestData[i].selectid) {
                        selectid = id;
                        this.setState({
                            valueUnit: parseInt(this.props.requestData[i].unit),
                            valueAskcode: this.props.requestData[i].askcode,
                            valueAskname: this.props.requestData[i].askname,
                            valueNum: this.props.requestData[i].num,
                            valueRemark: this.props.requestData[i].remark,
                            valueRemark2: this.props.requestData[i].remark2
                        });
                    }
                }
            }
        }

    }
    deleterequest(id) {
        checkdata = 0;
        if (this.props.requestData.length != 0) {
            for (var i = 0; i < this.props.requestData.length; i++) {
                if (id == this.props.requestData[i].selectid) {
                    
                    if (1 == this.props.requestData[i].lockselectcode && 1 == this.props.requestData[i].selectcode) {
                        this.generate("danger", "ไม่สามารถเปลียนแปลงได้เนื่องจากถูกล็อกผู้ขอ", "หน่วย: \"" + this.props.requestData[i].unit + "\" ผู้ขอ:\"" + this.props.requestData[i].askcode + " " + this.props.requestData[i].askname + "\" ลำดับ:\"" + this.props.requestData[i].num + "\" หมายเหตุ:\"" + this.props.requestData[i].remark + "\" ขอต่อ:\"" + this.props.requestData[i].remark2 + "\"", 2000, "success")
                        return
                    }
                    this.generate("danger", "ลบเสร็จสิ้น", "หน่วย: \"" + this.props.requestData[i].unit + "\" ผู้ขอ:\"" + this.props.requestData[i].askcode + " " + this.props.requestData[i].askname + "\" ลำดับ:\"" + this.props.requestData[i].num + "\" หมายเหตุ:\"" + this.props.requestData[i].remark + "\" ขอต่อ:\"" + this.props.requestData[i].remark2 + "\"", 2000, "success")
                }
            }
        }
        this.props.fetchdeleteRequest({ navyid: this.state.navyid, selectid: id })
        this.setState({ navyid: "" })

    }
    lockaskby(id, code) {
        checkdata = 0;
        if (this.props.requestData.length != 0) {
            for (var i = 0; i < this.props.requestData.length; i++) {
                if (id == this.props.requestData[i].selectid) {
                    this.generate("success", (code == 0 ? "ล็อกเสร็จสิ้น" : "ปลดล็อกเสร็จสิ้น"), "หน่วย: \"" + this.props.requestData[i].unit + "\" ผู้ขอ:\"" + this.props.requestData[i].askcode + " " + this.props.requestData[i].askname + "\" ลำดับ:\"" + this.props.requestData[i].num + "\" หมายเหตุ:\"" + this.props.requestData[i].remark + "\" ขอต่อ:\"" + this.props.requestData[i].remark2 + "\"", 2000, "success")
                }
            }
        }
        let scode
        if (code == 1) {
            scode = 0;
        }
        else {
            scode = 1;
        }
        this.props.fetchlockRequest({ navyid: this.state.navyid, selectid: id, lockselectcode: scode })
        this.setState({ navyid: "" })
    }
    submitunit3() {
        // if (this.props.requestData.length != 0) {
        //     this.generate("waring", "แจ้งเตือน", "ผู้ขอ โปรดแก้ไขข้อมูลในแท็บร้องขอ แทนการเปลี่ยนหน่วยโดยตรง", 0, "success")
        //     return false
        // }
        // else {
        this.props.fetchchangeunit3({ navyid: this.state.navyid, unit3: this.state.valueUnit3, type: "Change" })
        var navyid = this.state.navyid
        this.setState({ navyid: "" })
        // this.props.fetchmodalData({ navyid: navyid })
        this.generate("success", "เสร็จสิ้น", "เปลี่ยนหน่วยเสร็จสิ้น", 2000, "success")
        // }

    }
    render() {
        return (
            <div>

                <AlertList
                    position={this.state.position}
                    alerts={this.state.alerts}
                    timeout={this.state.timeout}
                    dismissTitle="Begone!"
                    onDismiss={this
                        .onAlertDismissed
                        .bind(this)} />
                < Panel bsStyle="primary">
                    <Row>
                        <Col xs={12}>
                            <Form onSubmit={this.handleSubmit}>
                                <Row>
                                    <Col xs={3}>
                                        <div>
                                            <b>หน่วยร้องขอ</b>
                                        </div>
                                    </Col>
                                    <Col xs={9}>
                                        <Select
                                            placeholder="เลือกหน่วย"
                                            name="Unit"
                                            clearable={false}
                                            value={this.state.valueUnit}
                                            options={this.props.unittabData}
                                            onChange={this.logChangeUnit}
                                            ref="RequestUnit" />
                                    </Col>
                                </Row>
                                <Row>
                                    <Col xs={3}>
                                        <div>
                                            <b>ผู้ขอ</b>
                                        </div>
                                    </Col>
                                    <Col xs={3}>
                                        <Select
                                            placeholder="รหัสผู้ขอ"
                                            name="Askcode"
                                            clearable={false}
                                            value={this.state.valueAskcode}
                                            options={this.state.optionsAskcode}
                                            onChange={this.logChangeAskcode}
                                            ref="RequestAskcode" />
                                    </Col>
                                    <Col xs={6}>
                                        <FormControl
                                            style={{
                                                height: "36px !important"
                                            }}
                                            placeholder="ชื่อผู้ขอ"
                                            type="text"
                                            onChange={this.handleAskname}
                                            onKeyPress={this.keypressAskname}
                                            name="Askname"
                                            value={this.state.valueAskname}
                                            ref={(RequestAskname) => {
                                                this.RequestAskname = RequestAskname;
                                            }} />
                                    </Col>
                                </Row>
                                <Row>
                                    <Col xs={3}>
                                        <div>
                                            <b>ลำดับ</b>
                                        </div>
                                    </Col>
                                    <Col xs={9}>
                                        <FormControl
                                            placeholder="ลำดับ"
                                            type="number"
                                            onChange={this.handleNum}
                                            value=
                                            {this.state.valueNum != null ? this.state.valueNum : ""}
                                            onKeyPress={this.keypressNum}
                                            name="Num"
                                            ref={(RequestNum) => {
                                                this.RequestNum = RequestNum;
                                            }} />
                                    </Col>
                                </Row>
                                <Row>
                                    <Col xs={3}>
                                        <div>
                                            <b>หมายเหตุ</b>
                                        </div>
                                    </Col>
                                    <Col xs={9}>
                                        <FormControl
                                            placeholder="หมายเหตุ"
                                            type="text"
                                            onChange={this.handleRemark}
                                            onKeyPress={this.keypressRemark}
                                            name="Remark"
                                            value={this.state.valueRemark}
                                            ref={(RequestRemark) => {
                                                this.RequestRemark = RequestRemark;
                                            }} />
                                    </Col>
                                </Row>
                                <Row>
                                    <Col xs={3}>
                                        <div>
                                            <b>ขอต่อ</b>
                                        </div>
                                    </Col>
                                    <Col xs={9}>
                                        <FormControl
                                            placeholder="ขอต่อ"
                                            type="text"
                                            onChange={this.handleRemark2}
                                            onKeyPress={this.keypressRemark2}
                                            name="Remark2"
                                            value={this.state.valueRemark2}
                                            ref={(RequestRemark2) => {
                                                this.RequestRemark2 = RequestRemark2;
                                            }} />
                                    </Col>
                                </Row>
                                <Row>
                                    <Col xs={3}></Col>
                                    <Col xs={3}></Col>
                                    <Col xs={3}></Col>
                                    <Col xs={3}>
                                        <Button
                                            bsStyle="primary"
                                            className=" pull-right"
                                            onClick={this.submit}
                                            ref={(RequestSubmit) => {
                                                this.RequestSubmit = RequestSubmit;
                                            }}><MdSave />&nbsp;บันทึก</Button>
                                    </Col>
                                </Row>
                            </Form>
                        </Col>
                    </Row>
                </ Panel>
                < Panel bsStyle="primary">
                    <Tabs defaultActiveKey={1} id="uncontrolled-tab-example">
                        <Tab eventKey={1} title="ร้องขอ">
                            <Row>
                                <Col xs={12}>
                                    <Table striped bordered condensed>
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>หน่วย</th>
                                                <th>ผู้ขอ</th>
                                                <th>ลำดับ</th>
                                                <th>หมายเหตุ</th>
                                                <th>ขอต่อ</th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {
                                                this
                                                    .state
                                                    .requestData
                                                    .map((item, i) => {
                                                        return (
                                                            <tr
                                                                /* className={item.selectcode > 0
                                                                ? "tsuccess"
                                                                : ""} */
                                                                key={item.selectid}>
                                                                <td>{item.rowNumber}</td>
                                                                <td>{item.unitname}</td>
                                                                <td>{item.askby}</td>
                                                                <td>{item.num}</td>
                                                                <td>{item.remark}</td>
                                                                <td>{item.remark2}</td>
                                                                < td >
                                                                    <Confirm
                                                                        onConfirm={() => this.changerequest(item.selectid)}
                                                                        body="ต้องการเปลี่ยนผู้ขอจริงหรือไม่"
                                                                        confirmText="เปลี่ยน"
                                                                        cancelText="ยกเลิก"
                                                                        confirmBSStyle="primary"
                                                                        title="ยืนยันการเปลี่ยนผู้ขอ">
                                                                        <Button
                                                                            bsStyle={item.selectcode > 0
                                                                                ? "success"
                                                                                : "default"}
                                                                            disabled={item.selectcode > 0
                                                                                ? true
                                                                                : false}
                                                                            bsSize="sm">
                                                                            <FontAwesome
                                                                                name={item.selectcode > 0
                                                                                    ? "hand-o-left"
                                                                                    : ""} /> {item.selectcode > 0
                                                                                        ? " เลือกแล้ว"
                                                                                        : " เลือก"}</Button>

                                                                    </Confirm>

                                                                </td>
                                                                < td >
                                                                    <Button
                                                                        onClick={() => this.editrequest(item.selectid)}
                                                                        bsStyle={item.selectid == selectid
                                                                            ? "default"
                                                                            : "warning"}
                                                                        bsSize="sm">
                                                                        <FontAwesome name="edit" /> {item.selectid == selectid
                                                                            ? " ยกเลิก"
                                                                            : " แก้ไข"}</Button>
                                                                    <Confirm
                                                                        onConfirm={() => this.deleterequest(item.selectid)}
                                                                        body="ต้องการลบผู้ขอจริงหรือไม่"
                                                                        confirmText="ลบ"
                                                                        cancelText="ยกเลิก"
                                                                        title="ยืนยันการลบ">
                                                                        <Button bsStyle="danger" bsSize="sm">
                                                                            <FontAwesome name="trash" />
                                                                            ลบ</ Button>
                                                                    </Confirm>
                                                                </td>
                                                                < td >
                                                                    <Confirm

                                                                        onConfirm={() => this.lockaskby(item.selectid, item.lockselectcode)}
                                                                        body={item.lockselectcode == 0 ? "ต้องการล็อกผู้ขอจริงหรือไม่" : "ต้องการปลดล็อกผู้ขอจริงหรือไม่"}
                                                                        confirmText={item.lockselectcode == 0 ? "ล็อก" : "ปลดล็อก"}
                                                                        cancelText="ยกเลิก"
                                                                        confirmBSStyle="primary"
                                                                        title={item.lockselectcode == 0 ? "ยืนยันการล็อก" : "ยืนยันการปลดล็อก"}>
                                                                        <Button
                                                                            bsStyle={item.lockselectcode == 0 ? "danger" : "success"}
                                                                            hidden={item.selectcode == 0}
                                                                            bsSize="sm">
                                                                            <Icon type={item.lockselectcode == 0 ? "unlock" : "lock"} /></Button>

                                                                    </Confirm>

                                                                </td>
                                                            </tr>
                                                        )
                                                    })}
                                        </tbody>
                                    </Table>

                                </Col>
                            </Row>
                        </Tab>
                        <Tab eventKey={2} title="หน่วยปัจจุบัน">
                            <br />
                            <Form onSubmit={this.handlechangeUnit3}>
                                <Row>
                                    <Col xs={3}>
                                        <div>
                                            <b>หน่วยร้องขอ</b>
                                        </div>
                                    </Col>
                                    <Col xs={6}>
                                        <Select
                                            placeholder="เลือกหน่วย"
                                            name="Unit3"
                                            clearable={false}
                                            value={this.state.valueUnit3}
                                            options={this.props.unittabData}
                                            onChange={this.logChangeUnit3}
                                            ref="Unit3" />
                                    </Col>
                                    <Col xs={3}>
                                        <Confirm
                                            onConfirm={this.submitunit3}
                                            body="มีการร้องขอลงหน่วย"
                                            confirmText="บันทึก"
                                            cancelText="ยกเลิก"
                                            confirmBSStyle="primary"
                                            title="แจ้งเตือน">
                                            <Button bsStyle="primary" bsSize="sm">
                                                <FontAwesome name="save" />
                                                &nbsp;บันทึก</ Button>
                                        </Confirm>

                                    </Col>
                                </Row>
                            </Form>
                        </Tab>
                    </Tabs>
                </ Panel>
            </div>
        )
    }
}

function mapStatetoProps(state) {
    // console.log("เช็ค mapstatetoprops" + JSON.stringify(requestData))
    return {
        insertrequestData: state.insertrequestData,
        editrequestData: state.editrequestData,
        deleterequestData: state.deleterequestData,
        lockRequestData: state.lockRequestData,
        changerequestData: state.changerequestData,
        requestData: state.requestData,
        modalData: state.modalData,
        unittabData: state.unittabData,
        changeUnit3: state.changeUnit3,
        numrequestData: state.numrequestData,
        checknumrequestData: state.checknumrequestData,
        view: state.view
    }
}

function mapDispatchToProps(dispatch) {
    return {
        fetchHisrequest_all: navyid => dispatch({ type: "FETCH_HISREQUESTALL_DATA" }),
        fetchHisrequest_request: navyid => dispatch({ type: "FETCH_HISREQUESTREQUEST_DATA" }),

        fetchRequest: navyid => dispatch({ type: "FETCH_REQUEST_DATA", payload: navyid }),
        fetchinsertRequest: navyid => dispatch({ type: "FETCH_INSERTREQUEST_DATA", payload: navyid }),
        fetchinsertRequest: unit => dispatch({ type: "FETCH_INSERTREQUEST_DATA", payload: unit }),
        fetchinsertRequest: askcode => dispatch({ type: "FETCH_INSERTREQUEST_DATA", payload: askcode }),
        fetchinsertRequest: askname => dispatch({ type: "FETCH_INSERTREQUEST_DATA", payload: askname }),
        fetchinsertRequest: num => dispatch({ type: "FETCH_INSERTREQUEST_DATA", payload: num }),
        fetchinsertRequest: remark => dispatch({ type: "FETCH_INSERTREQUEST_DATA", payload: remark }),

        fetcheditRequest: remark2 => dispatch({ type: "FETCH_EDITREQUEST_DATA", payload: remark2 }),
        fetcheditRequest: navyid => dispatch({ type: "FETCH_EDITREQUEST_DATA", payload: navyid }),
        fetcheditRequest: unit => dispatch({ type: "FETCH_EDITREQUEST_DATA", payload: unit }),
        fetcheditRequest: askcode => dispatch({ type: "FETCH_EDITREQUEST_DATA", payload: askcode }),
        fetcheditRequest: askname => dispatch({ type: "FETCH_EDITREQUEST_DATA", payload: askname }),
        fetcheditRequest: num => dispatch({ type: "FETCH_EDITREQUEST_DATA", payload: num }),
        fetcheditRequest: remark => dispatch({ type: "FETCH_EDITREQUEST_DATA", payload: remark }),
        fetcheditRequest: remark2 => dispatch({ type: "FETCH_EDITREQUEST_DATA", payload: remark2 }),
        fetcheditRequest: navyid => dispatch({ type: "FETCH_EDITREQUEST_DATA", payload: navyid }),

        fetchdeleteRequest: navyid => dispatch({ type: "FETCH_DELETEREQUEST_DATA", payload: navyid }),
        fetchdeleteRequest: selectid => dispatch({ type: "FETCH_DELETEREQUEST_DATA", payload: selectid }),
        fetchlockRequest: lockselectcode => dispatch({ type: "FETCH_LOCKREQUEST_DATA", payload: lockselectcode }),
        fetchlockRequest: navyid => dispatch({ type: "FETCH_LOCKREQUEST_DATA", payload: navyid }),
        fetchlockRequest: selectid => dispatch({ type: "FETCH_LOCKREQUEST_DATA", payload: selectid }),
        fetchchangeRequest: navyid => dispatch({ type: "FETCH_CHANGEREQUEST_DATA", payload: navyid }),
        fetchchangeRequest: selectid => dispatch({ type: "FETCH_CHANGEREQUEST_DATA", payload: selectid }),
        fetchchangeunit3: navyid => dispatch({ type: "FETCH_CHANGEUNIT3_DATA", payload: navyid }),
        fetchchangeunit3: unit3 => dispatch({ type: "FETCH_CHANGEUNIT3_DATA", payload: unit3 }),
        fetchchangeunit3: type => dispatch({ type: "FETCH_CHANGEUNIT3_DATA", payload: type }),
        fetchmodalData: navyid => dispatch({ type: "FETCH_MODAL_DATA", payload: navyid }),
        fetchUnittab: navyid => dispatch({ type: "FETCH_UNITTAB_DATA" }),
        fetchNumRequet: Unit => dispatch({ type: "FETCH_NUMREQUEST_DATA", payload: Unit }),
        fetchNumRequet: Askcode => dispatch({ type: "FETCH_NUMREQUEST_DATA", payload: Askcode }),
        fetchCheckNum: Unit => dispatch({ type: "FETCH_CHECKNUMREQUEST_DATA", payload: Unit }),
        fetchCheckNum: Askcode => dispatch({ type: "FETCH_CHECKNUMREQUEST_DATA", payload: Askcode }),
        fetchCheckNum: Num => dispatch({ type: "FETCH_CHECKNUMREQUEST_DATA", payload: Num }),

        modal: showModal => dispatch({ type: "CHANGE_VIEW_DATA", payload: showModal }),

    }
}

const ConnectedRequest = connect(mapStatetoProps, mapDispatchToProps)(Requestfrom)
export default ConnectedRequest