import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux'
import { BootstrapTable, TableHeaderColumn, ButtonGroup } from 'react-bootstrap-table';
import { Tabs, Tab, FormControl, HelpBlock, Col, Row, ControlLabel, Button, Table } from 'react-bootstrap';
import { AlertList, Alert, AlertContainer } from "react-bs-notifier";
import Requestfrom from '../component/Requestfrom'
import FaSearch from 'react-icons/lib/fa/search'
import FontAwesome from 'react-fontawesome'
import ModalDetail from '../component/ModalDetail'
import pdfMake from "pdfmake/build/pdfmake";
import pdfFonts from "pdfmake/build/vfs_fonts";
import Select from 'react-select';
import { request } from 'https';
import Education from '../view/Education';
import TableFilter from 'react-table-filter';

import SampleData from './sampleData.json';
pdfMake.vfs = pdfFonts.pdfMake.vfs;
var tmp = [], tmp2 = [];
var navyid = "", navyid2 = "", query = 0
var chec = 0, pdfdata = [], tableData = [];
class Table_changeUnit3 extends Component {
    constructor(props) {
        super(props)
        this.onAfterSaveCell = this.onAfterSaveCell.bind(this)
        this.onBeforeSaveCell = this.onBeforeSaveCell.bind(this)
        this.state = {
            position: "top-left",
            alerts: [],
            timeout: 0,
            newMessage: "This is a test of the Emergency Broadcast System. This is only a test.",
            cellEditProp: {
                mode: 'click',
                clickToSelectAndEditCell: true,
                blurToSave: true,
                beforeSaveCell: this.onBeforeSaveCell, // a hook for before saving cell
                afterSaveCell: this.onAfterSaveCell  // a hook for after saving cell
            },
            unit: [],
            currentUnit: [],
            valueChangeAllUnit3: "",
            start: "",
            to: "",
            total: "",
            valueChangeAllUnit3: "",
            valueLimit: "",
            upddatedData: [],
            filterConfiguration: false
        }
        this.generate = this.generate.bind(this)
        this.tablemodal = this.tablemodal.bind(this)
        this.close = this.close.bind(this)
        this.buttonFormatter2 = this.buttonFormatter2.bind(this)
        this.createCustomButtonGroup = this.createCustomButtonGroup.bind(this)
        this.exportPDF = this.exportPDF.bind(this)
        this.afterColumnFilter = this.afterColumnFilter.bind(this)
        this.afterSearch = this.afterSearch.bind(this)
        this.trClassFormat = this.trClassFormat.bind(this)
        this.logChangeAllUnit3 = this.logChangeAllUnit3.bind(this)
        this.changeAllUnit3 = this.changeAllUnit3.bind(this)
        this.renderPaginationShowsTotal = this.renderPaginationShowsTotal.bind(this)
        this.handleLimit = this.handleLimit.bind(this)
        this.clearAlerts = this.clearAlerts.bind(this)
        this.unit3Formatter = this.unit3Formatter.bind(this)
        this.unit1Formatter = this.unit1Formatter.bind(this)
        this.unit2Formatter = this.unit2Formatter.bind(this)
        this.unit4Formatter = this.unit4Formatter.bind(this)
        this.unit0Formatter = this.unit0Formatter.bind(this)
        this.unit3validate = this.unit3validate.bind(this)
        this.filterUpdated1 = this.filterUpdated1.bind(this)
        this.filterUpdated2 = this.filterUpdated2.bind(this)
    }
    createCustomButtonGroup(props) {
        return (
            <ButtonGroup className='my-custom-class' sizeClass='btn-group-md'>
                {props.showSelectedOnlyBtn}
                {props.exportCSVBtn}
                {props.insertBtn}
                {props.deleteBtn}
                <button type='button'
                    className={`btn btn-primary`}
                    onClick={this.exportPDF}>
                    <FontAwesome name="download" />
                    &nbsp;Export to PDF
            </button>
            </ButtonGroup>
        );
    }
    exportPDF() {
        // const { vehicleData } = this.props.parkedVehicle; const { plate_no, max_time,
        //   entry_date_time,   exit_date_time,   expiry_time,   address1, address2,
        // city,   state,   zip,   country,   parking_status } = vehicleData; var
        // converter = new pdfConverter(); var doc = converter.jsPDF('p', 'pt');
        if (pdfdata.length == 0) {
            this.props.alert({ type: "danger", headline: "ผิดพลาด", message: "ไม่พบรายชื่อที่จะออกรายงาน", timeout: 2000, position: "top-right" })
            return false
        }
        var data = [],
            contstr = [],
            j = 0
        var data = [
            [
                {
                    colSpan: 13,
                    border: [
                        false, false, false, false
                    ],
                    text: 'รายชื่อทหาร ผลัด ' + pdfdata[0].yearin,
                    alignment: 'center'
                },
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            [
                {
                    border: [
                        false, true, false, true
                    ],
                    text: 'ที่   '
                }, {
                    border: [
                        false, true, false, true
                    ],
                    text: 'ร/พ'
                }, {
                    border: [
                        false, true, false, true
                    ],
                    text: 'ทะเบียน'
                }, {
                    border: [
                        false, true, false, true
                    ],
                    text: 'ชื่อ - สกุล'
                }, {
                    border: [
                        false, true, false, true
                    ],
                    text: 'ผู้ขอ'
                }, {
                    border: [
                        false, true, false, true
                    ],
                    text: 'หน่วยที่ขอ'
                }, {
                    border: [
                        false, true, false, true
                    ],
                    text: 'ขอต่อ'
                }, {
                    border: [
                        false, true, false, true
                    ],
                    text: 'หมายเหตุ'
                }, {
                    border: [
                        false, true, false, true
                    ],
                    text: 'หน่วยที่ตก'
                }, {
                    border: [
                        false, true, false, true
                    ],
                    text: 'สถานภาพ'
                }, {
                    border: [
                        false, true, false, true
                    ],
                    text: 'กลุ่ม'
                }, {
                    border: [
                        false, true, false, true
                    ],
                    text: 'ความรู้'
                }, {
                    border: [
                        false, true, false, true
                    ],
                    text: 'คัดเลือกหน่วย'
                }
            ]
        ]
        //data.push(header);
        pdfdata.map((item, i) => {
            data.push([
                {
                    border: [false, false, false, false],
                    styles: { alignment: 'right' },
                    fillColor: item.hilight == 1 ? '#cccccc' : '',
                    noWrap: true,
                    text: i + 1
                }, {
                    border: [false, false, false, false],
                    fillColor: item.hilight == 1 ? '#cccccc' : '', noWrap: true,
                    text: item.belong == null ? "" : item.belong.toString()
                }, {
                    border: [false, false, false, false],
                    fillColor: item.hilight == 1 ? '#cccccc' : '',
                    noWrap: true,
                    text: item.id8 == null ? "" : item.id8.toString()
                }, {
                    border: [false, false, false, false],
                    fillColor: item.hilight == 1 ? '#cccccc' : '',
                    noWrap: true,
                    text: item.fullname == null ? "" : item.fullname.toString()
                }, {
                    border: [false, false, false, false],
                    fillColor: item.hilight == 1 ? '#cccccc' : '',
                    noWrap: true,
                    text: item.askby == null ? "" : item.refnumur == null ?
                        item.askby.toString() + " (" + item.num.toString() + ")" : (item.askby.toString() + " (" + item.num.toString() + ")").substring(41) != "" ? (item.askby.toString() + " (" + item.num.toString() + ")").substring(0, 38) + "..."
                            : item.askby.toString() + " (" + item.num.toString() + ")"
                }, {
                    border: [false, false, false, false],
                    fillColor: item.hilight == 1 ? '#cccccc' : '',
                    noWrap: true,
                    text: item.refnumur == null ? "" : item.refnumur.substring(12) != "" ? item.refnumur.substring(0, 9) + "..." : item.refnumur
                }, {
                    border: [
                        false, false, false, false
                    ],
                    fillColor: item.hilight == 1
                        ? '#cccccc'
                        : '',
                    noWrap: true,
                    text: item.remark2 == null
                        ? ""
                        : item
                            .remark2
                            .toString()
                }, {
                    border: [
                        false, false, false, false
                    ],
                    fillColor: item.hilight == 1
                        ? '#cccccc'
                        : '',
                    noWrap: true,
                    text: item.remark == null
                        ? ""
                        : item
                            .remark
                            .toString()
                }, {
                    border: [
                        false, false, false, false
                    ],
                    fillColor: item.hilight == 1
                        ? '#cccccc'
                        : '',
                    noWrap: true,
                    text: item.refnum3 == null
                        ? ""
                        : item
                            .refnum3
                            .substring(12) != ""
                            ? item
                                .refnum3
                                .substring(0, 9) + "..."
                            : item.refnum3
                }, {
                    border: [
                        false, false, false, false
                    ],
                    fillColor: item.hilight == 1
                        ? '#cccccc'
                        : '',
                    noWrap: true,
                    text: item.stitle == null
                        ? ""
                        : item
                            .stitle
                            .substring(13) != ""
                            ? item
                                .stitle
                                .substring(0, 10) + "..."
                            : item.stitle
                }, {
                    border: [
                        false, false, false, false
                    ],
                    fillColor: item.hilight == 1
                        ? '#cccccc'
                        : '',
                    noWrap: true,
                    text: item.addictive == null
                        ? ""
                        : item
                            .addictive
                            .toString()
                }, {
                    border: [
                        false, false, false, false
                    ],
                    fillColor: item.hilight == 1
                        ? '#cccccc'
                        : '',
                    noWrap: true,
                    text: item.educname == null
                        ? ""
                        : item.postname == null
                            ? item.educname
                            : item
                                .educname
                                .substring(21) != ""
                                ? item
                                    .educname
                                    .substring(0, 18) + "..."
                                : item.educname
                }, {
                    border: [
                        false, false, false, false
                    ],
                    fillColor: item.hilight == 1
                        ? '#cccccc'
                        : '',
                    noWrap: true,
                    text: item.postname == null
                        ? ""
                        : item
                            .postname
                            .toString() + "(" + item.item + ")"
                }
            ])
            j++;
        })

        var docDefinition = {
            content: [
                {

                    widths: '100%',
                    table: {
                        headerRows: 2,
                        widths: [
                            '2%',
                            '2%',
                            '4.5%',
                            '10%',
                            '20%',
                            '5%',
                            '10%',
                            '18%',
                            '5%',
                            '5%',
                            '2%',
                            '10%',
                            '7%'
                        ],
                        body: data
                    }
                }
            ],

            defaultStyle: {
                font: 'THSarabun',
                fontSize: 12
            },
            styles: {
                center: {
                    alignment: 'center'
                },
                tableExample: {
                    widths: '100%'
                },
                tableHeader: {
                    // bold: true, fontSize: 10,
                }
            },
            // a string or { width: number, height: number }
            pageSize: 'A4',

            // by default we use portrait, you can change it to landscape if you wish
            pageOrientation: 'landscape',

            // [left, top, right, bottom] or [horizontal, vertical] or just a number for
            // equal margins
            pageMargins: [5, 10, 5, 0]
        };
        pdfMake.fonts = {
            THSarabun: {
                normal: 'THSarabun.ttf',
                bold: 'THSarabun Bold.ttf',
                italics: 'THSarabun Italic.ttf',
                bolditalics: 'THSarabun BoldItalic.ttf'
            }
        }
        // pdfMake.createPdf(docDefinition).download();
        console.log(docDefinition)
        pdfMake.createPdf(docDefinition).download();

        // pdfMake.createPdf(docDefinition).print(); var doc = new jsPDF('L','mm','A4');
        // doc.addFont('THSarabun.ttf', 'THSarabun', 'normal'); doc.setFont("THSarabun")
        // doc.text(20, 50, 'Park Entry Ticketทดสอบ'); function
        // open_data_uri_window(url) {   var html = '<html>' +     '<style>html, body {
        // padding: 0; margin: 0; } iframe { width: 100%; height: 100%; border: 0;}
        // </style>' +     '<body>' +     '<iframe src="' + url + '"></iframe>' +
        // '</body></html>';   var open = window.open("","_blank")
        // open.document.write(html) } doc.save('custom_fonts.pdf');
        // open_data_uri_window(doc.output('datauri')); doc.text(20, 80, 'Address1: ' +
        // address1); doc.text(20, 100, 'Address2: ' + address2); doc.text(20, 120,
        // 'Entry Date & time: ' + entry_date_time); doc.text(20, 140, 'Expiry date &
        // time: ' + exit_date_time); doc.viewerPreferences({'FitWindow': true}, true)
        // doc.output('save', 'filename.pdf'); //Try to save PDF as a file (not works on
        // ie before 10, and some mobile devices) doc.output('datauristring');
        // //returns the data uri string doc.output('datauri');              //opens the
        // data uri in current window doc.output('dataurlnewwindow');     //opens the
        // data uri in new window doc.viewerPreferences({ 'HideWindowUI': true,
        // 'PrintArea': 'CropBox', 'NumCopies': 10 })
    }
    onAlertDismissed(alert) {
        const alerts = this.state.alerts;

        // find the index of the alert that was dismissed
        const idx = alerts.indexOf(alert);

        if (idx >= 0) {
            this.setState({
                // remove the alert from the array
                alerts: [
                    ...alerts.slice(0, idx),
                    ...alerts.slice(idx + 1)
                ]
            });
        }
    }
    clearAlerts() {
        this.setState({
            alerts: []
        });
    }
    generate(type, headline, message, timeout, headtype) {
        const newAlert = {
            id: (new Date()).getTime(),
            type: type,
            headline: `${headline}`,
            message: message
        };
        this.setState({ timeout: timeout })
        this.setState({ inputvalidate: headtype })
        this.setState({
            alerts: [
                ...this.state.alerts,
                newAlert
            ]
        });
    }
    componentDidMount() {
        if (this.state.unit != this.props.unittabData) {
            tmp = [];
            for (var i = 0; i < this.props.unittabData.length; i++) {
                tmp[i] = this.props.unittabData[i].label
            }
            this.setState({ unit: this.props.unittabData });
        }
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.tableData != tableData) {
            tableData = nextProps.tableData
            this.setState({ upddatedData: tableData })
            this.tableFilterNode1.reset(tableData, false);
            this.tableFilterNode2.reset(tableData, false);
            this.props.alert({ type: "", timeout: 1 })
        }
        if (this.state.unit != nextProps.unittabData) {
            tmp = [];
            for (var i = 0; i < nextProps.unittabData.length; i++) {
                tmp[i] = nextProps.unittabData[i].label
                tmp2[nextProps.unittabData[i].label] = nextProps.unittabData[i].label
            }
            this.setState({ unit: nextProps.unittabData });
        }
        if (nextProps.changeAllUnit3.hasOwnProperty('success') && query == 1) {

            query = 0;
            this.props.alert({ type: "", timeout: 1 })
            var navyidtmp = navyid.split(",")
            for (var i = 0; i < navyidtmp.length; i++) {
                for (var j = 0; j < tableData.length; j++) {
                    if (parseInt(navyidtmp[i]) == tableData[j].NAVYID) {
                        for (var k = 0; k < this.state.unit.length; k++) {
                            if (this.state.valueChangeAllUnit3 == this.state.unit[k].value) {
                                tableData[j].refnum3 = this.state.unit[k].label
                            }
                        }


                    }
                }
            }
            if (this.props.type == "education") {
                this.props.counteducation()
                this.props.countskill()
            }
        }
    }

    onAfterSaveCell(row, cellName, cellValue) {
        var navyid = row['NAVYID'];
        var refnum = '';
        if (cellName == "refnum3") {
            for (var i = 0; i < this.state.unit.length; i++) {
                if (cellValue == this.state.unit[i].label) {
                    refnum = this.state.unit[i].value
                }
            }
        }
        else {

            refnum = cellValue
            for (var i = 0; i < tableData.length; i++) {
                if (tableData[i].NAVYID == parseInt(row['NAVYID'])) {
                    tableData[i].refnum3 = this.state.unit[cellValue].label
                    tableData[i].u3 = ""
                }
            }

        }
        // console.log(this.state.unit)
        //this.setState({unit:nextProps.unittabData});
        if (row['askby'] != "" && row['askby'] != null) {
            cellValue = this.state.currentUnit;
        }
        else {
            //if ((row['askby'] == "" || row['askby'] == null) && (row['selectexam'] == "" || row['selectexam'] == null)) {
            this.props.fetchchangeunit3({ navyid: navyid, unit3: refnum, type: this.props.type })
            this.generate("success", "แจ้งเตือน", "เปลี่ยนหน่วยเสร็จสิ้น", 2000);
            // }
            // else {
            //     cellValue = this.state.currentUnit;
        }
        this.props.fetchHisrequest_request({})
        this.props.fetchHisrequest_incident({})
        this.props.fetchHisrequest_selectexam({})
        this.props.fetchselectexam2()
        this.props.fetchHisrequest_all({})

        if (this.props.hasOwnProperty('type')) {
            if (this.props.type == "Province") {
                this.props.fetchAmount({ armid: "32,25,45" })
            }
            if (this.props.type == "selectexam") {
            }
            if (this.props.type == "education") {
                this.props.counteducation()
                this.props.countskill()
            }
        }

    }
    onBeforeSaveCell(row, cellName, cellValue) {
        if (cellName == "u3") {
            if (cellValue != "") {
                const nan = isNaN(parseInt(cellValue, 10))
                if (nan) {
                    alert(nan)
                    return false
                }
                else if (parseInt(cellValue, 10) >= 0 && parseInt(cellValue, 10) <= 30) {
                    if ((row['askby'] == "" || row['askby'] == null) && (row['selectexam'] == "" || row['selectexam'] == null) && row['belong'] != "0/6") {
                        //return true;
                    }
                    else if (row['askby'] != "" && row['askby'] != null) {
                        this.generate("warning", "แจ้งเตือน", "มีการร้องขอ \"" + row['askby'] + "\"", 0)
                        //return true
                    } else if (row['belong'] == "0/6") {
                        this.generate("warning", "แจ้งเตือน", "สถานะ\"" + row['title'] + "\"", 0)
                        //return true;
                    } else {
                        this.generate("warning", "แจ้งเตือน", "พลฯ " + row['fullname'] + " ได้รับการคัดเลือก \"" + row['selectexam'] + "\"", 0)
                        //return true;
                    }
                }
                else {
                    return false
                }
            }
            else {
                return false
            }
        }
        else {
            if (row['refnum3'] != cellValue) {
                this.setState({ currentUnit: cellValue })
                // You can do any validation on here for editing value,
                // return false for reject the editing
                if ((row['askby'] == "" || row['askby'] == null) && (row['selectexam'] == "" || row['selectexam'] == null) && row['belong'] != "0/6") {
                    return true;
                }
                else if (row['askby'] != "" && row['askby'] != null) {
                    this.generate("warning", "แจ้งเตือน", "มีการร้องขอ \"" + row['askby'] + "\"", 0)
                    return true
                } else if (row['belong'] == "0/6") {
                    this.generate("warning", "แจ้งเตือน", "สถานะ\"" + row['title'] + "\"", 0)
                    return true;
                } else {
                    this.generate("warning", "แจ้งเตือน", "พลฯ " + row['fullname'] + " ได้รับการคัดเลือก \"" + row['selectexam'] + "\"", 0)
                    return true;
                }
            }
            else {
                return false
            }
        }
    }
    tablemodal(navyid) {
        this.props.modal({ showModal: true, requesttab: 2, selectexamtab: 2, incidenttab: 2, providetab: 2, changetab: 2 })
        this.props.fetchModalData({ navyid: navyid })
    }
    close() {
        this.props.modal({ showModal: false })
        if (this.props.hasOwnProperty('type')) {
            if (this.props.type == "Province") {
                this.props.func()
            }
        }
    }
    buttonFormatter2(cell, row) {
        return <a href="#" onClick={() => this.tablemodal(cell)}><FaSearch /></a>
    }
    unit3Formatter(a, b, order) {   // order is desc or asc

        console.log(parseInt(a.unit3) + " " + b.unit3 + " " + order)
        if (order === 'desc') {
            if (parseInt(a.unit3) > parseInt(b.unit3)) {
                return -1;
            } else if (parseInt(a.unit3) < parseInt(b.unit3)) {
                return 1;
            }
            return 0;
        }
        if (parseInt(a.unit3) < parseInt(b.unit3)) {
            return -1;
        } else if (parseInt(a.unit3) > parseInt(b.unit3)) {
            return 1;
        }
        return 0;
    }
    unit4Formatter(a, b, order) {   // order is desc or asc

        console.log(parseInt(a.unit4) + " " + b.unit4 + " " + order)
        if (order === 'desc') {
            if (parseInt(a.unit4) > parseInt(b.unit4)) {
                return -1;
            } else if (parseInt(a.unit4) < parseInt(b.unit4)) {
                return 1;
            }
            return 0;
        }
        if (parseInt(a.unit4) < parseInt(b.unit4)) {
            return -1;
        } else if (parseInt(a.unit4) > parseInt(b.unit4)) {
            return 1;
        }
        return 0;
    }
    unit1Formatter(a, b, order) {   // order is desc or asc

        console.log(parseInt(a.unit1) + " " + b.unit1 + " " + order)
        if (order === 'desc') {
            if (parseInt(a.unit1) > parseInt(b.unit1)) {
                return -1;
            } else if (parseInt(a.unit1) < parseInt(b.unit1)) {
                return 1;
            }
            return 0;
        }
        if (parseInt(a.unit1) < parseInt(b.unit1)) {
            return -1;
        } else if (parseInt(a.unit1) > parseInt(b.unit1)) {
            return 1;
        }
        return 0;
    }
    unit2Formatter(a, b, order) {   // order is desc or asc

        console.log(parseInt(a.unit2) + " " + b.unit2 + " " + order)
        if (order === 'desc') {
            if (parseInt(a.unit2) > parseInt(b.unit2)) {
                return -1;
            } else if (parseInt(a.unit2) < parseInt(b.unit2)) {
                return 1;
            }
            return 0;
        }
        if (parseInt(a.unit2) < parseInt(b.unit2)) {
            return -1;
        } else if (parseInt(a.unit2) > parseInt(b.unit2)) {
            return 1;
        }
        return 0;
    }
    unit0Formatter(a, b, order) {   // order is desc or asc

        console.log(parseInt(a.unit0) + " " + b.unit0 + " " + order)
        if (order === 'desc') {
            if (parseInt(a.unit0) > parseInt(b.unit0)) {
                return -1;
            } else if (parseInt(a.unit0) < parseInt(b.unit0)) {
                return 1;
            }
            return 0;
        }
        if (parseInt(a.unit0) < parseInt(b.unit0)) {
            return -1;
        } else if (parseInt(a.unit0) > parseInt(b.unit0)) {
            return 1;
        }
        return 0;
    }
    afterColumnFilter(filterConds, result) {
        navyid = ""
        navyid2 = ""
        var length
        pdfdata = result
        navyid2 = ""
        for (let i = 0; i < result.length; i++) {
            if (result[i].refnum3 != "ไม่ได้ระบุ") {
                navyid2 = "กรุณากรองเฉพาะหน่วยปัจจุบันที่เท่ากับ \"ไม่ได้ระบุ\""
                return
            }
        }
        if (this.state.valueLimit != "") {
            if (this.state.valueLimit <= result.length) {
                length = this.state.valueLimit
            }
            else {
                length = result.length
            }
        }
        else {
            length = result.length
        }
        for (let i = 0; i < length; i++) {
            if (navyid == "") {
                navyid += result[i].NAVYID
            }
            else {
                navyid += "," + result[i].NAVYID
            }
            // console.log('Product: ' + result[i].NAVYID + ', ' + result[i].refnum1);
        }
    }
    afterSearch(searchText, result) {
        navyid = ""

        var length
        pdfdata = result
        navyid2 = ""
        for (let i = 0; i < result.length; i++) {
            if (result[i].refnum3 != "ไม่ได้ระบุ") {
                navyid2 = "กรุณากรองเฉพาะหน่วยปัจจุบันที่เท่ากับ \"ไม่ได้ระบุ\""
                return
            }
        }
        if (this.state.valueLimit != "") {
            if (this.state.valueLimit <= result.length) {
                length = this.state.valueLimit
            }
            else {
                length = result.length
            }
        }
        else {
            length = result.length
        }
        for (let i = 0; i < length; i++) {
            if (navyid == "") {
                navyid += result[i].NAVYID
            }
            else {
                navyid += "," + result[i].NAVYID
            }
            //console.log('Product: ' + result[i].NAVYID + ', ' + result[i].refnum1);
        }
    }
    trClassFormat(row, rowIndex) {
        return row.addictive > 2 ? "danger" : ""
    }
    handleLimit(event) {
        this.setState({ valueLimit: event.target.value })
    }
    logChangeAllUnit3(val) {
        if (val != null) {
            this.setState({ valueChangeAllUnit3: val.value })
        }
        else {
            this.setState({ valueChangeAllUnit3: "" })
        }
    }
    changeAllUnit3() {
        if (navyid2 != "") {
            this.props.alert({ type: "danger", headline: "ผิดพลาด", message: navyid2, timeout: 2000, position: "top-right" })
            return false
        }
        if (this.state.valueChangeAllUnit3 == null || typeof this.state.valueChangeAllUnit3 == 'string') {
            this.props.alert({ type: "danger", headline: "ผิดพลาด", message: "ระบุหน่วยจัดลง", timeout: 2000, position: "top-right" })
            return false
        }
        if (navyid == "") {
            this.props.alert({ type: "danger", headline: "ผิดพลาด", message: "ไม่พบรายชื่อที่จะจัดลงหน่วย", timeout: 2000, position: "top-right" })
            return false
        }
        else {

            this.props.fetchChangeAllUnit3({ navyid: navyid, unit3: this.state.valueChangeAllUnit3, type: this.props.type })
            this.props.alert({ type: "success", headline: "กำลังโหลด...", message: < div className="loader" > </div>, timeout: 0, position: "top-right" })
            this.props.fetchHisrequest_request({})
            this.props.fetchHisrequest_incident({})
            this.props.fetchHisrequest_selectexam({})
            this.props.fetchselectexam2()
            this.props.fetchHisrequest_province({ armid: "25,32,45" })
            this.props.fetchHisrequest_all({})
            query = 1
        }
    }
    unit3validate(value, row) {
        alert(row['NAVYID'] + " " + value)
        return true
    }
    renderPaginationShowsTotal(start, to, total) {
        //this.setState({ start: start, to: to, total: total })
        return (
            <p >
                จาก {start} ถึง {to} ทั้งหมด {total}
            </p>
        );
    }
    filterUpdated1(newData, filterConfiguration) {
        this.setState({ upddatedData: newData });
        tableData = newData;
            this.tableFilterNode2.reset(tableData, false);
    }
    filterUpdated2(newData, filterConfiguration) {
        this.setState({ upddatedData: newData });
        tableData = newData;
        
        this.tableFilterNode1.reset(tableData, false);
    }
    render() {
        const options = {
            btnGroup: this.createCustomButtonGroup,
            paginationShowsTotal: this.renderPaginationShowsTotal,  // custom,
            afterColumnFilter: this.afterColumnFilter,
            afterSearch: this.afterSearch,
        };
        const keyedit = {
            enterToEdit: true
        }
        return (
            <div>
                <AlertList
                    position={this.state.position}
                    alerts={this.state.alerts}
                    timeout={this.state.timeout}
                    dismissTitle="Begone!"
                    onDismiss={this.onAlertDismissed.bind(this)} />
                <Row>
                    <Col sm={2}>
                        <ControlLabel>จัดลงหน่วย</ControlLabel>
                    </Col>
                    <Col sm={6}>
                        <Select
                            placeholder="หน่วยลง"
                            name="ChangeUnit"
                            clearable={false}
                            value={this.state.valueChangeAllUnit3}
                            options={this.props.unittabData}
                            onChange={this.logChangeAllUnit3}
                            ref="ChangeUnit" />
                    </Col>
                    <Col sm={3}>
                        <FormControl
                            type="number"
                            placeholder="จำนวน"
                            onKeyPress={this.keypressLimit}
                            onChange={this.handleLimit}
                            name="limit" />
                    </Col>
                    <Col sm={1}>
                        <Button bsStyle="primary" onClick={this.changeAllUnit3}>
                            บันทึก
              </Button>
                    </Col>
                </Row>
                <br />
                <Row>
                    <Col sm={12}>
                        <Table bordered>
                            <thead>
                            <TableFilter
                                onFilterUpdate={this.filterUpdated1}
                                initialFilters={this.state.filterConfiguration}
                                ref={(node) => { this.tableFilterNode1 = node; }}>
                                <th filterkey="addictive">กลุ่ม</th>
                                <th filterkey="refnum1" >สมัครใจ1</th>
                                <th filterkey="refnum2">สมัครใจ2</th>
                                <th filterkey="postname">คัดเลือก</th>
                                <th filterkey="refnum4">หน่วย</th>
                                <th filterkey="item">ลำดับ</th>
                                <th filterkey="religion">ศาสนา</th>
                                <th filterkey="refnum3">หน่วยที่ตก</th>
                                <th filterkey="refnumur">หน่วยขอ</th>
                                <th filterkey="askby">ผู้ขอ</th>
                            </TableFilter>
                            <TableFilter
                                onFilterUpdate={this.filterUpdated2}
                                initialFilters={this.state.filterConfiguration}
                                ref={(node) => { this.tableFilterNode2 = node; }}
                            >
                                <th filterkey="num">ลำดับ</th>
                                <th filterkey="remark">หมายเหตุ</th>
                                <th filterkey="remark2">ขอต่อ</th>
                                <th filterkey="title">สถานะ</th>
                                <th filterkey="batt">พัน</th>
                                <th filterkey="company">ร้อย</th>
                                <th filterkey="yearin">ผลัด</th>
                                <th filterkey="educname">การศึกษา</th>
                                <th filterkey="skill">ความรู้</th>
                                <th filterkey="distance">ว่ายน้ำ</th>
                            </TableFilter>
                            </thead>
                        </Table>
                        <br />
                        <BootstrapTable ref='table'
                            data={tableData} trClassName={this.trClassFormat} cellEdit={this.state.cellEditProp} pagination exportCSV options={options} search keyBoardNav={keyedit}>
                            <TableHeaderColumn dataField='id8' width='90' editable={false} dataSort={true} filter={{ type: 'RegexFilter' }}>ทะเบียน</TableHeaderColumn>
                            <TableHeaderColumn dataField='percent' width='60' editable={false} dataSort={true} filter={{ type: 'RegexFilter' }}>สอบ</TableHeaderColumn>
                            <TableHeaderColumn dataField='addictive' width='40' editable={false} dataSort={true} filter={{ type: 'RegexFilter' }}>กลุ่ม</TableHeaderColumn>
                            <TableHeaderColumn dataField='refnum1' width='80' editable={false} filter={{ type: 'RegexFilter' }} dataSort={true} sortFunc={this.unit1Formatter}>สมัครใจ1</TableHeaderColumn>
                            <TableHeaderColumn dataField='refnum2' width='80' editable={false} filter={{ type: 'RegexFilter' }} dataSort={true} sortFunc={this.unit2Formatter}>สมัครใจ2</TableHeaderColumn>

                            <TableHeaderColumn dataField='postname' width='90' editable={false} dataSort={true} filter={{ type: 'RegexFilter' }}>คัดเลือก</TableHeaderColumn>
                            <TableHeaderColumn dataField='refnum4' width='70' editable={false} dataSort={true} filter={{ type: 'RegexFilter' }} sortFunc={this.unit4Formatter}>หน่วย</TableHeaderColumn>
                            <TableHeaderColumn dataField='item' width='50' editable={false} dataSort={true} filter={{ type: 'RegexFilter' }}>ลำดับ</TableHeaderColumn>

                            <TableHeaderColumn dataField='religion' width='70' editable={false} dataSort={true} filter={{ type: 'RegexFilter' }}>ศาสนา</TableHeaderColumn>
                            <TableHeaderColumn dataField='u3' width='70' dataSort={true} tdStyle={{ backgroundColor: '#f39c12' }} filter={{ type: 'RegexFilter' }} thStyle={{ backgroundColor: '#f39c12' }} editable={true}>UNIT3</TableHeaderColumn>
                            <TableHeaderColumn dataField='refnum3' width='90' dataSort={true} tdStyle={{ backgroundColor: '#f39c12' }} filter={{ type: 'RegexFilter' }} thStyle={{ backgroundColor: '#f39c12' }} editable={{ type: 'select', options: { values: tmp } }} sortFunc={this.unit3Formatter}>ตกหน่วย</TableHeaderColumn>
                            <TableHeaderColumn dataField='refnumur' width='80' editable={false} dataSort={true} filter={{ type: 'RegexFilter' }} sortFunc={this.unit0Formatter}>ขอลง</TableHeaderColumn>
                            <TableHeaderColumn dataField='askby' width='220' editable={false} dataSort={true} filter={{ type: 'RegexFilter' }}>ผู้ขอ</TableHeaderColumn>
                            <TableHeaderColumn dataField='num' width='50' editable={false} dataSort={true} filter={{ type: 'RegexFilter' }}>ลำดับ</TableHeaderColumn>
                            <TableHeaderColumn dataField='remark' width='120' editable={false} dataSort={true} filter={{ type: 'RegexFilter' }}>หมายเหตุ</TableHeaderColumn>
                            <TableHeaderColumn dataField='remark2' width='50' editable={false} dataSort={true} filter={{ type: 'RegexFilter' }}>ขอต่อ</TableHeaderColumn>

                            <TableHeaderColumn dataField='title' width='70' editable={false} dataSort={true} filter={{ type: 'RegexFilter' }} >สถานะ</TableHeaderColumn>
                            <TableHeaderColumn dataField='batt' width='50' editable={false} dataSort={true} filter={{ type: 'RegexFilter' }}>พ</TableHeaderColumn>
                            <TableHeaderColumn dataField='company' width='50' editable={false} dataSort={true} filter={{ type: 'RegexFilter' }}>ร</TableHeaderColumn>
                            <TableHeaderColumn dataField='yearin' width='90' editable={false} dataSort={true} filter={{ type: 'RegexFilter' }}>ผลัดฝึก</TableHeaderColumn>
                            <TableHeaderColumn dataField='oldyearin' width='90' editable={false} dataSort={true} filter={{ type: 'RegexFilter' }}>ผลัดจริง</TableHeaderColumn>
                            <TableHeaderColumn dataField='fullname' editable={false} width='170' dataSort={true} filter={{ type: 'RegexFilter' }}>ชื่อ - นามสกุล</TableHeaderColumn>
                            <TableHeaderColumn dataField='id8' width='100' editable={false} dataSort={true} filter={{ type: 'RegexFilter' }}>ทะเบียน</TableHeaderColumn>
                            <TableHeaderColumn dataField='educname' width='170' editable={false} dataSort={true} filter={{ type: 'RegexFilter' }}>การศึกษา</TableHeaderColumn>

                            <TableHeaderColumn dataField='skill' width='120' editable={false} dataSort={true} filter={{ type: 'RegexFilter' }}>ความรู้</TableHeaderColumn>

                            <TableHeaderColumn dataField='HEIGHT' width='110' editable={false} dataSort={true} filter={{ type: 'RegexFilter' }}>ความสูง</TableHeaderColumn>
                            <TableHeaderColumn dataField='distance' width='100' editable={false} dataSort={true} filter={{ type: 'RegexFilter' }}>ว่ายน้ำ</TableHeaderColumn>
                            {/* <TableHeaderColumn dataField='title' width='100' editable={false} dataSort={true} filter={{ type: 'RegexFilter' }}>สถานะ</TableHeaderColumn> */}
                            <TableHeaderColumn
                                dataField="NAVYID"
                                dataFormat={this.buttonFormatter2} width='50' editable={false} ></TableHeaderColumn>
                            <TableHeaderColumn
                                dataField="NAVYID"
                                isKey={true}
                                hidden={true}></TableHeaderColumn>
                        </BootstrapTable>
                    </Col>
                </Row>
            </div>
        );
    }
}
function mapStatetoProps(state) {
    return {
        unittabData: state.unittabData,
        changeUnit3: state.changeUnit3,
        modalData: state.modalData,
        view: state.view,
        tableData: state.tableData,
        amountData: state.amountData,
        changeAllUnit3: state.changeAllUnit3,
        Alerts: state.Alerts,
        typeOpenModal: state.typeOpenModal

    }
}

function mapDispatchToProps(dispatch) {
    return {

        countskill: () => dispatch({ type: "FETCH_COUNTSKILL_DATA" }),
        counteducation: () => dispatch({ type: "FETCH_COUNTEDUCATION_DATA" }),
        fetchHisrequest_request: navyid => dispatch({ type: "FETCH_HISREQUESTREQUEST_DATA" }),
        fetchHisrequest_incident: navyid => dispatch({ type: "FETCH_HISREQUESTINCIDENT_DATA" }),
        fetchHisrequest_selectexam: navyid => dispatch({ type: "FETCH_HISREQUESTSELECTEXAM_DATA" }),
        fetchselectexam2: () => dispatch({ type: "FETCH_HISREQUESTSELECTEXAM2_DATA" }),
        fetchHisrequest_province: armid => dispatch({ type: "FETCH_HISREQUESTPROVINCE_DATA", payload: armid }),
        fetchHisrequest_all: navyid => dispatch({ type: "FETCH_HISREQUESTALL_DATA" }),
        fetchUnittab: navyid => dispatch({ type: "FETCH_UNITTAB_DATA" }),
        fetchchangeunit3: navyid => dispatch({ type: "FETCH_CHANGEUNIT3_DATA", payload: navyid }),
        fetchchangeunit3: unit3 => dispatch({ type: "FETCH_CHANGEUNIT3_DATA", payload: unit3 }),
        fetchchangeunit3: type => dispatch({ type: "FETCH_CHANGEUNIT3_DATA", payload: type }),
        modal: showModal => dispatch({ type: "CHANGE_VIEW_DATA", payload: showModal }),
        modal: requesttab => dispatch({ type: "CHANGE_VIEW_DATA", payload: requesttab }),
        modal: selectexamtab => dispatch({ type: "CHANGE_VIEW_DATA", payload: selectexamtab }),
        modal: incidenttab => dispatch({ type: "CHANGE_VIEW_DATA", payload: incidenttab }),
        modal: providetab => dispatch({ type: "CHANGE_VIEW_DATA", payload: providetab }),
        modal: changetab => dispatch({ type: "CHANGE_VIEW_DATA", payload: changetab }),
        fetchModalData: navyid => dispatch({ type: "FETCH_MODAL_DATA", payload: navyid }),
        fetchAmount: armid => dispatch({ type: "FETCH_AMOUNT_DATA", payload: armid }),
        fetchChangeAllUnit3: navyid => dispatch({ type: "FETCH_CHANGEALLUNIT3_DATA", payload: navyid }),
        fetchChangeAllUnit3: unit3 => dispatch({ type: "FETCH_CHANGEALLUNIT3_DATA", payload: unit3 }),
        fetchChangeAllUnit3: type => dispatch({ type: "FETCH_CHANGEALLUNIT3_DATA", payload: type }),
        fetchData: where => dispatch({ type: "FETCH_TABLE_DATA", payload: where }),
        fetchData: sort => dispatch({ type: "FETCH_TABLE_DATA", payload: sort }),
        fetchData: groupby => dispatch({ type: "FETCH_TABLE_DATA", payload: groupby }),
        fetchData: have => dispatch({ type: "FETCH_TABLE_DATA", payload: have }),
        fetchData: name => dispatch({ type: "FETCH_TABLE_DATA", payload: name }),
        fetchData: sname => dispatch({ type: "FETCH_TABLE_DATA", payload: sname }),
        fetchData: id8 => dispatch({ type: "FETCH_TABLE_DATA", payload: id8 }),
        alert: type => dispatch({ type: "ALERTS", payload: type }),
        alert: headline => dispatch({ type: "ALERTS", payload: headline }),
        alert: message => dispatch({ type: "ALERTS", payload: message }),
        alert: timeout => dispatch({ type: "ALERTS", payload: timeout }),
        alert: position => dispatch({ type: "ALERTS", payload: position }),
    }
}

const Connected_Table_changeUnit3 = connect(mapStatetoProps, mapDispatchToProps)(Table_changeUnit3)

export default Connected_Table_changeUnit3