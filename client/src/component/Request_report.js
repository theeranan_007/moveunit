import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import {
  Panel,
  Col,
  Form,
  Button,
  FormGroup,
  FormControl,
  ControlLabel,
  Row,
  Table,
  Grid
} from 'react-bootstrap'
// import Switch from 'react-toggle-switch'
import Select from 'react-select';
import FaSearch from 'react-icons/lib/fa/search'
import ModalDetail from '../component/ModalDetail'
import { BootstrapTable, TableHeaderColumn, ButtonGroup } from 'react-bootstrap-table';
import { connect } from 'react-redux'
import FontAwesome from 'react-fontawesome'
import pdfMake from "pdfmake/build/pdfmake";
import pdfFonts from "pdfmake/build/vfs_fonts";
import Requestfrom from '../component/Requestfrom'
import { Switch, Spin,Icon } from 'antd';
pdfMake.vfs = pdfFonts.pdfMake.vfs;
var switchrepeat = false,switchrepeat_2 = false,
  switchunitrequest = false,
  switchsort = false,switchlockselectcode = true;
var unit = 0, requesttab = 1, check = 0;
var requestdata = [], state = 0;
var navyid = "", navyid2 = "", pdfdata = []
const spinicon = <Icon type="loading" style={{ fontSize: 24 }} spin />;
class Request_report extends Component {
  constructor(props) {
    super(props)
    this.state = {
      url: '/example.pdf',
      pages: null,
      loaded: 0,
      total: 0,
      error: null,
      name: "",
      sname: "",
      id8: "",
      position: "top-right",
      data: [],
      timeout: 0,
      numPages: null,
      pageNumber: 1,
      newMessage: "This is a test of the Emergency Broadcast System. This is only a test.",
      inputvalidate: null,
      tmp: 0,
      alerts: [],
      firstnavyid: "",
    }
    this.handlevaluefilter = this.handlevaluefilter.bind(this)
    this.keypressvaluefilter = this.keypressvaluefilter.bind(this)
    this.onPrint = this.onPrint.bind(this)
    this.submit = this.submit.bind(this)
    this.logChangeCategory = this
      .logChangeCategory
      .bind(this)
    this.logChangOperator = this
      .logChangOperator
      .bind(this)
    this.query = this
      .query
      .bind(this)
    this.clear_strwhere = this
      .clear_strwhere
      .bind(this)
    this.clear_strgroup = this
      .clear_strgroup
      .bind(this)
    this.clear_strsort = this
      .clear_strsort
      .bind(this)
    this.logChangvaluefilter = this
      .logChangvaluefilter
      .bind(this)
    this.modal = this.modal.bind(this)
    this.close = this.close.bind(this)
    this.togglerepeat = this.togglerepeat.bind(this)
    this.togglerepeat_2 = this.togglerepeat_2.bind(this)
    this.toggleunitrequest = this.toggleunitrequest.bind(this)
    this.filter = this.filter.bind(this)
    this.togglesort = this.togglesort.bind(this)
    this.createCustomButtonGroup = this.createCustomButtonGroup.bind(this)
    this.renderPaginationShowsTotal = this.renderPaginationShowsTotal.bind(this)
    this.afterColumnFilter = this.afterColumnFilter.bind(this)
    this.afterSearch = this.afterSearch.bind(this)
    this.buttonFormatter = this.buttonFormatter.bind(this)
    this.togglelockselectcode = this.togglelockselectcode.bind(this)
  }
  buttonFormatter(cell, row) {
    return <a onClick={() => this.modal(cell)}><FontAwesome name="search" /></a>
  }
  createCustomButtonGroup(props) {
    return (
      <ButtonGroup className='my-custom-class' sizeClass='btn-group-md'>
        {props.showSelectedOnlyBtn}
        {props.exportCSVBtn}
        {props.insertBtn}
        {props.deleteBtn}
        <button type='button'
          className={`btn btn-primary`}
          onClick={this.onPrint}>
          <FontAwesome name="download" />
          &nbsp;Export to PDF
        </button>
      </ButtonGroup>
    );
  }
  renderPaginationShowsTotal(start, to, total) {
    //this.setState({ start: start, to: to, total: total })
    return (
      <p >
        จาก {start} ถึง {to} ทั้งหมด {total}
      </p>
    );
  }
  afterColumnFilter(filterConds, result) {
    navyid = ""
    navyid2 = ""
    var length
    pdfdata = result
    navyid2 = ""
    for (let i = 0; i < result.length; i++) {
      if (result[i].refnum3 != "ไม่ได้ระบุ") {
        navyid2 = "กรุณากรองเฉพาะหน่วยปัจจุบันที่เท่ากับ \"ไม่ได้ระบุ\""
        return
      }
    }
    if (this.state.valueLimit != "") {
      if (this.state.valueLimit <= result.length) {
        length = this.state.valueLimit
      }
      else {
        length = result.length
      }
    }
    else {
      length = result.length
    }
    for (let i = 0; i < length; i++) {
      if (navyid == "") {
        navyid += result[i].NAVYID
      }
      else {
        navyid += "," + result[i].NAVYID
      }
    }
  }
  afterSearch(searchText, result) {
    navyid = ""

    var length
    pdfdata = result
    navyid2 = ""
    for (let i = 0; i < result.length; i++) {
      if (result[i].refnum3 != "ไม่ได้ระบุ") {
        navyid2 = "กรุณากรองเฉพาะหน่วยปัจจุบันที่เท่ากับ \"ไม่ได้ระบุ\""
        return
      }
    }
    if (this.state.valueLimit != "") {
      if (this.state.valueLimit <= result.length) {
        length = this.state.valueLimit
      }
      else {
        length = result.length
      }
    }
    else {
      length = result.length
    }
    for (let i = 0; i < length; i++) {
      if (navyid == "") {
        navyid += result[i].NAVYID
      }
      else {
        navyid += "," + result[i].NAVYID
      }
      //console.log('Product: ' + result[i].NAVYID + ', ' + result[i].refnum1);
    }
  }
  filter() {
    var where = "";
    var sort = "";
    var groupby = "";
    var have = "";
    if (this.state.alerts.length != 1)
      if (!switchrepeat) {
        where += " and r.selectcode >= 1 "
      }
    if (switchunitrequest) {
      where += " and p.unit0 != p.unit3"
    }
    // if(switchlockselectcode){
    //   where += " and r.lockselectcode = 0"
    // }
    // else{
    //   where += " and r.lockselectcode = 1"
    // }
    if (!switchsort) {
      sort = "CONVERT(r.unit,INTEGER),"
    }
    if(switchrepeat_2){
      groupby = "p.navyid";
      have = "> 1";
    }
    else{
      groupby = "";
      have = "";
    }
    switch (state) {
      case 1:
        if (unit == 31) {
          this.props.fetchRequestReport({ where: where.substring(4), groupby: groupby, have: have, sort: sort + "CONVERT(substring(r.askby,1,3),INTEGER),r.num" })
        }
        else {
          this.props.fetchRequestReport({ where: " r.unit = " + unit + where, groupby: groupby, have: have, sort: "CONVERT(substring(r.askby,1,3),INTEGER),r.num" })
        }
        break;
      case 2:
        if (unit == 31) {
          this.props.fetchRequestReport({ where: "r.unit = p.unit3 " + where, groupby: groupby, have: have, sort: sort + "CONVERT(substring(r.askby,1,3),INTEGER),r.num" })
        }
        else {
          this.props.fetchRequestReport({ where: " r.unit = p.unit3 and r.unit=" + unit + where, groupby: groupby, have: have, sort: "CONVERT(substring(r.askby,1,3),INTEGER),r.num" })
        }
        break;

      default:
        if (unit == 31) {
          this.props.fetchRequestReport({ where: "r.unit != p.unit3 " + where, groupby: groupby, have: have, sort: sort + "CONVERT(substring(r.askby,1,3),INTEGER),r.num" })
        }
        else {
          this.props.fetchRequestReport({ where: " r.unit != p.unit3 and r.unit = " + unit + where, groupby: groupby, have: have, sort: "CONVERT(substring(r.askby,1,3),INTEGER),r.num" })
        }
        break;
    }
    this.props.alert({ type: "success", headline: "กำลังโหลด...", message: <Spin indicator={spinicon}/>, timeout: 0, position: "top-right" })

  }
  togglesort() {
    switchsort = !switchsort
    this.filter()
  }
  toggleunitrequest() {
    switchunitrequest = !switchunitrequest
    this.filter()
  }
  togglerepeat() {
    switchrepeat = !switchrepeat
    this.filter()
  }
  togglerepeat_2() {
    switchrepeat_2 = !switchrepeat_2;
    this.filter()
  }
  togglelockselectcode(){
    switchlockselectcode = !switchlockselectcode
    this.filter();
  }
  clear_strwhere() {
    this.setState({ strwhere: "" })
  }
  clear_strgroup() {
    this.setState({ strgroupby: "" })
    this.setState({ strhaving: "" })
  }
  clear_strsort() {
    this.setState({ strorderby: "" })
  }
  handlevaluefilter(event) {
    this.setState({ valuefilter: event.target.value })
  }
  keypressvaluefilter(event) {

    if (event.key == "Enter") {
      //alert(this.state.valuefilter)
    }
  }
  logChangvaluefilter(val) {
    this.setState({ valuefilter: val.value })
  }
  logChangOperator(val) {
    this.setState({ valueOperator: val.value })
  }
  logChangeCategory(val) {

  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.requestreportData != requestdata) {
      this.setState({ data: nextProps.requestreportData })
      requestdata = nextProps.requestreportData
      this.props.alert({ type: "", timeout: 1 })
    }

    if (nextProps.view.state >= 1) {
      unit = nextProps.view.unit
      state = nextProps.view.state
      this.filter()
      this.props.requesttab({ state: 0, requesttab: 2, unit: nextProps.view.unit })
      // this.props.alert({ type: "success", headline: "กำลังโหลด...", message: <Spin indicator={spinicon}/>, timeout: 0, position: "top-right" })
    }
  }
  componentDidMount() {
    this
      .props
      .fetchPositiontab()
    this.props.fetchStatus()
    var tmp = []
    for (var i = 1; i <= 20; i++) {
      tmp[i - 1] = {
        value: i < 10
          ? "0" + i.toString()
          : i.toString(),
        label: i < 10
          ? "0" + i
          : i
      }

    }
    this.setState({ optionaskcode: tmp });

  }
  query() {
  this.props.alert({ type: "success", headline: "กำลังโหลด...", message: <Spin indicator={spinicon}/>, timeout: 0, position: "top-right" })
    this.props.fetchRequestReport({ where: this.state.strwhere, have: this.state.strhaving, sort: this.state.strorderby })
  }
  modal(navyid) {
    this.props.modal({ showModal: true })
    this.props.fetchModalData({ navyid: navyid })
  }
  close() {
    this.props.modal({ showModal: false })
  }
  submit(event) {
    event.preventDefault();
  }

  onPrint() {
    // const { vehicleData } = this.props.parkedVehicle; const { plate_no, max_time,
    //   entry_date_time,   exit_date_time,   expiry_time,   address1, address2,
    // city,   state,   zip,   country,   parking_status } = vehicleData; var
    // converter = new pdfConverter(); var doc = converter.jsPDF('p', 'pt');
    if (pdfdata.length == 0) {
      this.props.alert({ type: "danger", headline: "ผิดพลาด", message: "ไม่พบรายชื่อที่จะออกรายงาน", timeout: 2000, position: "top-right" })
      return false
    }
    var data = [],
      contstr = [],
      j = 0
    var data = [
      [
        {
          colSpan: 14,
          border: [
            false, false, false, false
          ],
          text: 'รายชื่อขอทหาร เพื่อพิจารณาจัดแบ่ง ผลัด ' + pdfdata[0].yearin,
          alignment: 'center'
        },
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        ""
      ],
      [
        {
          border: [
            false, true, false, true
          ],
          text: 'ที่   '
        }, {
          border: [
            false, true, false, true
          ],
          text: '#'
        }, {
          border: [
            false, true, false, true
          ],
          text: 'ร/พ'
        }, {
          border: [
            false, true, false, true
          ],
          text: 'ทะเบียน'
        }, {
          border: [
            false, true, false, true
          ],
          text: 'ชื่อ - สกุล'
        }, {
          border: [
            false, true, false, true
          ],
          text: 'ผู้ขอ'
        }, {
          border: [
            false, true, false, true
          ],
          text: 'หน่วยที่ขอ'
        }, {
          border: [
            false, true, false, true
          ],
          text: 'ขอต่อ'
        }, {
          border: [
            false, true, false, true
          ],
          text: 'หมายเหตุ'
        }, {
          border: [
            false, true, false, true
          ],
          text: 'หน่วยที่ตก'
        }, {
          border: [
            false, true, false, true
          ],
          text: 'สถานภาพ'
        }, {
          border: [
            false, true, false, true
          ],
          text: 'กลุ่ม'
        }, {
          border: [
            false, true, false, true
          ],
          text: 'ความรู้'
        }, {
          border: [
            false, true, false, true
          ],
          text: 'คัดเลือกหน่วย'
        }
      ]
    ]
    //data.push(header);
    var count = 0, count2 = 0, oldaskcode = "", oldunit = ""
    requestdata.map((item, i) => {
      count++
      count2++
      if (switchsort) {
        if (oldaskcode != item.askcode && count == 1) {
          oldaskcode = item.askcode
        }
        else if (oldaskcode != item.askcode) {
          oldaskcode = item.askcode
          for (var i = count2; i < 31; i++) {
            data.push([{
              border: [false, false, false, false],
              styles: { alignment: 'right' },
              color: '#fff',
              text: "1"
            },
            {
              border: [false, false, false, false],
              styles: { alignment: 'right' },
              text: ""
            },
            {
              border: [false, false, false, false],
              styles: { alignment: 'right' },
              text: ""
            }, {
              border: [false, false, false, false],
              styles: { alignment: 'right' },
              text: ""
            },
            {
              border: [false, false, false, false],
              styles: { alignment: 'right' },
              text: ""
            },
            {
              border: [false, false, false, false],
              styles: { alignment: 'right' },
              text: ""
            }, {
              border: [false, false, false, false],
              styles: { alignment: 'right' },
              text: ""
            },
            {
              border: [false, false, false, false],
              styles: { alignment: 'right' },
              text: ""
            },
            {
              border: [false, false, false, false],
              styles: { alignment: 'right' },
              text: ""
            }, {
              border: [false, false, false, false],
              styles: { alignment: 'right' },
              text: ""
            },
            {
              border: [false, false, false, false],
              styles: { alignment: 'right' },
              text: ""
            },
            {
              border: [false, false, false, false],
              styles: { alignment: 'right' },
              text: ""
            },
            {
              border: [false, false, false, false],
              styles: { alignment: 'right' },
              text: ""
            },
            {
              border: [false, false, false, false],
              styles: { alignment: 'right' },
              text: ""
            }]);
          }
          count2 = 31
        }
      }
      else {
        if (oldunit != item.unitcode && count == 1) {
          oldunit = item.unitcode
        }
        else if (oldunit != item.unitcode) {
          oldunit = item.unitcode
          for (var i = count2; i < 31; i++) {
            data.push([{
              border: [false, false, false, false],
              styles: { alignment: 'right' },
              color: '#fff',
              text: "1"
            },
            {
              border: [false, false, false, false],
              styles: { alignment: 'right' },
              text: ""
            },
            {
              border: [false, false, false, false],
              styles: { alignment: 'right' },
              text: ""
            }, {
              border: [false, false, false, false],
              styles: { alignment: 'right' },
              text: ""
            },
            {
              border: [false, false, false, false],
              styles: { alignment: 'right' },
              text: ""
            },
            {
              border: [false, false, false, false],
              styles: { alignment: 'right' },
              text: ""
            }, {
              border: [false, false, false, false],
              styles: { alignment: 'right' },
              text: ""
            },
            {
              border: [false, false, false, false],
              styles: { alignment: 'right' },
              text: ""
            },
            {
              border: [false, false, false, false],
              styles: { alignment: 'right' },
              text: ""
            }, {
              border: [false, false, false, false],
              styles: { alignment: 'right' },
              text: ""
            },
            {
              border: [false, false, false, false],
              styles: { alignment: 'right' },
              text: ""
            },
            {
              border: [false, false, false, false],
              styles: { alignment: 'right' },
              text: ""
            },
            {
              border: [false, false, false, false],
              styles: { alignment: 'right' },
              text: ""
            },
            {
              border: [false, false, false, false],
              styles: { alignment: 'right' },
              text: ""
            }]);
          }
          count2 = 31
        }
      }
      if (count2 == 31) {
        count2 = 1
      }
      data.push([
        {
          border: [false, false, false, false],
          styles: { alignment: 'right' },
          fillColor: item.hilight == 1 ? '#cccccc' : '',
          noWrap: true,
          text: count.toString()
        }, {
          border: [false, false, false, false],
          fillColor: item.hilight == 1 ? '#cccccc' : '',

          noWrap: true,
          text: item.selectcode == null ? "" : item.selectcode.toString()
        }, {
          border: [false, false, false, false],
          fillColor: item.hilight == 1 ? '#cccccc' : '', noWrap: true,
          text: item.belong == null ? "" : item.belong.toString()
        }, {
          border: [false, false, false, false],
          fillColor: item.hilight == 1 ? '#cccccc' : '',
          noWrap: true,
          text: item.id8 == null ? "" : item.id8.toString()
        }, {
          border: [false, false, false, false],
          fillColor: item.hilight == 1 ? '#cccccc' : '',
          noWrap: true,
          text: item.fullname == null ? "" : item.fullname.toString()
        }, {
          border: [false, false, false, false],
          fillColor: item.hilight == 1 ? '#cccccc' : '',
          noWrap: true,
          text: item.askby == null ? "" : item.refnumur == null ?
            item.askby.toString() + " (" + item.num.toString() + ")" : (item.askby.toString() + " (" + item.num.toString() + ")").substring(41) != "" ? (item.askby.toString() + " (" + item.num.toString() + ")").substring(0, 38) + "..."
              : item.askby.toString() + " (" + item.num.toString() + ")"
        }, {
          border: [false, false, false, false],
          fillColor: item.hilight == 1 ? '#cccccc' : '',
          noWrap: true,
          text: item.refnumur == null ? "" : item.refnumur.substring(12) != "" ? item.refnumur.substring(0, 9) + "..." : item.refnumur
        }, {
          border: [
            false, false, false, false
          ],
          fillColor: item.hilight == 1
            ? '#cccccc'
            : '',
          noWrap: true,
          text: item.remark2 == null
            ? ""
            : item
              .remark2
              .toString()
        }, {
          border: [
            false, false, false, false
          ],
          fillColor: item.hilight == 1
            ? '#cccccc'
            : '',
          noWrap: true,
          text: item.remark == null
            ? ""
            : item
              .remark
              .toString()
        }, {
          border: [
            false, false, false, false
          ],
          fillColor: item.hilight == 1
            ? '#cccccc'
            : '',
          noWrap: true,
          text: item.refnum3 == null
            ? ""
            : item
              .refnum3
              .substring(12) != ""
              ? item
                .refnum3
                .substring(0, 9) + "..."
              : item.refnum3
        }, {
          border: [
            false, false, false, false
          ],
          fillColor: item.hilight == 1
            ? '#cccccc'
            : '',
          noWrap: true,
          text: item.stitle == null
            ? ""
            : item
              .stitle
              .substring(13) != ""
              ? item
                .stitle
                .substring(0, 10) + "..."
              : item.stitle
        }, {
          border: [
            false, false, false, false
          ],
          fillColor: item.hilight == 1
            ? '#cccccc'
            : '',
          noWrap: true,
          text: item.addictive == null
            ? ""
            : item
              .addictive
              .toString()
        }, {
          border: [
            false, false, false, false
          ],
          fillColor: item.hilight == 1
            ? '#cccccc'
            : '',
          noWrap: true,
          text: item.educname == null
            ? ""
            : item.postname == null
              ? item.educname
              : item
                .educname
                .substring(21) != ""
                ? item
                  .educname
                  .substring(0, 18) + "..."
                : item.educname
        }, {
          border: [
            false, false, false, false
          ],
          fillColor: item.hilight == 1
            ? '#cccccc'
            : '',
          noWrap: true,
          text: item.postname == null
            ? ""
            : item
              .postname
              .toString()
        }
      ])
      j++;
    })

    var docDefinition = {
      content: [
        {
          widths: '100%',
          table: {
            headerRows: 2,
            widths: [
              '2%',
              '1%',
              '2%',
              '4.5%',
              '10%',
              '19%',
              '5%',
              '10%',
              '18%',
              '5%',
              '5%',
              '2%',
              '10%',
              '7%'
            ],
            body: data
          }
        }
      ],

      defaultStyle: {
        font: 'THSarabun',
        fontSize: 12
      },
      styles: {
        center: {
          alignment: 'center'
        },
        tableExample: {
          widths: '100%'
        },
        tableHeader: {
          // bold: true, fontSize: 10,
        }
      },
      // a string or { width: number, height: number }
      pageSize: 'A4',

      // by default we use portrait, you can change it to landscape if you wish
      pageOrientation: 'landscape',

      // [left, top, right, bottom] or [horizontal, vertical] or just a number for
      // equal margins
      pageMargins: [5, 10, 5, 0]
    };
    pdfMake.fonts = {
      THSarabun: {
        normal: 'THSarabun.ttf',
        bold: 'THSarabun Bold.ttf',
        italics: 'THSarabun Italic.ttf',
        bolditalics: 'THSarabun BoldItalic.ttf'
      }
    }
    // pdfMake.createPdf(docDefinition).download();
    pdfMake.createPdf(docDefinition).download();

    // pdfMake.createPdf(docDefinition).print(); var doc = new jsPDF('L','mm','A4');
    // doc.addFont('THSarabun.ttf', 'THSarabun', 'normal'); doc.setFont("THSarabun")
    // doc.text(20, 50, 'Park Entry Ticketทดสอบ'); function
    // open_data_uri_window(url) {   var html = '<html>' +     '<style>html, body {
    // padding: 0; margin: 0; } iframe { width: 100%; height: 100%; border: 0;}
    // </style>' +     '<body>' +     '<iframe src="' + url + '"></iframe>' +
    // '</body></html>';   var open = window.open("","_blank")
    // open.document.write(html) } doc.save('custom_fonts.pdf');
    // open_data_uri_window(doc.output('datauri')); doc.text(20, 80, 'Address1: ' +
    // address1); doc.text(20, 100, 'Address2: ' + address2); doc.text(20, 120,
    // 'Entry Date & time: ' + entry_date_time); doc.text(20, 140, 'Expiry date &
    // time: ' + exit_date_time); doc.viewerPreferences({'FitWindow': true}, true)
    // doc.output('save', 'filename.pdf'); //Try to save PDF as a file (not works on
    // ie before 10, and some mobile devices) doc.output('datauristring');
    // //returns the data uri string doc.output('datauri');              //opens the
    // data uri in current window doc.output('dataurlnewwindow');     //opens the
    // data uri in new window doc.viewerPreferences({ 'HideWindowUI': true,
    // 'PrintArea': 'CropBox', 'NumCopies': 10 })
  }
  render() {
    const options = {
      btnGroup: this.createCustomButtonGroup,
      paginationShowsTotal: this.renderPaginationShowsTotal,  // custom,
      afterColumnFilter: this.afterColumnFilter,
      afterSearch: this.afterSearch,
    };
    return (
      <div>
        <Row>
          <Col sm={2}>
          </Col>
          <Col sm={10}>
            <div className="pull-right">
              <ControlLabel >เรียงตามผู้ขอ</ControlLabel>
              <Switch onClick={this.togglesort} on={switchsort}></Switch>
              &nbsp;&nbsp;&nbsp;
              <ControlLabel>แสดงผู้ขอซ้ำ</ControlLabel>
              <Switch onClick={this.togglerepeat} on={switchrepeat}></Switch>
              &nbsp;&nbsp;&nbsp;
              <ControlLabel>เฉพาะขอซ้ำ</ControlLabel>
              <Switch onClick={this.togglerepeat_2} on={switchrepeat_2}></Switch>
              &nbsp;&nbsp;&nbsp;
              <ControlLabel>หน่วยไม่ตรงกับร้องขอ</ControlLabel>
              <Switch onClick={this.toggleunitrequest} on={switchunitrequest}></Switch>
              {/* &nbsp;&nbsp;&nbsp;
              <ControlLabel>ล็อกไม่ตรงกับเลือก</ControlLabel>
              <Switch onClick={this.togglelockselectcode} on={switchlockselectcode} defaultChecked={switchlockselectcode}></Switch> */}

            </div>
          </Col>
        </Row>
        <BootstrapTable
          data={this.props.requestreportData} pagination exportCSV options={options} search>
          <TableHeaderColumn dataField='id' width="50" filter={{ type: 'RegexFilter' }}>ที่</TableHeaderColumn>
          <TableHeaderColumn dataField='selectcode' width="50" filter={{ type: 'RegexFilter' }}>#</TableHeaderColumn>
          <TableHeaderColumn dataField='addictive' width="50" filter={{ type: 'RegexFilter' }}>กลุ่ม</TableHeaderColumn>
          <TableHeaderColumn dataField='belong' width="50" filter={{ type: 'RegexFilter' }}>ร/พ</TableHeaderColumn>
          <TableHeaderColumn dataField='fullname' width="120" filter={{ type: 'RegexFilter' }}>ชื่อ - นามสกุล</TableHeaderColumn>
          <TableHeaderColumn dataField='refnumur' width="90" filter={{ type: 'RegexFilter' }}>หน่วยที่ขอ</TableHeaderColumn>
          <TableHeaderColumn dataField='askby' width="220" filter={{ type: 'RegexFilter' }}>ผู้ขอ</TableHeaderColumn>
          <TableHeaderColumn dataField='num' width="50" filter={{ type: 'RegexFilter' }}>ลำดับ</TableHeaderColumn>
          <TableHeaderColumn dataField='remark2' width="100" filter={{ type: 'RegexFilter' }}>ขอต่อ</TableHeaderColumn>
          <TableHeaderColumn dataField='remark' width="120" filter={{ type: 'RegexFilter' }}>หมายเหตุ</TableHeaderColumn>
          <TableHeaderColumn dataField='refnum3' width="90" filter={{ type: 'RegexFilter' }}>หน่วยที่ตก</TableHeaderColumn>
          <TableHeaderColumn dataField='stitle' width="90" filter={{ type: 'RegexFilter' }}>สถานภาพ</TableHeaderColumn>
          <TableHeaderColumn dataField='educname' width="150" filter={{ type: 'RegexFilter' }}>ความรู้</TableHeaderColumn>
          <TableHeaderColumn dataField='selectexam' width="170" filter={{ type: 'RegexFilter' }}>คัดเลือกหน่วย</TableHeaderColumn>
          <TableHeaderColumn
            dataField="NAVYID"
            isKey={true}
            dataFormat={this.buttonFormatter} width="50"></TableHeaderColumn>
        </BootstrapTable>
      </div>
    )
  }

}
function mapStatetoProps(state) {
  return {
    requestreportData: state.requestreportData,
    unittabData: state.unittabData,
    statustabData: state.statustabData,
    positiontabData: state.positiontabData,
    searchData: state.searchData,
    modalData: state.modalData,
    view: state.view,
    requestData: state.requestData
  }
}

function mapDispatchToProps(dispatch) {
  return {
    fetchData: name => dispatch({ type: 'FETCH_SEARCH_DATA', payload: name }),
    fetchData: sname => dispatch({ type: 'FETCH_SEARCH_DATA', payload: sname }),
    fetchData: id8 => dispatch({ type: 'FETCH_SEARCH_DATA', payload: id8 }),
    fetchModalData: navyid => dispatch({ type: "FETCH_MODAL_DATA", payload: navyid }),
    modal: showModalBuilder => dispatch({ type: "CHANGE_VIEW_DATA", payload: showModalBuilder }),
    fetchRequest: navyid => dispatch({ type: "FETCH_REQUEST_DATA", payload: navyid }),
    fetchUnittab: navyid => dispatch({ type: "FETCH_UNITTAB_DATA" }),
    fetchPositiontab: navyid => dispatch({ type: "FETCH_POSITIONTAB_DATA" }),
    fetchStatus: navyid => dispatch({ type: "FETCH_STATUSTAB_DATA" }),
    fetchRequestReport: where => dispatch({ type: "FETCH_REQUESTREPORT_DATA", payload: where }),
    fetchRequestReport: groupby => dispatch({ type: "FETCH_REQUESTREPORT_DATA", payload: groupby }),
    fetchRequestReport: have => dispatch({ type: "FETCH_REQUESTREPORT_DATA", payload: have }),
    fetchRequestReport: sort => dispatch({ type: "FETCH_REQUESTREPORT_DATA", payload: sort }),
    requesttab: requesttab => dispatch({ type: "CHANGE_VIEW_DATA", payload: requesttab }),
    requesttab: unit => dispatch({ type: "CHANGE_VIEW_DATA", payload: unit }),
    requesttab: state => dispatch({ type: "CHANGE_VIEW_DATA", payload: state }),
    alert: type => dispatch({ type: "ALERTS", payload: type }),
    alert: headline => dispatch({ type: "ALERTS", payload: headline }),
    alert: message => dispatch({ type: "ALERTS", payload: message }),
    alert: timeout => dispatch({ type: "ALERTS", payload: timeout }),
    alert: position => dispatch({ type: "ALERTS", payload: position }),
  }
}

const Connectedrequest_report = connect(mapStatetoProps, mapDispatchToProps)(Request_report)

export default Connectedrequest_report
