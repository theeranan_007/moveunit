import React, { Component } from 'react';
import HeaderNavigation from './component/HeaderNavigation'
import Alerts from "./component/Alerts";
import ModalDetail from './component/ModalDetail'
import { connect } from 'react-redux'

class App extends Component {
    constructor(props){
        super(props)
    }
    componentWillReceiveProps(nextProps){

    }
    render() {
        return (
            <div>
                <HeaderNavigation />
                <Alerts />
                <ModalDetail
                    
                   />
                <div>
                    {this.props.children}
                </div>
            </div>
        );
    }
}
function mapStatetoProps(state) {
    return {  typeOpenModal:state.typeOpenModal}
  }
  
  function mapDispatchToProps(dispatch) {
    return {
      fetchData: name => dispatch({ type: 'FETCH_SEARCH_DATA', payload: name }),
      fetchData: sname => dispatch({ type: 'FETCH_SEARCH_DATA', payload: sname }),
      fetchData: id8 => dispatch({ type: 'FETCH_SEARCH_DATA', payload: id8 }),
      fetchModalData: navyid => dispatch({ type: "FETCH_MODAL_DATA", payload: navyid }),
      modal: showModal => dispatch({ type: "CHANGE_VIEW_DATA", payload: showModal }),
      modal: requesttab => dispatch({ type: "CHANGE_VIEW_DATA", payload: requesttab }),
      modal: selectexamtab => dispatch({ type: "CHANGE_VIEW_DATA", payload: selectexamtab }),
      modalSelectexam: showModalSelectexam => dispatch({ type: "CHANGE_VIEW_DATA", payload: showModalSelectexam }),
      fetchRequest: navyid => dispatch({ type: "FETCH_REQUEST_DATA", payload: navyid }),
      alert: type => dispatch({ type: "ALERTS", payload: type }),
      alert: headline => dispatch({ type: "ALERTS", payload: headline }),
      alert: message => dispatch({ type: "ALERTS", payload: message }),
      alert: timeout => dispatch({ type: "ALERTS", payload: timeout }),
      alert: position => dispatch({ type: "ALERTS", payload: position }),
      typeModal: type => dispatch({type:"CHANGE_TYPEMODAL_DATA",payload:type})
    }
  }
  
  const ConnectedApp = connect(mapStatetoProps, mapDispatchToProps)(App)
  
  export default ConnectedApp

